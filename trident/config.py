import numpy as np
import pandas as pd
import yaml
import glob
from os import environ
from pathlib import Path


class GlobalConfig:
    """
    This class stroe the global file path and configure files.
    It includes the configure of DOM array and intra DOM geometry settings
    """

    def __init__(self, path_data_sets: str) -> None:
        """
        Initialize Config with the path of your data sets.
        The path should be relative path to `TRIDENT_DATA`.
        """

        self.initialized: bool

        self.file_array_config: Path
        self.path_data_sets: Path

        self.pos_range: dict
        self.dom_radius: float
        self.pmt_radius: float
        self.num_dom: int
        self.num_pmt: int
        self.num_sipm: int

        self.dom_pos: pd.DataFrame
        self.pmt_direction: pd.DataFrame
        self.sipm_direction: pd.DataFrame

        if not hasattr(self, "initialized"):
            self.initialized = True
        else:
            print("re-initialize global configure!")
        path_data_env = Path(environ["TRIDENT_DATA"])
        self.path_data_sets = path_data_env / path_data_sets
        return None

    def read_array_config(self):
        self.file_array_config = self.path_data_sets / "array.yaml"
        with open(self.file_array_config, 'r') as f:
            array_config = yaml.full_load(f)
        self.num_dom = array_config["number_x"] * array_config["number_y"] * array_config["number_z"]
        self.set_pos_range(array_config)
        self.set_dom_pos(array_config)

    def read_dom_config(self):
        self.file_dom_config = self.path_data_sets / "DOM.yaml"
        with open(self.file_dom_config, 'r') as f:
            dom_config = yaml.full_load(f)
        self.set_radius(dom_config)
        self.read_pmt_direction()
        self.set_pmt_direction_rect()
        self.read_sipm_direction()
        self.set_sipm_direction_rect()


    def read_run_config(self):
        self.read_run_array_config()
        self.read_run_dom_config()
        return None

    """ TODO Maybe not used anymore.
    def read_data(self, filename_data: str):
        if isinstance(filename_data, int):
            filename_data = str(filename_data)
        self.path_data = self.path_data_sets / filename_data
        self.file_particle = self.path_data / "particle.yaml"

        # The file data is different from others.
        # Since in Geant4 the data will be suffixed by the thread number
        # Here is an example: data_t1.root
        # So we need an wildcard to get the file.
        file_datas = glob.glob(str(self.path_data) + "/" + "*.root")
        try:
            self.file_data = file_datas[0]
        except:
            print("Path setting error!")
        if len(file_datas) > 1:
            print(f"Warning: muliti root file detected, using {self.file_data}")
        return None
    """

    def set_pos_range(self, array_config: dict) -> None:
        self.pos_range = {
            "minX": - array_config["seperation_x"] * (array_config["number_x"]-1)/2,
            "minY": - array_config["seperation_y"] * (array_config["number_y"]-1)/2,
            "minZ": - array_config["seperation_z"] * (array_config["number_z"]-1)/2,
            "maxX": array_config["seperation_x"] * (array_config["number_x"]-1)/2,
            "maxY": array_config["seperation_y"] * (array_config["number_y"]-1)/2,
            "maxZ": array_config["seperation_z"] * (array_config["number_z"]-1)/2,
        }
        return None

    def set_dom_pos(self, array_config) -> pd.DataFrame:
        dom_num = array_config['number_x'] * array_config['number_y'] * array_config['number_z']
        dom_pos_np = np.zeros((dom_num, 3))
        for i in range(dom_num):
            nx = (int)(i / array_config["number_y"] / array_config["number_z"])
            ny = (int)((i - (array_config["number_y"] * array_config["number_z"]) * nx) / array_config["number_z"])
            nz = (int)(i - (array_config["number_y"] * array_config["number_z"])*nx - array_config["number_z"] * ny)
            tx = (nx - (array_config["number_x"] - 1) / 2) * array_config["seperation_x"]
            ty = (ny - (array_config["number_y"] - 1) / 2) * array_config["seperation_y"]
            tz = (nz - (array_config["number_z"] - 1) / 2) * array_config["seperation_z"]
            dom_pos_np[i] = [tx, ty, tz]
        self.dom_pos = pd.DataFrame(dom_pos_np, index=np.arange(dom_num),
                                    columns=["x0", "y0", "z0"])
        return None

    def set_radius(self, dom_config) -> list:
        self.dom_radius = dom_config["radius_dom"]
        self.pmt_radius = dom_config["radius_pmt"]

    def read_pmt_direction(self) -> pd.DataFrame:
        filename = self.path_data_sets / "pmt_direction.csv"
        self.pmt_direction = pd.read_csv(filename, index_col=0)
        self.num_pmt = len(self.pmt_direction)
        return self.pmt_direction

    def set_pmt_direction_rect(self) -> pd.DataFrame:
        self.pmt_direction_rect = pd.DataFrame()
        self.pmt_direction_rect["nx"] = np.sin(self.pmt_direction["theta"]) * np.cos(self.pmt_direction["phi"])
        self.pmt_direction_rect["ny"] = np.sin(self.pmt_direction["theta"]) * np.sin(self.pmt_direction["phi"])
        self.pmt_direction_rect["nz"] = np.cos(self.pmt_direction["theta"])
        return self.pmt_direction_rect

    def read_sipm_direction(self) -> pd.DataFrame:
        filename = self.path_data_sets / "sipm_direction.csv"
        self.sipm_direction = pd.read_csv(filename, index_col=0)
        self.num_sipm = len(self.sipm_direction)
        return self.sipm_direction

    def set_sipm_direction_rect(self) -> pd.DataFrame:
        self.sipm_direction_rect = pd.DataFrame()
        self.sipm_direction_rect["nx"] = np.sin(self.sipm_direction["theta"]) * np.cos(self.sipm_direction["phi"])
        self.sipm_direction_rect["ny"] = np.sin(self.sipm_direction["theta"]) * np.sin(self.sipm_direction["phi"])
        self.sipm_direction_rect["nz"] = np.cos(self.sipm_direction["theta"])
        return self.sipm_direction_rect


class DataConfig(GlobalConfig):
    def __init__(self, globalconfig: GlobalConfig) -> None:
        self.globalconfig = globalconfig

    def read_data(self, filename_data: str):
        if isinstance(filename_data, int):
            filename_data = str(filename_data)
        self.path_data = self.globalconfig.path_data_sets / filename_data
        self.file_particle = self.path_data / "particle.yaml"
        self.file_photon_plane = self.path_data / "PhotonConfig.yaml"

        # Getting file data is a little tricky.
        # Since in Geant4 the data will be suffixed by the thread number
        # Here is an example: data_t1.root
        # So we need an wildcard to get the file.
        file_datas = glob.glob(str(self.path_data) + "/" + "*.root")
        try:
            self.file_data = file_datas[0]
        except:
            print("Path setting error!")
        if len(file_datas) > 1:
            print(f"Warning: muliti root file detected, using {self.file_data}")
        return None
