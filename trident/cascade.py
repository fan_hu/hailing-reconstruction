from pandas.core.frame import DataFrame
from trident.analysis import Analysis
from trident.constants import Constants

import pandas as pd
import numpy as np
import scipy.optimize as so
from scipy import special
from functools import wraps
from time import time
import logging


class Cascade(Analysis):
    """
    Class for cascade reconstruction
    """

    def __init__(self, dataconfig):
        super().__init__(dataconfig)
        # vertex is inform of [x, y, z, t]
        self.vertex_fg: np.ndarray
        self.vertex_rec: np.ndarray
        self.paras_first_guess: np.ndarray
        self.paras_time_residual: np.ndarray

    class PDF():
        """
        The assemble of pdf functions.

        Those function have parameters of x-data and other PDF specified paramters.
        All pdf function should support vectorize calculation.
        It can be used to calculate estimator (likelihood) or plot figures.
        """
        def emg(x: np.ndarray, lam: float, mu: float, sig2: float) -> float:
            y = (lam/2 * np.exp(-lam*(x-mu-lam*sig2/2))
                 * special.erfc((mu+lam*sig2-x)/np.sqrt(2*sig2)))
            return y

        def emg_modified(x: np.ndarray) -> float:
            return Cascade.PDF.emg(x, 0.02, -2., 5.) * 0.3 + Cascade.PDF.emg(x, 0.25, -2., 3.) * 0.7

        def possion(num: np.ndarray, lam: float) -> float:
            fract = np.array([np.math.factorial(num_) for num_ in num])
            return np.power(lam, num) * np.exp(-lam) / np.math.factorial(num)

    class Estimator():
        """
        The assemble of estimator function.

        The estimator function should not contain any PDF specified parameters.
        """
        def square(time_res: np.ndarray) -> float:
            return np.sqrt(1. + np.power(time_res, 2.)).sum()

        def emg(time_res: np.ndarray) -> float:
            return -np.log(Cascade.PDF.emg_modified(time_res)).sum()

        def possion(num: np.ndarray, lam: np.ndarray) -> float:
            """
            notes:
            ------
            num should be larger than one
            """
            return -(num * np.log(lam) - lam).sum()

        def hit_not_hit(hits: np.ndarray, lam: np.ndarray) -> float:
            lam_hit = lam[hits]
            likeli_hit = -(lam_hit).sum()
            lam_not_hit = lam[np.invert(hits)]
            likelihi_not_hit = np.log(1-np.exp(lam_not_hit)).sum()
            return likeli_hit + likelihi_not_hit

    def vertex_first_guess(self) -> np.ndarray:
        """
        First guess of vertex using mean method
        """
        self.vertex_fg = self.hits_sum.mean()[["x0", "y0", "z0", "tTTS"]]
        self.vertex_fg = self.vertex_fg.to_numpy()
        return self.vertex_fg

    def vertex_reconstruct(self, method: str):
        def likeli_decorator(hits: pd.DataFrame):
            """
            function of likelihood decorator that pass data to decorator
            """
            def decorator(func_estimator):
                """
                the real decorator
                """
                hits_x = hits["x0"].to_numpy()
                hits_y = hits["y0"].to_numpy()
                hits_z = hits["z0"].to_numpy()
                hits_t = hits["tTTS"].to_numpy()

                @ wraps(func_estimator)
                def wrapped(paras) -> float:
                    """
                    process the data to estimator recorgnized format.
                    This process is depents on the "paras" we want to fit.
                    """
                    rx = hits_x - paras[0]
                    ry = hits_y - paras[1]
                    rz = hits_z - paras[2]
                    r_len = np.power(rx * rx + ry * ry + rz * rz, 0.5)
                    time_res = hits_t - r_len / Constants.c_n - paras[3]
                    return func_estimator(time_res)
                return wrapped
            return decorator

        estimator_method = getattr(Cascade.Estimator, method)
        likeli_func = likeli_decorator(self.hits_sum)(estimator_method)
        opt_result = 0
        if method == "square":
            opt_result = so.minimize(fun=likeli_func,
                                     x0=self.vertex_fg,
                                     method="Nelder-Mead")
        elif method == "emg":
            if (not hasattr(self, "vertex_rec")):
                logging.error("you must implement square rection method first")
            opt_result = so.minimize(fun=likeli_func,
                                     x0=self.vertex_rec,
                                     method="Nelder-Mead")
            if (not opt_result["success"]):
                logging.warning("EMG reconstruction failed!")
        else:
            logging.error("method can only be 'square' or 'emg'!")
            raise KeyError(method)
        print(f"result: {opt_result}")
        self.vertex_rec = opt_result["x"]
        return opt_result

    @staticmethod
    def vertex_position_err(vertex: np.ndarray, paras_true: np.ndarray) -> np.ndarray:
        """
        calculate the displacement of the vertex
        """
        vertex_rec = vertex[0:3]
        vertex_true = paras_true[0:3]
        direc_true = paras_true[3:6]
        displace_vertex = vertex_rec - vertex_true
        displace_tot = np.linalg.norm(displace_vertex)
        displace_long = displace_vertex.dot(direc_true)
        displace_radi = np.sqrt(displace_tot*displace_tot-displace_long*displace_long)
        return np.array([displace_long, displace_radi, displace_tot])
        
    def fill_hits_sum(self):
        array_dom = np.arange(self.dataconfig.globalconfig.num_dom)
        array_pmt = np.arange(self.dataconfig.globalconfig.num_pmt)
        new_index = pd.MultiIndex.from_product([array_dom, array_pmt], names=["DomID", "PmtID"])
        hits_sum_filled = self.hits_sum.reindex(new_index, fill_value=0)
        hits_sum_filled = hits_sum_filled[["x0", "y0", "z0", "n"]]
        dom_pos = self.dataconfig.globalconfig.dom_pos
        dom_pos_filled = dom_pos.loc[new_index.get_level_values(0)].to_numpy()
        hits_sum_filled["x0"] = dom_pos_filled[:,0]
        hits_sum_filled["y0"] = dom_pos_filled[:,1]
        hits_sum_filled["z0"] = dom_pos_filled[:,2]
        return hits_sum_filled



    def preprocess_hits(self, hits: pd.DataFrame, vertex: np.ndarray) -> pd.DataFrame:
        """
        calculate the distance, time residual and direction from DOM to vertex
        according to the vertex coordinate
        """
        df_hits = hits[["x0", "y0", "z0"]]
        df_hits["x"] = vertex[0] - df_hits["x0"]
        df_hits["y"] = vertex[1] - df_hits["y0"]
        df_hits["z"] = vertex[2] - df_hits["z0"]
        df_hits["d"] = np.linalg.norm(df_hits[['x', 'y', 'z']].values, axis=1)
        df_hits["nx"] = df_hits["x"] / df_hits["d"]
        df_hits["ny"] = df_hits["y"] / df_hits["d"]
        df_hits["nz"] = df_hits["z"] / df_hits["d"]
        pmt_index = hits.index.get_level_values(1)
        pmt_direction = self.dataconfig.globalconfig.pmt_direction_rect
        df_pmt = pmt_direction.loc[pmt_index]
        df_pmt.set_index(hits.index, inplace=True)
        df_hits["pmt_nx"] = df_pmt["nx"]
        df_hits["pmt_ny"] = df_pmt["ny"]
        df_hits["pmt_nz"] = df_pmt["nz"]
        df_hits["n"] = hits["n"]
        return df_hits[["nx", "ny", "nz", "pmt_nx", "pmt_ny", "pmt_nz", "d", "n"]]

    @staticmethod
    def calculate_distance_factor(distance: np.ndarray, d0=50., len_att=40.) -> np.ndarray:
        return (d0*d0+1) / (distance*distance+1) * np.exp(-distance/len_att)

    @staticmethod
    def calculate_dom_factor(costh):
        return 1 / (1 + np.exp(-1.0*costh))

    @staticmethod
    def calculate_pmt_factor(costh):
        return 1 / (1 + np.exp(-2.0*costh))

    @staticmethod
    def calculate_lam(df_direction: np.ndarray, distance_factor: np.ndarray,
                      paras: np.ndarray, lam0=60., with_pmt=False) -> np.ndarray:
        direc = Analysis.pol2rect(*paras)
        costh_dom = -(df_direction[:, 0] * direc[0]
                      + df_direction[:, 1] * direc[1]
                      + df_direction[:, 2] * direc[2])
        dom_factor = Cascade.calculate_dom_factor(costh_dom)
        lam = lam0 * distance_factor * dom_factor
        if with_pmt:
            costh_pmt = -(df_direction[:, 3] * direc[0]
                          + df_direction[:, 4] * direc[1]
                          + df_direction[:, 5] * direc[2])
            pmt_factor = Cascade.calculate_pmt_factor(costh_pmt)
            lam = lam * pmt_factor
        return lam

    def direction_reconstruct(self, method: str):
        """
        0. calculate the DOM-vertex direction vector
        1. calculate the costh_dom according to a cascade direction
        2. propagability of nhits as a function of costh_dom
        """
        def likeli_decorator(hits: pd.DataFrame, vertex: np.ndarray):
            """
            function of likelihood decorator that pass data to decorator

            Parameters
            ----------
            method : str
                decide which decorator method to return
            """
            df_hits = self.preprocess_hits(hits, vertex)
            distance = df_hits["d"].to_numpy()
            num_hits = df_hits["n"].to_numpy()
            df_direction = df_hits[["nx", "ny", "nz", "pmt_nx", "pmt_ny", "pmt_nz"]].to_numpy()
            distance_factor = Cascade.calculate_distance_factor(distance)

            def decorator(func_estimator):
                @ wraps(func_estimator)
                def wrapped(paras) -> float:
                    """
                    process the data to estimator recorgnized format.
                    This process is depents on the "paras" we want to fit.
                    """
                    if method == "dom":
                        lam = Cascade.calculate_lam(df_direction, distance_factor, paras)
                    else:
                        lam = Cascade.calculate_lam(df_direction, distance_factor, paras, with_pmt=True)
                    return func_estimator(num_hits, lam)
                return wrapped
            return decorator

        likeli_func = likeli_decorator(self.hits_sum, self.vertex_rec)(Cascade.Estimator.possion)
        opt_result = so.minimize(fun=likeli_func,
                                 x0=[1.5, 2.0],
                                 method="Nelder-Mead")
        print(opt_result)
        self.direction_pol = opt_result["x"]
        self.direction_rect = Analysis.pol2rect(*self.direction_pol)
        return opt_result

    def direction_reconstruction_hit_binominal(self):
        def likeli_decorator(hits: pd.DataFrame, vertex: np.ndarray):
            """
            function of likelihood decorator that pass data to decorator

            Parameters
            ----------
            method : str
                decide which decorator method to return
            """
            hits_sum_filled = self.fill_hits_sum()
            df_hits = self.preprocess_hits(hits_sum_filled, vertex)
            distance = df_hits["d"].to_numpy()
            num_hits = df_hits["n"].to_numpy()
            df_direction = df_hits[["nx", "ny", "nz", "pmt_nx", "pmt_ny", "pmt_nz"]].to_numpy()
            distance_factor = Cascade.calculate_distance_factor(distance)

            def decorator(func_estimator):
                @ wraps(func_estimator)
                def wrapped(paras) -> float:
                    """
                    process the data to estimator recorgnized format.
                    This process is depents on the "paras" we want to fit.
                    """
                    lam = Cascade.calculate_lam(df_direction, distance_factor, paras)
                    return func_estimator(num_hits, lam)
                return wrapped
            return decorator

        likeli_func = likeli_decorator(self.hits_sum, self.vertex_rec)(Cascade.Estimator.hit_not_hit)
        opt_result = so.minimize(fun=likeli_func,
                                 x0=[1.5, 2.0],
                                 method="Nelder-Mead")
        print(opt_result)
        self.direction_pol = opt_result["x"]
        self.direction_rect = Analysis.pol2rect(*self.direction_pol)
        return opt_result



