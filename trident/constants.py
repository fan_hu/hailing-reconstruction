import math

class Constants:
    c = 0.2998
    n = 1.34
    c_n = c / n
    costh = 1 / n
    tanth = math.sqrt(1 - costh*costh) / costh
    sinth = costh * tanth
