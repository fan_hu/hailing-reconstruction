from trident.config import DataConfig
from trident.constants import Constants
from trident.root2pd import root2pd
from matplotlib.pyplot import axis
import pandas as pd
import numpy as np
import yaml
from functools import wraps


class Analysis:
    def __init__(self, dataconfig: DataConfig) -> None:
        self._paras_true: np.ndarray
        self._hits: pd.DataFrame
        self._hits_sum: pd.DataFrame
        self._hits_true: pd.DataFrame
        self.dataconfig = dataconfig
        self.globalconfig = dataconfig.globalconfig

    @ property
    def paras_true(self):
        return self._paras_true

    @ property
    def hits(self):
        return self._hits

    @ property
    def hits_sum(self):
        return self._hits_sum

    @ property
    def electrons(self):
        return self._electrons

    @ property
    def hits_true(self):
        return self._hits_true

    def read_hits(self, treename: str, flag_verbose: bool = True) -> pd.DataFrame:
        """
        Read hits ROOT tree `treename` from the ROOT file at the specified by dataconfig.

        This method set up `self._hits` as a `pandas.DataFrame`.

        Parameters
        ----------
        `treename: str`
            The tree name of the ROOT tree. 
            You can read the output to see the tree names in that ROOT file

        `flag_verbose: bool`
            Set true to print additional information about the ROOT file and tree.

        Return
        ------
        `hits: pandas.DataFrame`
            The hits in that tree.
        """
        from trident.root2pd import root2pd
        self._hits = root2pd(self.dataconfig.file_data, treename, flag_verbose)
        return self._hits

    def sample_hits_from_dom(self, area_ratio: float, qe_max: float) -> pd.DataFrame:
        """
        Sample SiPM hits from DOM hits
        """
        def get_qe(qe_max, energy: np.ndarray):
            sigma = 0.35
            mean = 2.75
            qe = (1 / np.sqrt(2*np.pi) / sigma *
                  np.exp(-np.power((energy-mean)/sigma, 2)/2))
            return qe_max * qe
        dom_hit = root2pd(self.dataconfig.file_data, "DomHit", False)
        eff = area_ratio * get_qe(qe_max, dom_hit["e"])
        eff_sample = np.random.uniform(0., 1., len(eff)) < eff
        hits_sample = dom_hit.loc[eff_sample, ["DomID", "x0", "y0", "z0", "t0"]]
        self._hits = hits_sample
        return self._hits

    def read_paras_true(self, flag_verbose: bool = True) -> np.ndarray:
        """
        Read the true parameters from a yaml file.

        This method set up `self._paras_true` as a `np.ndarray`.

        Parameters
        ----------
        `flag_verbose: bool`
            Set true to print additional information about the true paras.

        Return
        ------
        `numpy.ndarray`
            The numpy array of true parameters.
        """
        with open(self.dataconfig.file_particle, 'r') as f:
            input = yaml.full_load(f)
        position = np.array(list(input["position"]))
        direction = np.array(list(input["direction"]))
        self._paras_true = np.append(
            position, direction / np.linalg.norm(direction))
        if flag_verbose:
            print("Monte Carlo true parameters: ")
            print(self._paras_true)
        return self._paras_true

    def set_hits_sum(self, key: str = "DomID", method: str = "min") -> pd.DataFrame:
        """
        Use `pandas.grouby` to sum up hits in the same DOM and store it as `self._hits_sum`.

        The columns "t0" is set as the minimum hit time.
        The columns "n" is the sum of all hits in a DOM.

        Parameter
        ---------
        `key: str`
            The key to sum over. Accepted keys: `['DomID', 'PmtID', 'SipmID']`

        `method: str`
            The method used in `pandas.groupby`, it accepts `'min'`, `'mean'`...

        Return
        ------
        `pandas.DataFrame`
            The sumed hits
        """
        if (key == "DomID"):
            self._hits_sum = getattr(self._hits.groupby("DomID"), method)()
            self._hits_sum["n"] = self._hits["DomID"].value_counts()
        elif (key == "PmtID"):
            self._hits_sum = getattr(
                self._hits.groupby(["DomID", "PmtID"]), method)()
            self._hits_sum["n"] = self._hits[["DomID", "PmtID"]].value_counts()
        elif (key == "SipmID"):
            self._hits_sum = getattr(
                self._hits.groupby(["DomID", "SipmID"]), method)()
            self._hits_sum["n"] = self._hits[[
                "DomID", "SipmID"]].value_counts()
        else:
            print("Input key name is not accepted!")
            raise KeyError
        return self._hits_sum

    def copy_hits_sum(self) -> pd.DataFrame:
        if hasattr(self, "_hits_sum"):
            df = self._hits_sum[["x0", "y0", "z0", "t0", "n"]].copy()
        else:
            print("Need to sum up hits first!")
            raise KeyError
        return df

    def plot_hits(self, filename: str = "hits_sum.png",
                  flag_plot_arrow: bool = True) -> None:
        # TODO FIXME
        from trident.plots import plot_track_3d
        from trident.config import Config
        if flag_plot_arrow:
            plot_track_3d(self._hits_sum, Config.path_data +
                          filename, self._paras_true)
        else:
            plot_track_3d(self._hits_sum, Config.path_data+filename)
        return None

    def read_electrons(self) -> pd.DataFrame:
        """
        Read high energy secondary electrons from the ROOT file specified by dataconfig.

        This method set up self._hits as a `pandas.DataFrame`.

        It depends on the `uproot` module.

        Return
        ------
        `pandas.DataFrame`
            The electrons in that tree.
        """
        from trident.root2pd import root2pd
        self._electrons = root2pd(self.dataconfig.file_data, "electron")
        self._electrons.sort_values(by="t0", inplace=True)
        return self._electrons

    @ classmethod
    def angle_err(cls, dir1: np.ndarray, dir2: np.ndarray) -> float:
        """
        Calculate the intersection angle of the two directions.

        Parameter
        ---------
        `dir1: np.ndarray`
            The one direction, it can be in form of [theta, phi] or [nx, ny, nz].

        `dir2: np.ndarray`
            The other direction.

        Return
        ------
        `float`
            The intersection angle in units of **minute**
        """
        if len(dir1) == 2:
            dir1 = cls.pol2rect(*dir1)
            dir2 = cls.pol2rect(*dir2)
        dir1 = dir1 / np.linalg.norm(dir1)
        dir2 = dir2 / np.linalg.norm(dir2)
        delta = np.arccos(dir1.dot(dir2))
        return delta * 180 / np.pi * 60

    @ staticmethod
    def calculate_distance(hits: pd.DataFrame, paras: np.ndarray, tkey="t0", pmt_radius=0.215) -> pd.DataFrame:
        """
        This method do the following calculations:
        - the distances from the photon hits to the track, 
        - the geometric photon arrival time and the residual time.

        Parameter
        ---------
        `hits: pandas.DataFrame`
            The photon hits

        `paras: np.ndarray`
            The true or assumed parameters of track.
            It is constructed as [x0, y0, z0, nx, ny, nz]

        `tkey: str`
            The key name of time column. It can be `"t0"` or `"t_true"`. 
            Default value is `"t0"`

        Return
        ------
        `pd.DataFrame`
            The dataframe that contain distance, geometry time and residual time.
            It has keys: `['d', 't_g', 't_r']`
        """
        [nx, ny, nz] = paras[-3:]
        rx = hits["x0"] - paras[0]
        ry = hits["y0"] - paras[1]
        rz = hits["z0"] - paras[2]
        r_len = np.power(rx*rx + ry*ry + rz*rz, 0.5)
        l_len = rx * nx + ry * ny + rz * nz
        df = pd.DataFrame()
        distance = np.power(r_len * r_len - l_len * l_len, 0.5)
        time_geometry = ((l_len + distance * Constants.tanth) / Constants.c - pmt_radius / Constants.c_n)
        time_residual = hits[tkey] - time_geometry
        df = pd.DataFrame({"d": distance, "t_g": time_geometry, "t_r": time_residual})
        return df

    @ staticmethod
    def calculate_parent(hits, paras: np.ndarray) -> pd.DataFrame:
        """
        This method can calculate the distances from the parent electrons to the track, 
        the cos theta of the parent-track intersection angle, 
        the parent energy and parent residual time.

        Parameter
        ---------
        `hits: pandas.DataFrame`
            The photon hits that contains the parent particle information

        `paras: np.ndarray`
            The true or assumed parameters of track

        Return
        ------
        `pd.DataFrame`
            The dataframe with keys: `['parent_d', 'parent_costh', 'parent_e', 'parent_tr']`
        """
        from trident.constants import Constants
        [nx, ny, nz] = paras[-3:]
        rx = hits["parent_x0"] - paras[0]
        ry = hits["parent_y0"] - paras[1]
        rz = hits["parent_z0"] - paras[2]
        r_len = np.power(rx*rx + ry*ry + rz*rz, 0.5)
        l_len = rx * nx + ry * ny + rz * nz
        df = pd.DataFrame()
        df["parent_d"] = np.power(r_len * r_len - l_len * l_len, 0.5)
        df["parent_costh"] = (hits["parent_nx"] * nx
                              + hits["parent_ny"] * ny
                              + hits["parent_nz"] * nz)
        df["parent_e"] = hits["parent_e"]
        df["parent_tr"] = hits["parent_t0"] - r_len / Constants.c
        return df

    # * You can add more information to this DataFrame
    def set_hits_true(self) -> pd.DataFrame:
        """
        Analysis method
        ---------------
        This method is used to get the PDF of hits.
        It derive information from `self._paras_true`.

        Return
        ------
        `self._hits_true: pandas.DataFrame`
            The particle independent information with columns:
            `['d', 't_g', 't_r', 'parent_d', 'parent_costh', 'parent_e', 'parent_tr']`
        """
        if not hasattr(self, "paras_true"):
            self.read_paras_true(self.dataconfig.file_particle)
        df_distance = Analysis.calculate_distance(
            self._hits, self._paras_true)
        df_parent = Analysis.calculate_parent(self._hits, self._paras_true)
        self._hits_true = pd.concat([df_distance, df_parent], axis=1)
        return self._hits_true

    def set_pmt_trigger_time(hits: pd.DataFrame, t_tts: float = 2.0,
                             t_raise: float = 0.9,
                             t_adc: float = 1) -> pd.DataFrame:
        """
        Set the time fluctuation for PMT

        Parameter
        ---------
        `t_tts`, `t_raise` and `t_adc`: the time spread due to transient time spread, 
        waveform raise time and ADC sample rate.
        """
        time_smear = np.sqrt(t_tts*t_tts + t_raise*t_raise + t_adc*t_adc)
        hits["t_true"] = hits["t0"]
        hits["t0"] += np.random.normal(0., time_smear, len(hits))
        return hits

    @ staticmethod
    def pol2rect(theta: float, phi: float) -> np.ndarray:
        """
        Change from polar coordinates to rectangular coordinates
        """
        return np.array([np.cos(phi)*np.sin(theta),
                         np.sin(phi)*np.sin(theta),
                         np.cos(theta)])

    @ staticmethod
    def rect2pol(nx: float, ny: float, nz: float) -> np.ndarray:
        """
        Change from rectangular coordinates to polar coordinates
        """
        theta = np.arccos(nz)
        phi = np.arctan2(ny, nx)
        return np.array([theta, phi])

    @ staticmethod
    def rot_uz(px0: np.ndarray, py0: np.ndarray, pz0: np.ndarray, vec_z: list) -> list:
        """
        Rotate coordinate so that its z-axis is now pointing to `vec_z`.
        """
        [ux, uy, uz] = vec_z
        up = ux * ux + uy * uy
        if up > 0:
            up = np.sqrt(up)
            px = (ux*uz*px0 - uy*py0)/up + ux*pz0
            py = (uy*uz*px0 + ux*py0)/up + uy*pz0
            pz = -up*px0 + uz*pz0
        elif uz < 0:
            [px, py, pz] = [-px0, py0, -pz0]
        else:
            [px, py, pz] = [px0, py0, pz0]
        return [px, py, pz]

    @ staticmethod
    def transform(px0: np.ndarray, py0: np.ndarray, pz0: np.ndarray, paras: np.ndarray) -> list:
        """
        Transform coordinate so that its origin point is now at `paras[0:3]`, 
        its z-axis is now pointing to `paras[3:6]`.
        """
        [x0, y0, z0] = paras[0:3]
        px = px0 - x0
        py = py0 - y0
        pz = pz0 - z0
        [px, py, pz] = Analysis.rot_uz(px, py, pz, paras[3:6])
        return [px, py, pz]

    def plot_distance_time(self, filename: str = "distance_time.png", tkey="t0",
                           flag_plot_paras=False, tau=30., lam=50.):
        df = self._hits_sum[["x0", "y0", "z0", tkey, "n"]]
        df_distance = Analysis.calculate_distance(df, self._paras_true, tkey)
        from trident.plots import plot_distance_time
        plot_distance_time(df_distance["d"], df_distance["t_r"], df["n"],
                           self.dataconfig.path_data / filename, tau, lam, flag_plot_paras)
        return None

    def plot_electrons(self, filename: str = "electrons.png",) -> None:
        df = pd.DataFrame()
        [df["px"], df["py"], df["pz"]] = Analysis.rot_uz(self.electrons["px"],
                                                         self.electrons["py"],
                                                         self.electrons["pz"],
                                                         self._paras_true[3:])
        [df["x0"], df["y0"], df["z0"]] = Analysis.transform(self.electrons["x0"],
                                                            self.electrons["y0"],
                                                            self.electrons["z0"],
                                                            self._paras_true)
        df["nx"] = ((df["x0"] + 500) / 20).astype("int16")
        df["ny"] = ((df["y0"] + 0.004) / 0.001).astype("int16")
        df["nz"] = ((df["z0"] + 0.004) / 0.001).astype("int16")
        df_n = df[["nx", "ny", "nz"]]
        mask = ((df_n["nx"] >= 0) & (df_n["nx"] < 50)
                & (df_n["ny"] >= 0) & (df_n["ny"] < 10)
                & (df_n["nz"] >= 0) & (df_n["nz"] < 10))
        df_mask = df_n[mask]
        df_mask["id"] = df_mask["nx"] + 10 * \
            df_mask["ny"] + 10*20 * df_mask["nz"]
        df_group = df_mask.groupby("id").mean().astype("int16")
        df_group["x0"] = 20.0 * df_group["nx"] - 500.
        df_group["y0"] = 1. * df_group["ny"] - 4.  # in unit of mm
        df_group["z0"] = 1. * df_group["nz"] - 4.
        df_group["n"] = df_mask["id"].value_counts()
        from trident.plots import plot_electrons
        plot_electrons(df_group, self.dataconfig.path_data+filename)
        return None

    def align_electrons(self):
        self._electrons["dx"] = self._electrons["x0"] - self._paras_true[0]
        self._electrons["dy"] = self._electrons["y0"] - self._paras_true[1]
        self._electrons["dz"] = self._electrons["z0"] - self._paras_true[2]
        self._electrons["l0"] = (self._electrons["dx"] * self._paras_true[3]
                                 + self._electrons["dy"] * self._paras_true[4]
                                 + self._electrons["dz"] * self._paras_true[5])
        self._electrons["costh_d"] = (1.0 - self._electrons["px"] * self._paras_true[3]
                                      - self._electrons["py"] * self._paras_true[4]
                                      - self._electrons["pz"] * self._paras_true[5])
        return None
