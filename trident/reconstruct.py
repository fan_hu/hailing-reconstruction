from typing import Callable
from trident.analysis import Analysis
from trident.constants import Constants
from trident.plots import plot_track_3d

import pandas as pd
import numpy as np
from numpy import pi, power, sqrt, exp, log
import scipy.optimize as so
from functools import wraps
from time import time


class LikelihoodCollection:
    """
    Get a likelihood function.

    likelihood function:
    -------
    input: residual time, distance to track, num of hits
    output: -log(probability) + const
    """
    # TODO Build a factory

    def m_estimator() -> Callable[[np.ndarray, np.ndarray, np.ndarray], np.ndarray]:
        def m_estimator_(time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
            likeli = 2 - 2*np.sqrt(time*time/2 + 1)
            return likeli
        return m_estimator_

    def gaussian(tau: float, lam: float) -> Callable[[np.ndarray, np.ndarray, np.ndarray], np.ndarray]:
        def gaussian_(time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
            mean = tau / lam * distance
            sig = tau * sqrt(distance / lam / num)
            time_ = (time - mean) / sig
            likeli = np.log(sig) + np.power(time_, 2) / 2
            return likeli
        return gaussian_

    def skew_cauchy(mean_vs_tau: float, tau_vs_distance: float, alpha: float) -> Callable[[np.ndarray, np.ndarray, np.ndarray], np.ndarray]:
        def skew_cauchy_(time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> float:
            tau = tau_vs_distance * distance + 1
            mean = mean_vs_tau * tau
            time_ = (time - mean) / tau
            t_sk = alpha * time_
            log_pdf = log((t_sk/sqrt(t_sk*t_sk+1)+1) / (pi*tau*(time_*time_+1)))
            log_cdf = np.log(1/2 - 1/pi*(1/sqrt(t_sk*t_sk+1) + np.arctan(t_sk)))
            likeli = log(num) + log_pdf + (num-1)*log_cdf
            return -likeli
        return skew_cauchy_


class OptimizationCollection:
    """
    Get a optimization function.

    optimization function:
    -----------------
    input: paras(pol), hits
    output: -log(likelihood)
    """

    def m_estimator() -> Callable[[np.ndarray, pd.DataFrame], np.ndarray]:
        def m_estimator_(paras: np.ndarray, hits: pd.DataFrame) -> float:
            paras_rect = np.append(paras[:3], Analysis.pol2rect(*paras[-2:]))
            df = Analysis.calculate_distance(hits, paras_rect)
            likeli_func = LikelihoodCollection.m_estimator()
            likeli = likeli_func(df["t_r"], 0, 0)
            return likeli.sum()
        return m_estimator_

    def gaussian(tau: float, lam: float) -> Callable[[np.ndarray, pd.DataFrame], np.ndarray]:
        def gaussian_(paras: np.ndarray, hits_mean: pd.DataFrame) -> float:
            paras_rect = np.append(paras[:3], Analysis.pol2rect(*paras[-2:]))
            df = Analysis.calculate_distance(hits_mean, paras_rect)
            likeli_func = LikelihoodCollection.gaussian(tau, lam)
            likeli = likeli_func(df["t_r"], df["d"], hits_mean["n"])
            return likeli.sum()
        return gaussian_

    def skew_cauchy(mean_vs_tau: float, tau_vs_distance: float, alpha: float) -> Callable[[np.ndarray, pd.DataFrame], np.ndarray]:
        def skew_cauchy_(paras: np.ndarray, hits_first: pd.DataFrame) -> float:
            paras_rect = np.append(paras[:3], Analysis.pol2rect(*paras[-2:]))
            df = Analysis.calculate_distance(hits_first, paras_rect)
            likeli_func = LikelihoodCollection.skew_cauchy(mean_vs_tau, tau_vs_distance, alpha)
            likeli = likeli_func(df["t_r"], df["d"], hits_first["n"])
            return likeli.sum()
        return skew_cauchy_


def timing(func):
    """
    decorator for timing
    """
    @ wraps(func)
    def wrapped(*args, **kwards):
        ts = time()
        result = func(*args, **kwards)
        te = time()
        print('time took: {:2.4f} sec'.format(te-ts))
        return result
    return wrapped


def model_decorator(tau_: float, lam_: float):
    """
    Specify the tau and lam used in the model.
    """
    def decorator(func):
        @ wraps(func)
        def wrapped(*args, **kwargs):
            return func(*args, **kwargs, tau=tau_, lam=lam_)
        return wrapped
    return decorator


def print_rec_result(method: str, paras_rec, paras_true):
    print("-------------------------")
    print(f"Result of reconstruction using {method}: ")
    print(paras_rec)
    print("Monte Carlo true parameters: ")
    print(paras_true)
    print("Angle error: {:.3f} minutes".format(
        Analysis.angle_err(paras_rec[3:], paras_true[3:])))
    print("-------------------------")


class Reconstruct(Analysis):
    def __init__(self, dataconfig):
        super().__init__(dataconfig)
        self.paras_first_guess: np.ndarray
        self.paras_time_residual: np.ndarray

    def first_guess(self, hits: pd.DataFrame, flag_verbose=True) -> None:
        # select data
        df = hits.copy()
        df = df.assign(tbin=lambda df: (df["t0"]/300).astype('int32'))
        df_tbin = df.groupby("tbin").mean()
        df_tbin["n"] = df["tbin"].value_counts()

        def calculate_likeli(paras: np.ndarray) -> float:
            df_trans = df_tbin.sub(
                np.append(paras[:3], [0, 0]), axis="columns")
            df_expect = pd.DataFrame((df_trans[["t0", "t0", "t0"]] * paras[-3:]).to_numpy(),
                                     index=df_trans.index, columns=["x0", "y0", "z0"])
            df_delta = df_trans[["x0", "y0", "z0"]] - df_expect
            df_delta = df_delta.multiply(df_trans["n"], axis="index")
            error = df_delta.pow(2).sum().sum() / 1e6
            return error

        def fit_line(ti, x0, y0, z0, vx, vy, vz):
            xi = x0 + ti * vx
            yi = y0 + ti * vy
            zi = z0 + ti * vz
            return np.array([xi, yi, zi])

        v0 = ((df_tbin.iloc[-1, :3] - df_tbin.iloc[0, :3]) /
              (df_tbin.iloc[-1, 3] - df_tbin.iloc[0, 3]))
        x0 = df_tbin.iloc[0, :3] - v0 * df_tbin.iloc[0, 3]
        para0 = x0.append(v0).to_list()
        opt_result = so.minimize(fun=calculate_likeli, x0=para0)
        self.paras_first_guess = opt_result["x"]
        time_fit = np.linspace(
            self._hits["t0"].min(), self._hits["t0"].max(), 10)
        pos_fit = fit_line(time_fit, *self.paras_first_guess).transpose()
        angle_error = Analysis.angle_err(self.paras_first_guess[3:], self.paras_true[3:])
        if flag_verbose:
            print_rec_result("First guess", self.paras_first_guess, self.paras_true)
        return angle_error

    def time_residual_m_estimator(self, hits: pd.DataFrame, flag_verbosity=True):
        """
        Reconstruct using average arrival time method

        Returen
        -------
        Angle error in minutes
        """
        # data preprocess
        df = hits.copy()

        # prepare the first guess parameters
        position = self.paras_first_guess[0:3]
        direction = Analysis.rect2pol(*(self.paras_first_guess[3:6] /
                                        np.linalg.norm(self.paras_first_guess[3:6])))
        paras_init = np.append(position, direction)

        # the likelihood function
        def calculate_likeli(paras: np.ndarray) -> float:
            paras = np.append(paras[:3], Analysis.pol2rect(*paras[-2:]))
            df_distance = Analysis.calculate_distance(df, paras, "t0")
            t_res = df_distance["t_r"]
            likeli = 2 - 2*np.sqrt(1+t_res*t_res/2)
            return -likeli.sum()

        # doing minimization
        if flag_verbosity:
            print("Performing minimation for time residual likelihood method...")
        opt_result = so.minimize(fun=calculate_likeli, x0=paras_init)
        opt_direction = Analysis.pol2rect(*opt_result["x"][3:5])
        opt_position = opt_result["x"][0:3]
        self.paras_estimator = np.append(opt_position, opt_direction)
        angle_error = Analysis.angle_err(self.paras_estimator[3:], self.paras_true[3:])
        # print results
        if flag_verbosity:
            print_rec_result("time residual M estimator", self.paras_estimator, self.paras_true)
        return angle_error

    def time_residual_avg(self, hits, tau, lam, flag_verbosity=True) -> float:
        """
        Reconstruct using average arrival time method

        Returen
        -------
        Angle error in minutes
        """
        # prepare the first guess parameters
        position = self.paras_estimator[0:3]
        direction = Analysis.rect2pol(*(self.paras_estimator[3:6] /
                                        np.linalg.norm(self.paras_estimator[3:6])))
        paras_init = np.append(position, direction)

        # the likelihood function
        likelihood_fuc = OptimizationCollection.gaussian(tau, lam)
        # doing minimization
        opt_result = so.minimize(fun=likelihood_fuc, x0=paras_init, args=hits)
        opt_direction = Analysis.pol2rect(*opt_result["x"][3:5])
        opt_position = opt_result["x"][0:3]
        self.paras_time_residual = np.append(opt_position, opt_direction)
        angle_error = Analysis.angle_err(self.paras_time_residual[3:], self.paras_true[3:])
        # print results
        if flag_verbosity:
            print_rec_result("time residual average", self.paras_time_residual, self.paras_true)
        return angle_error

    def time_residual_spe(self, tau, lam, flag_verbosity=True) -> float:
        """
        Reconstruct using SPE method

        Returen
        -------
        Angle error in minutes
        """
        # data preprocess
        df = self.copy_hits_sum()
        time_mean = df["t0"].mean()
        df["t0"] = df["t0"] - time_mean

        # prepare the first guess parameters
        position = (self.paras_first_guess[0:3] +
                    self.paras_first_guess[3:6] * time_mean)
        direction = Analysis.rect2pol(*(self.paras_first_guess[3:6] /
                                        np.linalg.norm(self.paras_first_guess[3:6])))
        paras_init = np.append(position, direction)

        # the likelihood function
        def calculate_likeli(paras: np.ndarray) -> float:
            paras = np.append(paras[:3], Analysis.pol2rect(*paras[-2:]))
            df_distance = Analysis.calculate_distance(df, paras, "t0")
            t_eff = df_distance["t_r"] / tau
            alpha = df_distance["d"] / lam
            likeli = np.log(alpha) + t_eff - (alpha - 1) * np.log(t_eff)
            return likeli.sum()

        # doing minimization
        if flag_verbosity:
            print("Performing minimation for time residual likelihood method...")
        opt_result = so.minimize(fun=calculate_likeli, x0=paras_init)
        opt_direction = Analysis.pol2rect(*opt_result["x"][3:5])
        opt_position = opt_result["x"][0:3] - time_mean * opt_direction * Constants.c
        self.paras_time_residual = np.append(opt_position, opt_direction)
        angle_error = Analysis.angle_err(self.paras_time_residual[3:], self.paras_true[3:])
        # print results
        if flag_verbosity:
            print_rec_result("time residual SPE", self.paras_time_residual, self.paras_true)
        return angle_error

    def time_residual_mpe(self, hits: pd.DataFrame, mean_vs_tau: float, tau_vs_distance: float, alpha: float, flag_verbosity=True) -> float:
        """
        Reconstruct using MPE method

        Parameter
        ---------
        mean_vs_tau: the PDF is shifted right with mean_vs_tau * tau
        tau_vs_distance: typical time (sigma) = tau_vs_distance * distance
        alpha: represent skewness

        Returen
        -------
        Angle error in minutes
        """
        # data preprocess
        # prepare the first guess parameters
        position = self.paras_estimator[0:3]
        direction = Analysis.rect2pol(*(self.paras_estimator[3:6] /
                                        np.linalg.norm(self.paras_estimator[3:6])))
        paras_init = np.append(position, direction)

        # the likelihood function
        likelihood_func = OptimizationCollection.skew_cauchy(mean_vs_tau, tau_vs_distance, alpha)

        # doing minimization
        if flag_verbosity:
            print("Performing minimation for time residual likelihood method...")
        opt_result = so.minimize(fun=likelihood_func, x0=paras_init, args=hits)
        opt_direction = Analysis.pol2rect(*opt_result["x"][3:5])
        opt_position = opt_result["x"][0:3]
        self.paras_time_residual = np.append(opt_position, opt_direction)
        angle_error = Analysis.angle_err(self.paras_time_residual[3:], self.paras_true[3:])
        # print results
        if flag_verbosity:
            print_rec_result("time residual MPE", self.paras_time_residual, self.paras_true)
        return angle_error

    @staticmethod
    def inspect_likelihood(likelihood_func, hits: pd.DataFrame, paras: np.ndarray, dt: float) -> pd.DataFrame:
        """
        Inspect the profile of the likelihood for the input hits and parameters

        Output
        ------
        A pandas dataframe that has two columns:
        - likelihood at t_res - dt
        - likelihood at t_res
        - likelihood at t_res + dt
        """
        df = Analysis.calculate_distance(hits, paras)
        likeli_pre = likelihood_func(df["t_r"] - dt, df["d"], hits["n"])
        likeli = likelihood_func(df["t_r"], df["d"], hits["n"])
        likeli_aft = likelihood_func(df["t_r"] + dt, df["d"], hits["n"])
        return [likeli_pre, likeli, likeli_aft]
