import numpy as np
import pandas as pd
import uproot as proot


def root2pd(filename: str, treename: str,
            flag_verbose: bool = True) -> pd.DataFrame:
    with proot.open(filename) as file:
        if flag_verbose:
            print("Reading Hists in the ROOT file {:s}, Tree {:s}:"
                  .format(filename, treename))
            print(file.classnames())
        tree = file[treename]
        df = tree.arrays(tree.keys(), library="pd")
        if treename == "DomHit":
            df = df.loc[:, ["DomID", "x0", "y0", "z0", "t0"]]
        elif treename == "PmtHit":
            df = df.loc[:, ["DomID", "PmtID", "x0", "y0", "z0", "t0"]]
        elif treename == "PmtHit":
            df = df.loc[:, ["DomID", "SipmID", "x0", "y0", "z0", "t0"]]
        if flag_verbose:
            print(df.head())
            print(" ")
    return df


if __name__ == "__main__":
    from trident.config import GlobalConfig
    treename = "DomHit"
    GlobalConfig("parents/1")
    hits = root2pd(GlobalConfig.file_data, treename)
    print(hits.head)
