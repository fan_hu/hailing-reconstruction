from trident.config import Config
from trident.reconstruct import Reconstruct

rec = Reconstruct()
Config("electrons")
Config.read_data("1")
rec.read_hits(Config.file_data, "DomHit")

rec.read_paras_true(Config.file_particle)
rec.first_guess(flag_plot=True)
rec.time_residual()