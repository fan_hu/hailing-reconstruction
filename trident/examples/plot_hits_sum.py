from trident.config import Config
from trident.analysis import Analysis

Config("electrons")
Config.read_data("1")
ana = Analysis()

ana.read_hits(Config.file_data, "DomHit")
ana.read_paras_true(Config.file_particle)
ana.set_hits_sum()
ana.plot_hits()