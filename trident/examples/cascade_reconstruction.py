from trident.cascade import Cascade
from trident.config import GlobalConfig, DataConfig
from trident.analysis import Analysis

globalconfig = GlobalConfig("cascade")
dataconfig = DataConfig(globalconfig)
dataconfig.read_data(4)
cas = Cascade(dataconfig)
cas.read_hits("PmtHit", flag_verbose=False)
cas.set_hits_sum("PmtID", method="min")
cas.vertex_first_guess()
cas.vertex_reconstruct("square")
cas.vertex_reconstruct("emg")
print("vertex reconstruction error: ")
print(Cascade.vertex_position_err(cas.vertex_rec, cas.paras_true))

cas.direction_reconstruct("dom")
cas.direction_reconstruct("dom_and_pmt")
print(cas.direction_rect)

cas.read_paras_true()
print(cas.direction_rect.dot(cas.paras_true[3:6]))
Analysis.angle_err(cas.paras_true[3:6], cas.direction_rect)