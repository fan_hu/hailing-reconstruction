from trident.config import Config
from trident.analysis import Analysis

Config("electrons")
Config.read_data("1")
ana = Analysis()

ana.read_electrons(Config.file_data)
ana.plot_electrons()