from trident.analysis import Analysis
from trident.config import Config

Config("electrons")
Config.read_data("6")
ana = Analysis()
ana.read_hits(Config.file_data, "PmtHit")
ana.set_hits_sum(key="PmtID")
ana.read_paras_true(Config.file_particle)
ana.plot_distance_time()
