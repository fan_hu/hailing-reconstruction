from trident.analysis import Analysis
from trident.config import Config, Constants
import pandas as pd
import matplotlib.pyplot as plt

Config("noscattering")
ana = Analysis()
df_tot = pd.DataFrame()
for i in range(1, 7):
    workdir = "noscattering".format(i)
    Config(workdir)
    Config.read_data(str(i))

    ana.read_hits(Config.file_data, "DomHit", False)
    ana.set_hits_sum()
    ana.read_paras_true(Config.file_particle, False)
    df = ana.set_hits_true().reset_index()
    if len(df_tot) == 0:
        df_tot = df
    else:
        df_tot = pd.concat([df_tot, df], ignore_index=True)

plt.hist(df_tot["t_r"], range=(0,200), bins=20)
plt.yscale("log")
df_on_time = df_tot[df_tot["t_r"] < 5]
df_on_time.describe()
df_early = df_tot[(df_tot["t_r"] >= 5) & (df_tot["t_r"] < 20)]
df_early.describe()
df_late = df_tot[(df_tot["t_r"] >= 20) & (df_tot["t_r"] < 100)]
df_late.describe()
df_too_late = df_tot[df_tot["t_r"] >= 100]
df_too_late.describe()

"""
FIXME
df_muons = df_tot[df_tot["parent_PDG"] == 13]
plt.scatter(df_muons["parent_t0"], df_muons["parent_e"])
df_electron = df_tot[df_tot["parent_PDG"] != 13]
df_electron.describe()
"""