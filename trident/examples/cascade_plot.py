from trident.cascade import Cascade
from trident.config import GlobalConfig, DataConfig

globalconfig = GlobalConfig("cascade")
dataconfig = DataConfig(globalconfig)
dataconfig.read_data(1)
cas = Cascade(dataconfig)
cas.read_hits("PmtHit", flag_verbose=False)
cas.read_paras_true()
cas.read_electrons()
cas.align_electrons()
cas.set_hits_sum("PmtID", method="min")

from trident.plots import plot_cascade_3d

plot_cascade_3d(cas.hits_sum, cas.electrons, cas.paras_true, cas.globalconfig, filename="a")