from trident.config import Config
from trident.reconstruct import Reconstruct

Config("electrons")
Config.read_data("1")
rec = Reconstruct()
rec.read_hits(Config.file_data, "DomHit")
rec.first_guess()