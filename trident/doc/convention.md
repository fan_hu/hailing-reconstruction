# Convention

## class

Class names should normally use the CapWords convention.

Methods should be lowercase, with words separated by underscores as necessary to improve readability.

## function

Function names should be lowercase, with words separated by underscores as necessary to improve readability.

## variable

Varaible names should be lowercase, with words separated by underscores as necessary to improve readability.

Using `paras` for parameters.