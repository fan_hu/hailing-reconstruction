from trident.analysis import Analysis
from trident.config import DataConfig
import numpy as np
import pandas as pd
import yaml

class HDOM(Analysis):
    def __init__(self, dataconfig: DataConfig) -> None:
        self.dataconfig = dataconfig
        self.globalconfig = dataconfig.globalconfig

    def read_photon(self):
        with open(self.dataconfig.file_photon_plane, 'r') as f:
            input = yaml.full_load(f)
        self.dataconfig.num_photon_inject = input["num"]
        self.dataconfig.dir_photon_inject = np.array([input["x"], input["y"], input["z"]])

    def read_hits(self) -> None:
        from trident.root2pd import root2pd
        self._hits_pmt = root2pd(self.dataconfig.file_data, "PmtHit", False)
        self._hits_sipm = root2pd(self.dataconfig.file_data, "SipmHit", False)
        return None

    def preprocess_hits(self) -> None:
        """
        0. Remove the unused columns in self._hits
        1. Set the index of self._hits_pmt to be PmtID
        """
        self._hits_pmt = self._hits_pmt[["PmtID", "nx", "ny", "nz", "px", "py", "pz"]]
        self._hits_pmt.set_index(keys="PmtID", drop=True, inplace=True)
        self._hits_sipm = self._hits_sipm[["SipmID", "nx", "ny", "nz", "px", "py", "pz"]]
        self._hits_sipm.set_index(keys="SipmID", drop=True, inplace=True)
        return None

    def sum_hits(self) -> None:
        """
        0. count the number of hits in each PMT
        1. assign the direction of PMT
        """
        hits_sum_pmt = self._hits_pmt.index.value_counts().sort_index().to_frame(name="counts")
        hits_sum_pmt = hits_sum_pmt.reindex(range(self.globalconfig.num_pmt), fill_value=0)
        df_dir_pmt = self.globalconfig.pmt_direction_rect
        hits_sum_pmt = pd.concat([hits_sum_pmt, df_dir_pmt], axis=1)
        hits_sum_pmt["costh"] = (hits_sum_pmt["nx"] * self.dataconfig.dir_photon_inject[0] +
                                 hits_sum_pmt["ny"] * self.dataconfig.dir_photon_inject[1] +
                                 hits_sum_pmt["nz"] * self.dataconfig.dir_photon_inject[2])
        self._hits_pmt_sum = hits_sum_pmt
        hits_sum_sipm = self._hits_sipm.index.value_counts().sort_index().to_frame(name="counts")
        hits_sum_sipm = hits_sum_sipm.reindex(range(self.globalconfig.num_sipm), fill_value=0)
        df_dir_sipm = self.globalconfig.sipm_direction_rect
        hits_sum_sipm = pd.concat([hits_sum_sipm, df_dir_sipm], axis=1)
        hits_sum_sipm["costh"] = (hits_sum_sipm["nx"] * self.dataconfig.dir_photon_inject[0] +
                                  hits_sum_sipm["ny"] * self.dataconfig.dir_photon_inject[1] +
                                  hits_sum_sipm["nz"] * self.dataconfig.dir_photon_inject[2])
        self._hits_sipm_sum = hits_sum_sipm
        return None
