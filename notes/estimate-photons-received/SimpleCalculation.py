import numpy as np

# effective collection area
RadiusDom = 0.15
HalfAngle = 10. / 180. * np.pi
NbOfPmt = 31
QE = 1.0
ReflectionFactor = 1.0
AreaDom = 4 * np.pi * np.power(RadiusDom, 2)
AreaPmt = NbOfPmt * 2*np.pi*np.power(RadiusDom, 2) * (1 - np.cos(HalfAngle))
AreaPmtEffective = AreaPmt * QE * ReflectionFactor

# photon propagation sphere
DistanceDom = 8.
AttanuationLength = 30.
AttanuationFactor = np.exp(-DistanceDom / AttanuationLength)
PropagationSphere = 4*np.pi * np.power(DistanceDom, 2) / AttanuationFactor

# photon collection probability
PmtHitProbability = AreaPmtEffective / PropagationSphere
DomHitProbability = AreaDom / PropagationSphere

# number of photons generated
ParticleEnergy = 100.  # GeV
PropagationLength = ParticleEnergy / 0.2  # m
NbOfPhoton = PropagationLength * 40000.

# number of received photons
NbOfPmtHit = PmtHitProbability * NbOfPhoton
NbOfDomHit = DomHitProbability * NbOfPhoton