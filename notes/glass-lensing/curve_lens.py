import matplotlib.pyplot as plt
import matplotlib
import numpy as np
font = {'family': 'serif',
        'weight': 'normal', 'size': 12}
matplotlib.rc('font', **font)

"""
In the following calculation, n2 = n_2^2 / n_1^2
and r2 = R^2 / r^2
"""


def reflect(costh: float, n2: float) -> float:
    alpha = np.sqrt(n2 + costh*costh - 1)
    return (np.power((costh-alpha)/(costh+alpha), 2)
            + np.power((alpha-n2*costh)/(alpha+n2*costh), 2)) / 2


def dumy(costh: float, n2: float) -> float:
    return 1.


def integrate(costh_min: float, costh_max: float, n2: float) -> float:
    x_list = np.linspace(costh_min, costh_max, 100)
    dx = (costh_max - costh_min) / 100
    sum = 0
    for x in x_list:
        sum += x * (1-reflect(x, n2)) * dx
    return sum


def amp(r2: float, n2: float) -> float:
    if r2 < n2:
        return 2*r2*integrate(0, 1, n2)
    else:
        return 2*r2*integrate(np.sqrt(1-n2/r2), 1, n2)


fig = plt.figure(dpi=700)
ax = fig.add_subplot(111)
for n2 in [1.0, 1.25, 1.5, 1.75, 2.0]:
    r2 = np.linspace(1., 2.5, 200)
    amplify = np.array([amp(r2_, n2) for r2_ in r2])
    ax.plot(r2, amplify, label=r"$n_2^2/n_1^2 = {:.2f}$".format(n2))

ax.legend()
ax.set_xlim(1.0, 2.5)
ax.set_ylim(0.9, 2.0)
ax.set_xlabel(r"$R^2/r^2$")
ax.set_ylabel("amplify")
fig.savefig("curve_lens.png")
