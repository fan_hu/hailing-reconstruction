# Lensing Effect of Pressure Housing Glass

- The glass has a refraction index of about 1.5, which is larger than water of 1.33
- The glass is a spherical object

   **->** The glass sphere can converge lights to its center



## The number of photons received by inner sphere

Assume the photon filed is isotropic, i.e. $I = \frac{\mathrm{d}N}{\mathrm{d}A \mathrm{d}t \mathrm{d}\Omega}$ is constant.

For a sphere of radius $r$, the number of photons it can receive per second is:
$$
\frac{~\mathrm{d}N}{~\mathrm{d}t} = \int I ~\mathrm{d}A_\mathrm{eff} ~\mathrm{d}\Omega = \int I \cos \theta ~\mathrm{d}A ~\mathrm{d}\Omega = \pi I \int \mathrm{d} A = 4 \pi^2 r^2 I
$$
Consider that we have a small sphere with radius $r$ inside a large sphere with radius $R$. The refraction index of the medium (glass) of the shell between two sphere is $n_2$ and the refraction index of medium (water) outside is $n_1$.

Think about there are some photons arrive at the larger sphere and can also arrive to the inner sphere, the threshold value is:
$$
\begin{aligned}
\frac{\sin \theta_2}{\sin \theta_1} = \frac{n_1}{n_2}&，~\sin \theta_1 = \frac{r}{R} \\

\sin \theta_2 = \frac{n_1}{n_2}\frac{r}{R}&,~~~~\mathrm{if}~ R > r\frac{n_2}{n_1} \\
\sin \theta_2 = 1&,~~~~\mathrm{else}
\end{aligned}
$$
We have photon collection area of:
$$
\frac{~\mathrm{d}N}{~\mathrm{d}t} = \int I ~\mathrm{d}A_\mathrm{eff} ~\mathrm{d}\Omega = \int I \cos \theta ~\mathrm{d}A ~\mathrm{d}\Omega = \int^1 _{\cos\theta_\mathrm{max}} I \cos \theta ~\mathrm{d}A \times 2\pi ~\mathrm{d} \cos\theta = 4 \pi^2 R^2I \times 2 \int^1 _{\cos\theta_\mathrm{max}} \cos\theta ~\mathrm{d} \cos\theta
$$
The amplification is:
$$
A(R) = \frac{R^2}{r^2} \times 2 \int^1 _{\cos\theta_\mathrm{max}} \cos\theta ~\mathrm{d} \cos\theta
$$



## The reflectivity in glass water surface

There will be additional cost in reflection at the glass-water surface

Fresnel equations:
$$
R(\theta) = \frac{1}{2} (\frac{\cos\theta - \alpha}{\cos\theta + \alpha} + \frac{\alpha - k^2\cos\theta}{\alpha + k^2\cos\theta}) \\
k = n_2 / n_1,~ \alpha = \sqrt{k^2 - \sin^2\theta}
$$
![](Fresnel.png)

## The total effect

$$
A(R) = \frac{R^2}{r^2} \times 2 \int^1 _{\cos\theta_\mathrm{max}} \cos\theta (1-R(\theta)) ~\mathrm{d} \cos\theta, \\
\begin{aligned}
\cos\theta_\mathrm{max} =\sqrt{1 - (\frac{n_1}{n_2}\frac{r}{R})^2} &, ~~~~\mathrm{if}~  R > r\frac{n_2}{n_1} \\
\cos\theta_\mathrm{max} = 0 &, ~~~~\mathrm{else}~
\end{aligned}
$$



![](curve_lens.png)