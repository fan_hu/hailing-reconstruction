- Using slice to assign value is much faster to assign value individually. See `Analysis.sum_hits`.

- Using groupby to create multiindex data frame. See `Analysis.sum_hits`.

