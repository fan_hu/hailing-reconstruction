For the late arrive photons, they are produced by electrons with nx ~ 0.8.

No energy decrease is found for the parents of late arrive photons.

There are no muons produced in the simulation.

The mean electron energy is 200 MeV, maximum electron energy 

