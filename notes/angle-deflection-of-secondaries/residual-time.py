import matplotlib.pyplot as plt
import numpy as np

theta = np.linspace(0, np.pi/2, 50)
thetaC = np.arccos(1/1.34)
plt.plot(theta, (1-np.cos(theta-thetaC)) / np.sin(theta) / np.cos(thetaC))

plt.xlim(0.2, np.pi/2)
plt.ylim(0, 0.7)
plt.xlabel("theta")
plt.ylabel("time residual / distance")
plt.show()
