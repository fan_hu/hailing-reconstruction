## The impact of angle deflection of secondaries to photon arriving time

Normal Cherenkov photons are emitted with angle $\theta_C$ from the main track. Its arriving time is:
$$
t_g = l - d / \tan \theta_C +d / \sin \theta_C \times n  = l + d \times \tan \theta_C
$$
Assuming a photon emitted from a secondaries electrons, it is originated from the main track, but its angle is not $\theta$: 
$$
t = l - d / \tan \theta +d / \sin \theta \times n  = l + d \times \frac{1 - \cos\theta \cos\theta_C}{\sin\theta \cos\theta_C}
$$
The residual time due to this effect is:
$$
t_\mathrm{res} = t - t_g = d \times \frac{1 - \sin\theta\sin\theta_C -\cos\theta\cos\theta_C}{\sin\theta \cos\theta_C}
$$

But in the data analysis, no distance depended residual time is observed.

*speed of light is set to zero.*