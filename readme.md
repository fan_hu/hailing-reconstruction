0. run `pip install uproot` to install package that can read ROOT file.

1. add the trident package to the PYTHONPATH in `~/.bashrc`:
`export PYTHONPATH=/path/to/trident:$PYTHONPATH`.

2. set up environment variable `TRIDENT_DATA` to the absolute path of the data: `/path/to/data`.

3. run example `plot_hits_sum.py`.