import matplotlib.pyplot as plt
import numpy as np
from scipy.optimize import curve_fit
import pandas as pd

# read CK spectrum data
def read_data(address):
    arr = np.array([])
    with open(address, 'r') as f:
        for line in f.readlines():
            arr = np.append(arr, float(line))
    return arr
sp_CK = read_data('../output/data_spectrum')
length = len(sp_CK)
wl = np.linspace(300, 549, length)

# fit CK specturm
def func(x, a, b):
    return a / (x + b)**2
popt, pcov = curve_fit(func, wl, sp_CK, bounds=([100000, -20], [300000, 20]))

# read hit spectrum data
directory = "/Users/hufan/data/04301507/"
data_hit = pd.DataFrame()
for id in range(0, 100):
    data_hit_temp = pd.read_csv(directory+"hits_"+str(id), sep=",", skiprows=1, names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "tRes", "wl"])
    data_hit = data_hit.append(data_hit_temp, ignore_index=True)
data_wl = data_hit["wl"].to_numpy()


# smoothing data
data_len = len(data_wl)
jetter = np.random.normal(0, 2, 50*data_len)
data_wl_sm = np.tile(data_wl, 50) + jetter


# plot first figure
fig, ax1 = plt.subplots(figsize=(8, 5))
ax1.plot(wl, sp_CK, '-', color='darkred',linewidth = 2)
font1 = {'family': 'serif', 'color':  'darkred', 'weight': 'normal', 'size': 12}
fontx = {'family': 'serif', 'weight': 'normal', 'size': 12}
ax1.set_xlim(300, 550)
ax1.set_xlabel('Wave length (nm)', fontdict=fontx)
ax1.set_ylim(0, 2.5)
ax1.set_ylabel(r'Source spectrum $\frac{d^2 N}{d\lambda dx}$ (Photons/nm/cm)', fontdict=font1)
ax1.tick_params(axis='y', labelcolor='darkred')

# plot second figure
ax2 = ax1.twinx()
[n, bins, patches] = ax2.hist(data_wl_sm, 100, density=True, alpha=0.7)
font2 = {'family': 'serif', 'color':  'royalblue', 'weight': 'normal', 'size': 12}
ax2.set_ylim(0, 0.010)
ax2.set_ylabel("DOM received spectrum distribution", fontdict=font2)
ax2.tick_params(axis='y', labelcolor='royalblue')

plt.show()
