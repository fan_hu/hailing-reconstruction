#!/usr/local/bin/python3
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
import imageio

import numpy as np
import pandas as pd
from pathlib import Path
import RecClass as rec

class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0,0), (0,0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0],ys[0]),(xs[1],ys[1]))
        FancyArrowPatch.draw(self, renderer)


directory = rec.directory
Path(directory+"figs").mkdir(parents=True, exist_ok=True)

# read simulation track
paraSim = rec.read_para(directory, 0, model='sim')
length = np.linspace(0, 1750, 100)
trackX = paraSim[0] + np.sin(paraSim[3]) * np.cos(paraSim[4]) * length
trackY = paraSim[1] + np.sin(paraSim[3]) * np.sin(paraSim[4]) * length
trackZ = paraSim[2] + np.cos(paraSim[3]) * length

# read hit data
data_hit = pd.read_csv(directory + "hits_0", sep=",", skiprows=1, 
    names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "t_res", "wl"])
data_hit = data_hit.sort_values(by="t")
hit_num = np.zeros((20,20,20))
hit_time = np.zeros((20,20,20))
t_min = 10000

# calcuate number and average time of hits
for i in range(20):
    for j in range(20):
        for k in range(20):
            mask_pos = (data_hit["x"] == -475+50*i).values & (data_hit["y"] == -475+50*j).values & (data_hit["z"] == -475+50*k).values
            data_hit_xyz = data_hit[mask_pos]
            hit_num[i][j][k] = data_hit_xyz.shape[0]
            if (hit_num[i][j][k] > 0):
                hit_time[i][j][k] = data_hit_xyz["t"].mean()
                t_min = min(t_min, hit_time[i][j][k])
t_max = hit_time.max()
hit_time_un = (hit_time - t_min) / (6000. - t_min)
# print(t_max)

# init figure and ax
fig = plt.figure()
ax = fig.add_axes([0.1, 0.15, 0.7, 0.7], projection='3d')

# plot DOM
domCoordinate = np.linspace(-475, 475, 20)
[yyyDom, xxxDom, zzzDom] = [array_.flatten() for array_ in np.meshgrid(domCoordinate, domCoordinate, domCoordinate)]
hit_num_flat = hit_num.flatten()
hit_time_un_flat = hit_time_un.flatten()
data_dom_scat = np.array([xxxDom, yyyDom, zzzDom, hit_num_flat, hit_time_un_flat]).transpose()
mask_data_dom_scat = data_dom_scat[:,3] > 0
data_dom_scat_masked = data_dom_scat[mask_data_dom_scat]
ax.scatter(data_dom_scat_masked[:,0], data_dom_scat_masked[:,1], data_dom_scat_masked[:,2], 
            s = np.power(data_dom_scat_masked[:,3], .7) * 10, 
            c = data_dom_scat_masked[:,4], 
            marker = '.', 
            alpha = 1.,
            cmap = 'rainbow_r')

# plot track
ax.plot(trackX, trackY, trackZ, '-', c='xkcd:steel blue')
arrow_prop_dict = dict(mutation_scale=30, arrowstyle='-|>', shrinkA=0, shrinkB=0)
arrow = Arrow3D([trackX[0],trackX[-1]], [trackY[0],trackY[-1]], [trackZ[0],trackZ[-1]], 
                **arrow_prop_dict, color='steelblue')
ax.add_artist(arrow)

# # plot lines
# DELTA = 1e-10  # a small delta to avoid vertical line
# for i in range(10):
#     for j in range(10):
#         starPoint = np.array([-475+100*i, -475+100*j, -475])
#         endPoint  = np.array([-475+100*i+DELTA, -475+100*j+DELTA, 475])
#         dataPoints = np.array([starPoint, endPoint])
#         ax.plot(*dataPoints.transpose(), linestyle='solid', linewidth=1., alpha=.3, c='xkcd:sea blue')

# plot DOM
# domCoordinate = np.linspace(-475, 475, 20)
# [xxxDom, yyyDom, zzzDom] = [array_.flatten() for array_ in np.meshgrid(domCoordinate, domCoordinate, domCoordinate)]
# ax.scatter(xxxDom, yyyDom, zzzDom, s=1., marker='.', alpha=0.8, color='xkcd:steel')


# config axis
font = {'family': 'serif', 'color':  'black', 'weight': 'normal', 'size': 12}
ax.set_xlim(-500, 500)
ax.set_ylim(-500, 500)
ax.set_zlim(-500, 500)
ax.set_xticks((-500, -300, -100, 100, 300, 500))
ax.set_yticks((-500, -300, -100, 100, 300, 500))
ax.set_zticks((-500, -300, -100, 100, 300, 500))
ax.set_xlabel("X coordinate (m)", fontdict=font)
ax.set_ylabel("Y coordinate (m)", fontdict=font)
ax.set_zlabel("Z coordinate (m)", fontdict=font)
ax.grid(False)

# plot color bar
cmap_ = plt.get_cmap('rainbow_r')
# fig.add_axes([left, bottom, width, height])
cax = fig.add_axes([0.83, 0.2, 0.03, 0.6])
norm = mpl.colors.Normalize(vmin=0, vmax=6000)
cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap_,
                                norm=norm,
                                orientation='vertical')
cax.set_ylabel('Time (ns)', size = 12)

plt.show()

# plt.savefig(directory + "figs/Hit" + ".pdf")
# plt.clf()
# plt.close()
