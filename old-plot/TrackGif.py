import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import RecClass as rec
import imageio

directory = rec.directory
Path(directory+"figs").mkdir(parents=True, exist_ok=True)

data_hit = pd.read_csv(directory + "hits_0", sep=",", skiprows=1, names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "t_res", "wl"])
data_hit = data_hit.sort_values(by="t")

# read simulation track
paraSim = rec.read_para(directory, 0, model='sim')
length = np.linspace(0, 1500, 100)
trackX = paraSim[0] + np.cos(paraSim[4]) * length
trackY = paraSim[1] + np.sin(paraSim[4]) * length

hit_sum = 0
for time in range(30):
    hit_temp = np.zeros((20,20))
    mask_t = data_hit["t"].between(250.0 * time, 250.0 * (time+1))
    data_t = data_hit[mask_t]
    for i in range(20):
        for j in range(20):
            mask_pos = (data_t["x"] == -475+50*i).values & ((data_t["y"] == -475+50*j).values)
            hit_temp[i][j] += data_t[mask_pos].shape[0]
    hit_sum += hit_temp
    fig, ax = plt.subplots(figsize=(6, 5))
    for i in range(20):
        xpoints = np.linspace(-475, 475, 20)
        ypoints = (-475+50*i) * np.ones(20)
        plt.scatter(xpoints, ypoints, alpha=0.2, s=0.5, color='gray')
        for j in range(20):
            ax.add_patch(plt.Circle((-475+50*i, -475+50*j), 5, color = 'r', alpha = min(hit_sum[i][j]**0.5/10, 1)))
    ax.set_aspect('equal')
    ax.plot()
    ax.plot(trackX, trackY, '--', c='g')
    font = {'family': 'serif', 'color':  'darkred', 'weight': 'normal', 'size': 14}
    ax.text(-400, 400, 'Time: %d'%(time*250) + ' ns', fontdict=font)
    ax.set_xlim(-500, 500)
    ax.set_ylim(-500, 500)
    plt.xticks((-500, -300, -100, 100, 300, 500), color='darkred', size=14, family='serif', weight='normal')
    plt.yticks((-500, -300, -100, 100, 300, 500), color='darkred', size=14, family='serif', weight='normal')
    ax.set_xlabel("X coordinate (m)", fontdict=font)
    ax.set_ylabel("Y coordinate (m)", fontdict=font)
    plt.grid(True)
    plt.savefig(directory + "figs/Hit_" + str(time) + ".png")
    plt.clf()
    plt.close()
    hit_sum = 0

# convert pngs to gif. See https://stackoverflow.com/questions/753190/programmatically-generate-video-or-animated-gif-in-python
filenames = [directory+"figs/Hit_"+str(time)+".png" for time in range(30)]
images = []
for filename in filenames:
    images.append(imageio.imread(filename))
imageio.mimsave(directory + 'figs/Hit.gif', images, duration=0.5)
