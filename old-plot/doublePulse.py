import numpy as np
import matplotlib.pyplot as plt

def func(x, a, b, c):
    y = a * np.power(x/b, c) * np.exp(-x/b)
    for i in range(len(y)):
        y[i] = max(y[i], 0)
    return y

def funb(x, a, b):
    y = a * np.exp(-1/2*(x/b+np.exp(-x/b)))
    return y

bg = 1

x = np.arange(0, 800, 0.1)
y = func(x-50, 20, 75, 1) + funb(x-500, 5, 50) + bg/2
plt.figure(figsize=(4.5,2.5), dpi=300)
plt.plot(x, y)
plt.axvline(3.3*128, linestyle='-.', color='r')
xShade = np.arange(3.3*128, 800, 0.1)
plt.fill_between(xShade, 0, 10, color='r', alpha=0.2)
plt.text(500, 5, 'out of range', color='darkred', fontfamily='Times', fontsize=15)
plt.xlim(0, 800)
plt.ylim(0, 10)
plt.xlabel('time (ns)')
plt.ylabel('Amplitude')
plt.yticks([])