import matplotlib.pyplot as plt
import numpy as np


for energy_0, color_n in zip([100, 1000, 10000], ['orange', 'blue', 'green']):
    length = np.linspace(0, 2000, 10000)
    energy = 2e-1 / 3.4e-4 * ((energy_0 * 3.4e-4 / 2e-1 + 1) * np.exp(-3.4e-4 * length) - 1)
    energy_un = energy_0 * 3.4e-4 / 2e-1
    length_un = 3.4e-4 * length
    length_expand = ((1 + 1 / energy_un) * length_un + (1 / 2 / energy_un + 1 / 2 / energy_un**2) * np.power(length_un, 2))
    l_eff = 1.172 + 0.023 * np.log(energy)
    l_eff_fit = 1.172 + 0.023 * np.log(energy_0) - 0.023 * length_expand
    plt.plot(length, l_eff, label = r"$E_0$ = %.2g GeV" % (energy_0), color = color_n)
    plt.plot(length, l_eff_fit, '-.', label = r"$E_0$ = %.2g GeV, fit" % (energy_0), color = color_n)

plt.xlabel('Propagation length (m)')
plt.ylabel("Effective length factor")
plt.ylim(1.0, 1.5)
plt.legend()
plt.show()