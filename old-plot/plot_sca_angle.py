import matplotlib.pyplot as plt
import numpy as np

def read_data(address):
    arr = np.array([])
    with open(address, 'r') as f:
        for line in f.readlines():
            arr = np.append(arr, float(line))
    return arr
        
data = read_data('../output/data_sca_angle')
[n, bins, patches] = plt.hist(data, 50, density=True, alpha=0.75)
plt.xlabel(r'$\cos \theta$')
plt.xlim(-1, 1)
plt.ylabel('Probability')
plt.yscale('log', nonposy='clip')
plt.show()