import matplotlib.pyplot as plt
import numpy as np

wl_IC = np.arange(300, 600, 1)
wl_Bk = np.arange(350, 675)
sc_coe_IC = 0.05 * np.power(wl_IC/400, -0.9)
sc_IC = 1 / sc_coe_IC
sc_coe_Bk = 0.002 * np.power(wl_Bk/500, -0.5)
sc_Bk = 1 / sc_coe_Bk

plt.figure(figsize=(5,4))
plt.plot(wl_IC, sc_IC, label=r'IceCube $\times 20$')
plt.plot(wl_Bk, sc_Bk, label='Baikal GVD, KM3NeT Italy')
plt.xlabel('Wave length (nm)')
plt.ylabel('Effective scattering length (m)')
plt.yscale('log')
plt.ylim(1, 1000)
plt.xlim(300, 675)
plt.legend(loc='lower right')
plt.grid(True)
plt.show()