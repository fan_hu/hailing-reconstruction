import matplotlib.pyplot as plt
import numpy as np

data_x = np.logspace(1, 6, 10000)
data_y = 1.0 / 3.4e-4 * np.log(data_x * 3.4e-4 / 2e-1 + 1.0)
data_y_half = 1.0 / 3.4e-4 * np.log((data_x * 3.4e-4 / 2e-1 + 1.0) / (data_x * 3.4e-4 / 2e-1 / 2.0 + 1.0))
plt.loglog(data_x, data_y, label = "Propagation length")
plt.loglog(data_x, data_y_half, label = "Half energy length")
plt.xlabel(r'$\mu$ eneggy (GeV)')
plt.ylabel(r'$\mu$ propagation length (m)')
plt.legend()
plt.show()