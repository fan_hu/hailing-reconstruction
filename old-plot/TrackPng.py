import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import imageio
import RecClass as rec
import matplotlib as mpl



directory = rec.directory
Path(directory+"figs").mkdir(parents=True, exist_ok=True)

# read simulation track
paraSim = rec.read_para(directory, 0, model='sim')
length = np.linspace(0, 1500, 100)
trackX = paraSim[0] + np.cos(paraSim[4]) * length
trackY = paraSim[1] + np.sin(paraSim[4]) * length

# read hit data
data_hit = pd.read_csv(directory + "hits_0", sep=",", skiprows=1, \
    names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "t_res", "wl"])
data_hit = data_hit.sort_values(by="t")
hit_num = np.zeros((20,20))
hit_time = np.zeros((20,20))
t_min = 10000

# calcuate number and average time of hits
for i in range(20):
    for j in range(20):
        mask_pos = (data_hit["x"] == -475+50*i).values & (data_hit["y"] == -475+50*j).values
        data_hit_xy = data_hit[mask_pos]
        hit_num[i][j] = data_hit_xy.shape[0]
        if (hit_num[i][j] > 0):
            hit_time[i][j] = data_hit_xy["t"].mean()
            t_min = min(t_min, hit_time[i][j])
t_max = hit_time.max()
hit_time_un = (hit_time - t_min) / 4000

# plot circles
fig, ax = plt.subplots(1, 1, figsize=(7.5, 6))
plt.subplots_adjust(left=0.1, right=0.85, top=0.9, bottom=0.1)
for i in range(20):
    for j in range(20):
        if (hit_num[i][j] > 0):
            colorPair = mpl.cm.cool(hit_time_un[i][j])
            ax.add_patch(plt.Circle((-475+50*i, -475+50*j), hit_num[i][j]**0.5*1.5, color=colorPair))
ax.plot(trackX, trackY, '--', c='g')

# config axis
ax.set_aspect('equal')
ax.plot()
font = {'family': 'serif', 'color':  'darkred', 'weight': 'normal', 'size': 12}
ax.set_xlim(-500, 500)
ax.set_ylim(-500, 500)
plt.xticks((-500, -300, -100, 100, 300, 500), color='darkred', size=12, family='serif', weight='normal')
plt.yticks((-500, -300, -100, 100, 300, 500), color='darkred', size=12, family='serif', weight='normal')
ax.set_xlabel("X coordinate (m)", fontdict=font)
ax.set_ylabel("Y coordinate (m)", fontdict=font)
plt.grid(True)

#plot color bar
cax = fig.add_axes([0.85, 0.1, 0.05, 0.8])
cmap = mpl.cm.cool
norm = mpl.colors.Normalize(vmin=0, vmax=4000)
cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap,
                                norm=norm,
                                orientation='vertical')
cax.set_ylabel('Time (ns)', size = 12)

plt.savefig(directory + "figs/Hit" + ".pdf")
plt.clf()
plt.close()
