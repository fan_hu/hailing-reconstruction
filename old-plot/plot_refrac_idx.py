import matplotlib.pyplot as plt
import numpy as np

wl = np.arange(300, 600, 1)
wl = wl / 1000
phase = 1.55749 - 1.57988*wl +3.99993*np.power(wl,2) - 4.68271*np.power(wl,3) + 2.09354*np.power(wl,4)
group = phase * (1.227106 - 0.954648*wl + 1.42568*np.power(wl,2) - 0.711832*np.power(wl,3))


plt.plot(wl * 1000, group, label="Group")
plt.plot(wl * 1000, phase, label="Phase")
plt.xlabel('Wave length (nm)')
plt.ylabel('Refraction index')
plt.ylim(1.3, 1.42)
plt.legend()
plt.grid(True)
plt.show()