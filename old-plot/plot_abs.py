import matplotlib.pyplot as plt
import numpy as np

wl_IC = np.arange(300, 600, 1)
wl_Bk = np.arange(375, 675, 1)
wl_It = np.arange(400, 700, 1)
ab_coe_IC = 0.01 * np.power(wl_IC/400, -1.1) + 7000 * np.exp(-7000/wl_IC)
ab_IC = 1/ ab_coe_IC
ab_coe_Bk = 180 * np.exp(-9.5*wl_Bk/500) + 7e-7*np.exp(10*wl_Bk/500)
ab_Bk = 1 / ab_coe_Bk
ab_coe_It = 180 * np.exp(-10*wl_It/450) + 1.5e-6*np.exp(8.5*wl_It/450)
ab_It = 1 / ab_coe_It

plt.figure(figsize=(5,4))
plt.plot(wl_IC, ab_IC, label='IceCube')
plt.plot(wl_Bk, ab_Bk, label='Baikal GVD')
plt.plot(wl_It, ab_It, label='KM3NeT Italy')
plt.xlabel('Wave length (nm)')
plt.ylabel('Absorption length (m)')
plt.legend()
plt.grid(True)
plt.ylim(0, 110)
plt.xlim(300, 700)
plt.show()