import math

c = 0.2998
n_water = 1.34  # for pure water
n_sea = 1.375  # for sea water
n_ice = 1.370  # for ice
c_n = c / n_sea
costh = 1 / n_sea
tanth = math.sqrt(1 - costh*costh) / costh
sinth = costh * tanth
