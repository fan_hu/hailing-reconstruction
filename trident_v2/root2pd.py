import numpy as np
from numpy.matrixlib.defmatrix import matrix
import pandas as pd
import uproot


def root2pd(path_data_file: str, tree_name: str,
            flag_verbose: bool = True) -> pd.DataFrame:
    with uproot.open(path_data_file) as file:
        tree = file[tree_name]
        df = tree.arrays(tree.keys(), library="pd")
        if tree_name == "DomHit":
            df = df.loc[:, ["DomId", "x0", "y0", "z0", "t0"]]
        elif tree_name == "PmtHit":
            df = df.loc[:, ["DomId", "PmtId", "x0", "y0", "z0", "t0"]]
        elif tree_name == "SipmHit":
            df = df.loc[:, ["DomId", "Sipmid", "x0", "y0", "z0", "t0"]]
    df.set_index("DomId", inplace=True)
    df.index = df.index.astype("int32")
    return df


def data_reader(path_data_file: str, tree_name: str,
                entry: int = 0) -> pd.DataFrame:
    """
    Data reader for ICRC 2021 version.
    """
    array = {"nx": 10, "ny": 10, "nz": 50, "dx": 100, "dy": 100, "dz": 20}

    with uproot.open(path_data_file) as file:
        tree = file[tree_name]
        df = tree.arrays(library="pd", entry_start=entry, entry_stop=entry+1)
    df = df.loc[entry, ["DomId", "t0", "e0"]]
    nx = (df["DomId"] / (array["ny"] * array["nz"])).astype("int16")
    ny = ((df["DomId"] - nx * (array["ny"] * array["nz"])) / array["nz"]).astype("int16")
    nz = (df["DomId"] - nx * (array["ny"] * array["nz"]) - ny * array["nz"]).astype("int16")
    df["x0"] = (nx - (array["nx"] - 1) / 2) * array["dx"]
    df["y0"] = (ny - (array["ny"] - 1) / 2) * array["dy"]
    df["z0"] = (nz - (array["nz"] - 1) / 2) * array["dz"]
    df.set_index("DomId", inplace=True)
    df.index = df.index.astype("int32")
    df.sort_values("t0")
    return df

def primary_reader(path_data_file: str,
                   entry: int = 0) -> pd.DataFrame:
    """
    Data reader for ICRC 2021 version.
    """
    with uproot.open(path_data_file) as file:
        tree = file["Primary"]
        df = tree.arrays(library="pd", entry_start=entry, entry_stop=entry+1)
    if df.loc[(entry, 0), "PdgId"] == 13:
        df = df.loc[(entry, 0), ["x0", "y0", "z0", "px", "py", "pz", "e0"]]
    else:
        print("Particle not muon!")
        raise ValueError
    return df
