"""
Statistical analysis of augular resolution of reconstruction
Combine analysis of SiPM hits and PMT hits
1. Sample PMT hits from DOM hits and do reconstruction
2. Add SiPM hits
3. If a DOM is fired by SiPM hits, it will use SiPM hit time
3.1 Set PMT ID and SiPM ID when sampling hits
3.2 
4. Do reconstruction
"""

from trident_v2.config import Config
from trident_v2.track import Track
from trident_v2.hits import Hits
from trident_v2.plots import plot_track_3d
from trident_v2.reconstruct import LineFit, Estimator, ResidualTimeMPE, ResidualTimeSPE, ResidualTimeSpline
from trident_v2.hits_selector import SelectByResidualTime, SelectByPmtId
from tqdm import tqdm
import pandas as pd
import numpy as np

# configure
num_data = 7000
config = Config("ICRC")
config.file_data = "/home/lab110/HL/analysis/data/ICRC/ice_1TeV_6000.root"

# output
output_pmt = pd.DataFrame(columns=["num_hits", "line_fit", "estimator", "likelihood"],
                          index=pd.Index(np.arange(0, num_data), dtype="int32", name="idx"),
                          dtype="float32")
output_sipm_toy = output_pmt.copy()
output_hyb = output_pmt.copy()
list_name = ["pmt", "sipm_toy", "hybrid"]
list_output = [output_pmt, output_sipm_toy, output_hyb]

model_itp = config.get_interpolate_ice()

for i in tqdm(range(num_data)):
    # read hits and track
    try:
        hits0 = Hits.read_data(config.file_data, "PmtHit", i, config.get_pmt_pde_function())
    except:
        print(f"No hits in PMT for event: {i:d}")
        continue
    try:
        hits1 = Hits.read_data(config.file_data, "SipmHit", i, config.get_sipm_pde_function(debug=True))
    except:
        print(f"No hits in SiPM for event: {i:d}")
        continue
    track_true = Track.read_truth("ROOT", config.file_data, i)
    
    if (len(hits0) < 35 or len(hits1) < 10):
        print(f"Too little hits for event: {i:d}, hits0: {len(hits0)}, hits1: {len(hits1)}")
        continue
    hits_pmt = Hits(hits0.data.copy())
    hits_pmt.spread(3.0)
    hits_pmt_first = hits_pmt.merge("min")
    # plot_track_3d(hits_pmt_first.data, track_true.paras, 1200, "name")
    hits_sipm_toy = Hits(hits0.data.copy())
    hits_sipm_toy_first = hits_sipm_toy.merge("min")
    hits_hyb = hits1.append(hits_pmt, False)
    hits_hyb_first = hits_hyb.merge("min")

    list_hits = [hits_pmt, hits_sipm_toy, hits_hyb]
    list_hits_first = [hits_pmt_first, hits_sipm_toy_first, hits_hyb_first]

    for mode in range(3):
        hits = list_hits[mode]
        hits_first = list_hits_first[mode]
        name = list_name[mode]
        output = list_output[mode]
        
        output.loc[i, "num_hits"] = len(hits)

        # line fit as first guess
        first_guess = LineFit(hits)
        track_first_guess = Track.first_guess(first_guess)
        output.loc[i, "line_fit"] = track_true.angle_error(track_first_guess)

        # m-estimator
        estimator = Estimator(hits_first)
        track_estimator = track_first_guess.reconstruct(estimator)
        output.loc[i, "estimator"] = track_true.angle_error(track_estimator)

        """
        t_res = Hits.get_residual(hits.get_numpy_data(), track_true.paras)[2]
        plt.hist(t_res, range=(0,50), bins=10)

        t_res_est = Hits.get_residual(hits.get_numpy_data(), track_estimator.paras)[2]
        plt.hist(t_res_est, range=(0,50), bins=10)
        """

        # clean hits
        selection = SelectByResidualTime(track_estimator, 300)
        hits_clean = hits_first.purge(selection)

        # likelihood method
        # likelihood = ResidualTimeMPE(hits_clean, "sea_water_new_cdf")
        likelihood = ResidualTimeSpline(hits_clean, model_itp)
        # likelihood = Estimator(hits_clean)
        
        track_lkh = track_estimator.reconstruct(likelihood)
        output.loc[i, "likelihood"] = track_true.angle_error(track_lkh)
        if not likelihood.result["success"]:
            print(f"reconstrcut not success for event: {i:d}")
            output.loc[i, "likelihood"] = np.nan

for mode in range(3):
    name = list_name[mode]
    output = list_output[mode]
    output.to_csv(f"output_{name}.csv", sep=",")