import pandas as pd
import numpy as np
import matplotlib as mpl
import matplotlib.pyplot as plt
font = {'family': 'serif', 'weight': 'normal', 'size': 10}
mpl.rc('font', **font)


def read_data(file_name: str, key="likelihood") -> pd.Series:
    data = pd.read_csv(file_name, sep=",").loc[:, key]
    data.dropna(inplace=True)
    num_trunc = int(len(data) * 0.95)
    data = data.sort_values().iloc[0:num_trunc]
    return data


def get_describe(data: pd.DataFrame) -> np.ndarray:
    return data.describe().to_numpy()[3:-1]


names = ["pmt", "sipm_toy", "hybrid"]
xmax = 60
setting_hist = {"bins": 20, "density": True, "range": (0, xmax),
                "histtype": "step", "linewidth": 1.5}
# single plot
for name in names:
    data = read_data(f"water_v1/output_{name}.csv")
    density = plt.hist(data, **setting_hist)[0]
    plt.xlim(0, xmax)
    plt.ylim(0, 0.18)
    plt.title(name)
    plt.text(12.0, 0.17, f"mean: {data.mean():.2f}")
    plt.text(12.0, 0.155, f"std: {data.std():.2f}")
    plt.text(12.0, 0.14, f"median: {data.median():.2f}")
    plt.show()
    plt.close()

# combine plot


########################
# single plots
pmt_sipm_water = read_data("water_v1/output_hybrid.csv", key="likelihood")
pmt_sipm_water /= 60
pmt_water = read_data("water_v1/output_pmt.csv", key="likelihood")
pmt_water /= 60
sipm_water = read_data("water_v1/output_sipm_toy.csv", key="likelihood")
sipm_water /= 60

pmt_sipm_ice = read_data("ice_v2/output_hybrid.csv", key="likelihood")
pmt_sipm_ice /= 60
pmt_ice = read_data("ice_v2/output_pmt.csv", key="likelihood")
pmt_ice /= 60
sipm_ice = read_data("ice_v2/output_sipm_toy.csv", key="likelihood")
sipm_ice /= 60

#######################
# multi plots
def plot_data(ax, data: np.ndarray, xmax, ymax, shift: float, color: str, label: str):
    n, _, _ = ax.hist(data, range=(0, xmax), color=color, label=label, **setting_hist)
    median = np.median(data)
    ax.vlines(median, 0, ymax, color=color, **setting_vline)
    ax.text(median+0.005, n.max() + shift, f"{median:.3f}", color=color)

font_title = {'family': 'serif', 'weight': 'normal', 'size': 12}

fig = plt.figure(figsize=(9.0, 3.5), dpi=300)
ax0, ax1 = fig.subplots(1, 2)
setting_hist = {"bins": 40, "density": True,
                "histtype": "step", "linewidth": 1.5}
setting_vline = {"linestyles": "-.", "alpha": 0.7, "linewidth": 1.0}
xmax0, ymax0 = 0.4, 12
plot_data(ax0, pmt_sipm_water, xmax0, ymax0, 0.5,"#4b5cc4", "hDOM")
plot_data(ax0, pmt_water, xmax0, ymax0, 0.5, "#16a951", "PMT DOM")
plot_data(ax0, sipm_water, xmax0, ymax0, 0.0, "#c9171e", "toy SiPM DOM")
ax0.set_xlim(0, xmax0)
ax0.set_ylim(0, ymax0)
ax0.text(0.25, 11, "Sea Water", fontdict=font_title)
ax0.set_xlabel("Angle Error [degree]")
ax0.set_ylabel("PDF")
ax0.legend(loc='lower left', bbox_to_anchor=(-0.05, 1.02), ncol=3)

xmax1, ymax1 = 1, 5
plot_data(ax1, pmt_sipm_ice, xmax1, ymax1, -1.0, "#4b5cc4", "hDOM")
plot_data(ax1, pmt_ice, xmax1, ymax1, -1.0, "#16a951", "PMT DOM")
plot_data(ax1, sipm_ice, xmax1, ymax1, -1.0, "#c9171e", "toy SiPM DOM")
ax1.set_xlim(0, xmax1)
ax1.set_ylim(0, ymax1)
ax1.text(0.65, 4.5, "Glacial Ice", fontdict=font_title)
ax1.set_xlabel("Angle Error [degree]")

fig.savefig("compare.pdf")
