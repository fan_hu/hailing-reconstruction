from trident_v2.config import Config
from trident_v2.track import Track
from trident_v2.hits import Hits
from trident_v2.reconstruct import LineFit, Estimator, ResidualTimeMPE, ResidualTimeSPE
from trident_v2.hits_selector import SelectByResidualTime
from trident_v2.plots import plot_track_3d
import matplotlib.pyplot as plt

# configure
config = Config("ICRC")
config.file_data = "/home/lab110/HL/analysis/data/ICRC/sum_data_1TeV_100.root"
idx_event = 3
hit_type = "DomHit"
# read hits and track



hits = Hits.read_data(config.file_data, hit_type, idx_event)
hits_first = hits.merge("min")
track_true = Track.read_truth("ROOT", config.file_data, idx_event)

# line fit as first guess
first_guess = LineFit(hits)
track_first_guess = Track.first_guess(first_guess)
print(track_true.angle_error(track_first_guess))

# m-estimator
estimator = Estimator(hits)
track_estimator = track_first_guess.reconstruct(estimator)
print(track_true.angle_error(track_estimator))

# clean hits
selection = SelectByResidualTime(track_estimator, 10)
hits_clean = hits_first.purge(selection)

"""
estimator = Estimator(hits_clean)
track_estimator = track_first_guess.reconstruct(estimator)
print(track_true.angle_error(track_estimator))

# show m-estimator
d, _, t_res = Hits.get_residual(hits_clean.get_numpy_data(), 
                                track_true.paras)
pdf = estimator.pdf(t_res, d, hits_clean.data["n"].to_numpy())
plt.hist(t_res)
"""

# likelihood method
likelihood = ResidualTimeSPE(hits_clean, "sea_water_new_cdf")
track_lkh = track_estimator.reconstruct(likelihood)
print(track_true.angle_error(track_lkh))

# plot_track_3d(hits_first.data, track_true.paras, 500, "track_3d")