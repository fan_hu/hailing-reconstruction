import pickle
import numpy as np
import pandas as pd
from scipy.interpolate import CubicSpline
from os import environ
from pathlib import Path


class Config:
    """
    This class stroe the global file path and configure files.
    It includes the configure of DOM array and intra DOM geometry settings
    """

    def __init__(self, dir_data_sets: str) -> None:
        """
        Initialize Config with the path of your data sets.
        The path should be relative path to `TRIDENT_DATA`.
        """

        self.initialized: bool

        if not hasattr(self, "initialized"):
            self.initialized = True
        else:
            print("re-initialize global configure!")
        path_data_env = Path(environ["TRIDENT_DATA"])
        self.dir_data_sets = path_data_env / dir_data_sets
        self.file_data = self.dir_data_sets / "data.root"
        self.pos_range = {
            "minX": - 100 * (10-1)/2,
            "minY": - 100 * (10-1)/2,
            "minZ": - 20 * (50-1)/2,
            "maxX": 100 * (10-1)/2,
            "maxY": 100 * (10-1)/2,
            "maxZ": 20 * (50-1)/2,
        }
        self.num_dom = 10 * 10 * 50
        self.dom_radius = 0.215
        return None

    def read_pmt_direction(self) -> pd.DataFrame:
        filename = self.path_dir_data_sets / "pmt_direction.csv"
        self.pmt_direction = pd.read_csv(filename, index_col=0)
        self.num_pmt = len(self.pmt_direction)
        return self.pmt_direction

    def set_pmt_direction_rect(self) -> pd.DataFrame:
        self.pmt_direction_rect = pd.DataFrame()
        self.pmt_direction_rect["nx"] = np.sin(self.pmt_direction["theta"]) * np.cos(self.pmt_direction["phi"])
        self.pmt_direction_rect["ny"] = np.sin(self.pmt_direction["theta"]) * np.sin(self.pmt_direction["phi"])
        self.pmt_direction_rect["nz"] = np.cos(self.pmt_direction["theta"])
        return self.pmt_direction_rect

    def read_sipm_direction(self) -> pd.DataFrame:
        filename = self.path_dir_data_sets / "sipm_direction.csv"
        self.sipm_direction = pd.read_csv(filename, index_col=0)
        self.num_sipm = len(self.sipm_direction)
        return self.sipm_direction

    def set_sipm_direction_rect(self) -> pd.DataFrame:
        self.sipm_direction_rect = pd.DataFrame()
        self.sipm_direction_rect["nx"] = np.sin(self.sipm_direction["theta"]) * np.cos(self.sipm_direction["phi"])
        self.sipm_direction_rect["ny"] = np.sin(self.sipm_direction["theta"]) * np.sin(self.sipm_direction["phi"])
        self.sipm_direction_rect["nz"] = np.cos(self.sipm_direction["theta"])
        return self.sipm_direction_rect

    def get_pmt_pde_function(self):
        path_data = "/home/lab110/HL/analysis/trident_v2/config/PMT_QE.csv"
        data = pd.read_csv(path_data, names=["wl", "flux"])
        data.sort_values("wl", inplace=True)
        cs = CubicSpline(data["wl"], data["flux"])
        normlize1 = 0.28 / (cs(405) * 1.24 / 405)
        normlize2 = 0.20 / (cs(470) * 1.24 / 470)  # Why not same as normlize1
        normalize = (normlize1 + normlize2) / 2.0
        normalize *= 0.9  # for photon collection efficiency
        def cs_warp(wl: np.ndarray):
            return normalize * cs(wl) / wl * 1.24
        return cs_warp

    def get_sipm_pde_function(self, debug=False):
        path_data = "/home/lab110/HL/analysis/trident_v2/config/SiPM_PDE.csv"
        data = pd.read_csv(path_data, names=["wl", "pde"])
        data.sort_values("wl", inplace=True)
        cs = CubicSpline(data["wl"], data["pde"])
        def cs_warp(wl: np.ndarray):
            if debug:
                return 2 * cs(wl) / 100
            else:
                return cs(wl) / 100
        return cs_warp

    def get_interpolate_sea_water(self):
        path_data = "/home/lab110/HL/analysis/trident_v2/config/interpolate_sea_water"
        with open(path_data, "rb") as f:
            interplolate_model = pickle.load(f)
        return interplolate_model

    def get_interpolate_ice(self):
        path_data = "/home/lab110/HL/analysis/trident_v2/config/interpolate_ice"
        with open(path_data, "rb") as f:
            interplolate_model = pickle.load(f)
        return interplolate_model
