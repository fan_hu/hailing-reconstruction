from trident_v2.config import Config
import numpy as np
import pandas as pd
import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.patches import FancyArrowPatch
from mpl_toolkits.mplot3d import proj3d
font = {'family': 'serif',
        'weight': 'normal', 'size': 12}
mpl.rc('font', **font)


class Arrow3D(FancyArrowPatch):
    def __init__(self, xs, ys, zs, *args, **kwargs):
        FancyArrowPatch.__init__(self, (0, 0), (0, 0), *args, **kwargs)
        self._verts3d = xs, ys, zs

    def draw(self, renderer):
        xs3d, ys3d, zs3d = self._verts3d
        xs, ys, zs = proj3d.proj_transform(xs3d, ys3d, zs3d, renderer.M)
        self.set_positions((xs[0], ys[0]), (xs[1], ys[1]))
        FancyArrowPatch.draw(self, renderer)


def plot_track_3d(hits: pd.DataFrame, paras_true: np.ndarray, 
                    size_tel: float, filename: str, ) -> None:
    """
    `hits: pd.DataFrame`
        Contains following columns:
        - "x0", "y0", "z0": position of DOM being hit
        - "t0": time of hits, can be earlist time or average time
        - "n": number of hits
    
    `size_tel: float`
        Size of neutrino telescope.
    
    `paras_true: np.ndarray`
        The simulation truth of the track. It is composed as [x0, y0, z0, nx, ny, nz]
    """
    fig = plt.figure(figsize=(9, 9))
    # fig.set_dpi(700)
    # fig.add_axes([left, bottom, width, height])
    ax = fig.add_axes([0.07, 0.15, 0.7, 0.7], projection='3d')

    ax.scatter(hits["x0"], hits["y0"], hits["z0"],
               s=np.power(hits["n"], .5) * 6.,
               c=hits["t0"],
               marker='.',
               alpha=1.,
               cmap='rainbow_r')

    if (len(paras_true) != 0):
        length = np.linspace(0, 1000, 100)
        trackX = paras_true[0] + paras_true[3] * length
        trackY = paras_true[1] + paras_true[4] * length
        trackZ = paras_true[2] + paras_true[5] * length
        ax.plot(trackX, trackY, trackZ, '-', c='xkcd:steel blue')
        arrow_prop_dict = dict(
            mutation_scale=30, arrowstyle='-|>', shrinkA=0, shrinkB=0)
        arrow = Arrow3D([trackX[0], trackX[-1]], [trackY[0], trackY[-1]], [trackZ[0], trackZ[-1]],
                        **arrow_prop_dict, color='steelblue')
        ax.add_artist(arrow)

    # config axis
    space = size_tel / 6
    ax.set_xlim(-size_tel-space, size_tel+space)
    ax.set_ylim(-size_tel-space, size_tel+space)
    ax.set_zlim(-size_tel-space, size_tel+space)
    ax.set_xticks(np.linspace(-size_tel, size_tel, 6))
    ax.set_yticks(np.linspace(-size_tel, size_tel, 6))
    ax.set_zticks(np.linspace(-size_tel, size_tel, 6))
    ax.set_xlabel("X coordinate (m)")
    ax.set_ylabel("Y coordinate (m)")
    ax.set_zlabel("Z coordinate (m)")
    ax.grid(False)

    # plot color bar
    cmap_ = plt.get_cmap('rainbow_r')
    # fig.add_axes([left, bottom, width, height])
    cax = fig.add_axes([0.83, 0.2, 0.03, 0.6])
    length = int(len(hits) / 20)
    hits.sort_values("t0", inplace=True)
    minT = hits["t0"].iloc[length]
    maxT = hits["t0"].iloc[-length-1]
    norm = mpl.colors.Normalize(vmin=minT, vmax=maxT)
    cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap_,
                                    norm=norm,
                                    orientation='vertical')
    cax.set_ylabel('Time (ns)', size=12)
    plt.show()
    # plt.savefig(filename)
    return None


def plot_cascade_3d(hits: pd.DataFrame, electrons: pd.DataFrame, paras_true: np.ndarray,
                    size_tel: float, filename: str) -> None:
    fig = plt.figure(figsize=(9, 9))
    # fig.set_dpi(700)
    # fig.add_axes([left, bottom, width, height])
    ax = fig.add_axes([0.07, 0.15, 0.7, 0.7], projection='3d')
    ax.scatter(hits["x0"], hits["y0"], hits["z0"],
               s=np.power(hits["n"], .7) * 10.,
               c=hits["t0"],
               marker='.',
               alpha=1.,
               cmap='rainbow_r')

    length = np.linspace(electrons["l0"].min(), electrons["l0"].max(), 10)
    l_delta = (length[-1] - length[0]) / 5.
    trackX = paras_true[0] + paras_true[3] * length
    trackY = paras_true[1] + paras_true[4] * length
    trackZ = paras_true[2] + paras_true[5] * length
    trackX_plus = paras_true[0] + paras_true[3] * (length+l_delta)
    trackY_plus = paras_true[1] + paras_true[4] * (length+l_delta)
    trackZ_plus = paras_true[2] + paras_true[5] * (length+l_delta)

    ax.plot(trackX, trackY, trackZ, '-', c='xkcd:steel blue')
    arrow_prop_dict = dict(mutation_scale=10, arrowstyle='-|>', shrinkA=0, shrinkB=0)
    arrow = Arrow3D([trackX_plus[0], trackX_plus[-1]],
                    [trackY_plus[0], trackY_plus[-1]],
                    [trackZ_plus[0], trackZ_plus[-1]],
                    **arrow_prop_dict, color='steelblue')
    ax.add_artist(arrow)

    # config axis
    # ax.set_xlim(dom_rg["minX"]-space, dom_rg["maxX"]+space)
    # ax.set_ylim(dom_rg["minY"]-space, dom_rg["maxY"]+space)
    # ax.set_zlim(dom_rg["minZ"]-space, dom_rg["maxZ"]+space)
    # ax.set_xticks(np.linspace(dom_rg["minX"], dom_rg["maxX"], 6))
    # ax.set_yticks(np.linspace(dom_rg["minY"], dom_rg["maxY"], 6))
    # ax.set_zticks(np.linspace(dom_rg["minZ"], dom_rg["maxZ"], 6))
    ax.set_xlabel("X coordinate (m)")
    ax.set_ylabel("Y coordinate (m)")
    ax.set_zlabel("Z coordinate (m)")
    ax.grid(False)

    # plot color bar
    cmap_ = plt.get_cmap('rainbow_r')
    # fig.add_axes([left, bottom, width, height])
    cax = fig.add_axes([0.83, 0.2, 0.03, 0.6])
    length = int(len(hits) / 20)
    hits.sort_values("t0", inplace=True)
    minT = hits["t0"].iloc[length]
    maxT = hits["t0"].iloc[0]
    norm = mpl.colors.Normalize(vmin=minT, vmax=maxT)
    cb1 = mpl.colorbar.ColorbarBase(cax, cmap=cmap_,
                                    norm=norm,
                                    orientation='vertical')
    cax.set_ylabel('Time (ns)', size=12)
    plt.show()
    # plt.savefig(filename)
    return None


def plot_distance_time(distance: np.ndarray, time_res: np.ndarray, num: np.ndarray, filename: str,
                       tau: float = 30., lam: float = 30., flag_plot_paras: bool = False):
    mpl.rcParams.update({'figure.autolayout': True})
    fig = plt.figure(figsize=(7, 5.5))
    fig.set_dpi(250)
    ax = fig.add_subplot(111)
    ax.scatter(distance, time_res, s=np.power(num, .5)*4.,
               c=np.power(num, .2)*4.,
               cmap="autumn",
               alpha=0.5)

    if flag_plot_paras:
        distance_fit = np.linspace(distance.min(), distance.max(), 100)
        time_fit = distance_fit * tau / lam
        ax.plot(distance_fit, time_fit)

    ax.set_xlabel("distance to track (m)")
    ax.set_ylabel("time residual (ns)")
    fig.savefig(filename)


def plot_electrons(df: pd.DataFrame, filename: str) -> None:
    fig = plt.figure(figsize=(9, 9))
    fig.set_dpi(700)
    ax = fig.add_subplot(111, projection='3d')
    ax.scatter(df["x0"], df["y0"], df["z0"],
               s=np.power(df["n"], 0.5) * 2.,
               c=np.power(df["n"], 0.2),
               cmap="OrRd")

    ax.set_xlabel("X coordinate (m)")
    ax.set_ylabel("Y coordinate (mm)")
    ax.set_zlabel("Z coordinate (mm)")

    # plt.show()
    fig.savefig(filename)
