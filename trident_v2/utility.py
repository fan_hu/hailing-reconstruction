import numpy as np

class VecOperation:
    """
    Collection of vector operations
    """
    @staticmethod
    def pol2rect(theta: float, phi: float) -> np.ndarray:
        """
        Change from polar coordinates to rectangular coordinates
        """
        return np.array([np.cos(phi)*np.sin(theta),
                         np.sin(phi)*np.sin(theta),
                         np.cos(theta)])

    @staticmethod
    def rect2pol(nx: float, ny: float, nz: float) -> np.ndarray:
        """
        Change from rectangular coordinates to polar coordinates
        """
        theta = np.arccos(nz)
        phi = np.arctan2(ny, nx)
        return np.array([theta, phi])

    @classmethod
    def get_open_angle(cls, dir1: np.ndarray, dir2: np.ndarray) -> float:
        """
        Calculate the intersection angle of the two directions.

        Parameter
        ---------
        `dir1: np.ndarray`
            The one direction, it can be in form of [theta, phi] or [nx, ny, nz].

        `dir2: np.ndarray`
            The other direction.

        Return
        ------
        `open_angle: float`
            The intersection angle in units of **minute**
        """
        if len(dir1) == 2:
            dir1 = cls.pol2rect(*dir1)
            dir2 = cls.pol2rect(*dir2)
        dir1 = dir1 / np.linalg.norm(dir1)
        dir2 = dir2 / np.linalg.norm(dir2)
        costh = dir1.dot(dir2)
        if costh <= 1.0:
            delta = np.arccos(costh)
        elif costh > 1.0 and np.abs(costh - 1) < 1e-6:
            dir_d = dir1 - dir2
            delta = np.linalg.norm(dir_d)
        return delta * 180 / np.pi * 60

    @staticmethod
    def rot_uz(px0: np.ndarray, py0: np.ndarray, pz0: np.ndarray, vec_z: list) -> list:
        """
        Rotate coordinate so that its z-axis is now pointing to `vec_z`.
        """
        [ux, uy, uz] = vec_z
        up = ux * ux + uy * uy
        if up > 0:
            up = np.sqrt(up)
            px = (ux*uz*px0 - uy*py0)/up + ux*pz0
            py = (uy*uz*px0 + ux*py0)/up + uy*pz0
            pz = -up*px0 + uz*pz0
        elif uz < 0:
            [px, py, pz] = [-px0, py0, -pz0]
        else:
            [px, py, pz] = [px0, py0, pz0]
        return [px, py, pz]

    @staticmethod
    def transform(px0: np.ndarray, py0: np.ndarray, pz0: np.ndarray, paras: np.ndarray) -> list:
        """
        Transform coordinate so that its origin point is now at `paras[0:3]`, 
        its z-axis is now pointing to `paras[3:6]`.
        """
        [x0, y0, z0] = paras[0:3]
        px = px0 - x0
        py = py0 - y0
        pz = pz0 - z0
        [px, py, pz] = VecOperation.rot_uz(px, py, pz, paras[3:6])
        return [px, py, pz]
