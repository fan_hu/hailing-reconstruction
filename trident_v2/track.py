from __future__ import annotations
from trident_v2.utility import VecOperation
from trident_v2.root2pd import primary_reader
import trident_v2.reconstruct as rec
import numpy as np
import yaml


class Track:
    def __init__(self, paras: np.ndarray) -> None:
        self.paras = paras
        self.energy = None

    def __repr__(self) -> str:
        return self.paras.__repr__()

    def __str__(self) -> str:
        return self.paras.__str__()

    @classmethod
    def read_truth(cls, mode: str, path_data_file: str, entry: int = 0) -> Track:
        """
        Read the true parameters from file.

        Parameters
        ----------
        `mode: str`
            The type of file to read, can be YAML or ROOT

        `path_data_file: str`
            The path to the ROOT file.

        `entry: int`
            The entry (G4Event) to reader.

        Return
        ------
        `track: Track`
        """
        if mode == "YAML":
            return cls.__read_truth_yaml(path_data_file)
        elif mode == "ROOT":
            return cls.__read_truth_root(path_data_file, entry)
        else:
            print("Wrong parameters!")
            raise KeyError

    @classmethod
    def __read_truth_yaml(cls, path_data_file: str) -> Track:
        """
        Read the true parameters from a yaml file.

        Parameters
        ----------
        `path_data_file: str`
            The path to the YAML file.

        Return
        ------
        `track: Track`
        """
        with open(str(path_data_file), 'r') as f:
            input = yaml.full_load(f)
        position = np.array(list(input["position"]))
        direction = np.array(list(input["direction"]))
        energy = input["energy"]
        paras = np.append(position, direction / np.linalg.norm(direction))
        track = cls(paras)
        track.energy = energy
        return track

    @classmethod
    def __read_truth_root(cls, path_data_file: str, entry: int = 0) -> Track:
        """
        Read the true parameters from ROOT file.

        Parameters
        ----------
        `path_data_file: str`
            The path to the ROOT file.

        `entry: int`
            The entry (G4Event) to reader.

        Return
        ------
        `track: Track`
        """
        df = primary_reader(path_data_file, entry)
        paras = df.to_numpy()
        paras[3:6] /= np.linalg.norm(paras[3:6])  # normalize direction
        track = cls(paras[0:6])
        track.energy = paras[6]
        return track

    @classmethod
    def from_pol(cls, paras_pol: np.ndarray) -> Track:
        paras_dir = VecOperation.pol2rect(*paras_pol[-2:])
        paras = np.append(paras_pol[0:3], paras_dir)
        return cls.__init__(paras)

    def to_pol(self) -> np.ndarray:
        paras_dir = VecOperation.rect2pol(*self.paras[-3:])
        paras = np.append(self.paras[0:3], paras_dir)
        return paras

    def angle_error(self, track: Track) -> float:
        error = VecOperation.get_open_angle(self.paras[-3:], track.paras[-3:])
        return error

    def position_error(self, track: Track) -> float:
        delta_position = self.paras[0:3] - track.paras[0:3]
        error = np.sqrt(np.dot(delta_position, delta_position))
        return error

    @classmethod
    def first_guess(cls, guess: "rec.IGuessMethod") -> Track:
        return guess.guess()

    def reconstruct(self, rec: "rec.IRecMethod") -> Track:
        return rec.reconstruct(self)
