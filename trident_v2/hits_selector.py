from __future__ import annotations
from pandas.core.frame import DataFrame
import trident_v2.track as track
import trident_v2.hits as hits

import pandas as pd
import numpy as np
from abc import abstractmethod


class IHitSelector:
    @abstractmethod
    def __init__(self) -> None:
        pass

    @abstractmethod
    def select(self, hits: hits.Hits) -> pd.DataFrame:
        pass


class SelectByResidualTime(IHitSelector):
    def __init__(self, track: track.Track, time_threshold: float) -> None:
        self.time_threshold = time_threshold
        self.track = track

    def select(self, hits: hits.Hits) -> pd.DataFrame:
        t_res = hits.get_residual(hits.get_numpy_data(), self.track.paras)[2]
        mask_select = np.power(t_res, 2) <= np.power(self.time_threshold, 2)
        data_select = hits.data[mask_select]
        return data_select

class SelectByPmtId(IHitSelector):
    def __init__(self, flag_even: bool = True) -> None:
        self.flag_even = flag_even

    def select(self, hits: hits.Hits) -> pd.DataFrame:
        if self.flag_even:
            mask_even_id = hits.data["PmtId"] % 2 == 0
        else:
            mask_even_id = hits.data["PmtId"] % 2 == 1
        data_select = hits.data[mask_even_id]
        return data_select
