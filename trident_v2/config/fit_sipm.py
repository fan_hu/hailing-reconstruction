from scipy.interpolate import CubicSpline
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
font = {'family': 'serif',
        'weight': 'normal', 'size': 12}
mpl.rc('font', **font)

path_data = "/home/lab110/HL/analysis/trident_v2/config/SiPM_PDE.csv"
data = pd.read_csv(path_data, names=["wl", "pde"])
data.sort_values("wl", inplace=True)

cs = CubicSpline(data["wl"], data["pde"])
wl_fit = np.linspace(300, 600, 300)
pde_fit = cs(wl_fit)

fig = plt.figure(figsize=(5,5), dpi=300)
ax = fig.add_subplot(111)
ax.scatter(data["wl"], data["pde"], s=3, color="#ff474c")
ax.plot(wl_fit, pde_fit, color="#0485d1", alpha=0.6)
ax.set_xlim(300, 600)
ax.set_ylim(0, 50)
ax.set_xlabel("Wave length [nm]")
ax.set_ylabel("Photon Detection Efficiency [%]")
fig.savefig("SiPM_PDE", bbox_inches='tight')
