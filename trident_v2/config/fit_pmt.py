from scipy.interpolate import CubicSpline
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
font = {'family': 'serif',
        'weight': 'normal', 'size': 12}
mpl.rc('font', **font)

path_data = "/home/lab110/HL/analysis/work_path/pmt/PMT_QE.csv"
data = pd.read_csv(path_data, names=["wl", "flux"])
data.sort_values("wl", inplace=True)

cs = CubicSpline(data["wl"], data["flux"])
wl_fit = np.linspace(300, 600, 300)
flux_fit = cs(wl_fit)

fig = plt.figure(figsize=(5,5), dpi=300)
ax = fig.add_subplot(111)
ax.scatter(data["wl"], data["flux"], s=3, color="#ff474c")
ax.plot(wl_fit, flux_fit, color="#0485d1", alpha=0.6)
ax.set_yscale("log")
ax.set_xlim(300, 600)
ax.set_ylim(5, 120)
ax.set_xlabel("Wave length [nm]")
ax.set_ylabel("Cathode Radiant Sensitivity [mA/W]")
fig.savefig("PMT_SK", bbox_inches='tight')

cs(405) * 1.24 / 405
cs(470) * 1.24 / 470

normlize1 = 28 / (cs(405) * 1.24 / 405)
normlize2 = 20 / (cs(470) * 1.24 / 470)  # Why not same as normlize1
normalize = (normlize1 + normlize2) / 2.0
qe_fit = normalize * flux_fit * 1.24 / wl_fit

fig = plt.figure(figsize=(5,5), dpi=300)
ax = fig.add_subplot(111)
ax.plot(wl_fit, qe_fit, color="#0485d1", alpha=1.0)
ax.grid(which='both')
ax.set_yscale("log")
ax.set_xlim(300, 600)
ax.set_ylim(1, 40)
ax.set_xlabel("Wave length [nm]")
ax.set_ylabel("Quantum Efficiency [%]")
fig.savefig("PMT_QE", bbox_inches='tight')
