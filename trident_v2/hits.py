from __future__ import annotations
from trident_v2 import constants
from trident_v2.root2pd import data_reader
import trident_v2.hits_selector as hs

import pandas as pd
import numpy as np


class Hits:
    def __init__(self, data: pd.DataFrame) -> None:
        self.data = data.copy()

    def __repr__(self) -> str:
        return self.data.__repr__()

    def __str__(self) -> str:
        return self.data.__str__()

    def __len__(self) -> int:
        return self.data.__len__()

    @classmethod
    def read_data(cls, path_data_file: str, tree_name: str, entry: int = 0,
                  func_qe=None) -> Hits:
        """
        Read hits ROOT tree `treename` from the ROOT file.

        Parameters
        ----------
        `path_data_file: str`
            The path to the ROOT file

        `tree_name: str`
            The tree name of the ROOT tree. 
            You can read the output to see the tree names in that ROOT file

        `entry: int`
            The entry (G4Event) to reader.

        `apply_qe: bool`
            If apply quantum efficiency selection to hits.
        Return
        ------
        `Hits`
            The hits in that tree.
        """
        data = data_reader(str(path_data_file), tree_name, entry)
        if func_qe != None:
            wl = (1240 / data["e0"]).to_numpy()
            qe = func_qe(wl)
            rnd = np.random.random(len(qe))
            mask = rnd < qe
            data = data.loc[mask]
        data.drop("e0", axis=1, inplace=True)
        return(cls(data))

    @classmethod
    def from_sampling(cls, hits: Hits, ratio: float) -> Hits:
        """
        Sampling hits from an existing hits.
        """
        if ratio >= 1.0:
            raise ValueError
        mask_sample = np.random.uniform(0., 1., len(hits.data)) < ratio
        data_sample = hits.data.loc[mask_sample]
        return Hits(data_sample)

    @classmethod
    def from_selector(cls, hits: Hits, selector: "hs.IHitSelector") -> Hits:
        data_purge = selector.select(hits)
        return Hits(data_purge)

    @classmethod
    def build_hDOM_hits(cls, data_sipm: pd.DataFrame, data_pmt: pd.DataFrame) -> Hits:
        data_tot = data_sipm.append(data_pmt)
        data_tot = data_tot[~data_tot.index.duplicated(keep='first')]
        return Hits(data_tot)

    def get_numpy_data(self) -> np.ndarray:
        """
        Get the numpy array that has [x0, y0, z0, t0]
        """
        return self.data[["x0", "y0", "z0", "t0"]].to_numpy().transpose()

    def spread(self, time_sig: float) -> None:
        """
        Add time spread to hits.
        """
        self.data["t0"] += np.random.normal(0., time_sig, len(self.data))

    def merge(self, method: str) -> Hits:
        """
        Merge hits in the same DOM according to method.
        """
        data_merge = getattr(self.data.groupby("DomId"), method)()
        data_merge["n"] = self.data.index.value_counts()
        return Hits(data_merge)

    def append(self, hits: Hits, flag_clean: bool = False) -> Hits:
        """
        Append two hits.
        """
        data_tot = self.data.append(hits.data)
        if flag_clean:
            data_tot = data_tot[~data_tot.index.duplicated(keep='first')]
        return Hits(data_tot)

    def purge(self, selector: "hs.IHitSelector") -> Hits:
        """
        Purge the hits by selector
        """
        data_purge = selector.select(self)
        return Hits(data_purge)

    @staticmethod
    def get_residual(hits_data: np.ndarray, track_paras: np.ndarray) -> np.ndarray:
        """
        This method do the following calculations:
        - the distances from the photon hits to the track, 
        - the geometric photon arrival time and the residual time.

        Parameter
        ---------
        `hits_data: np.ndarray`
            The data of hits that contains [x0, y0, z0, t0]

        `track_paras: np.ndarray`
            The simulation-true or reconstructed parameters of track.
            It contains [x0, y0, z0, nx, ny, nz]

        Return
        ------
        `residuals: np.ndarray`
            The array that contain [distance, geometry_time, residual_time].
        """
        pmt_radius = 0.200  # * This is not good writing
        [x0, y0, z0, nx, ny, nz] = track_paras
        rx = hits_data[0] - x0
        ry = hits_data[1] - y0
        rz = hits_data[2] - z0
        r_len = np.power(rx*rx + ry*ry + rz*rz, 0.5)
        l_len = rx * nx + ry * ny + rz * nz
        distance = np.power(r_len * r_len - l_len * l_len, 0.5)
        np.nan_to_num(distance, copy=False, nan=0.0)
        time_geometry = ((l_len + distance * constants.tanth) / constants.c
                         - pmt_radius / constants.c_n)
        time_residual = hits_data[3] - time_geometry
        df = np.array([distance, time_geometry, time_residual])
        return df
