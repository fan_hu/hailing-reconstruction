from __future__ import annotations
import trident_v2.hits as hits
import trident_v2.track as track
from trident_v2.utility import VecOperation
import numpy as np
from numpy import pi, power, sqrt, exp, log
from scipy.optimize import minimize
from scipy.stats import linregress
from scipy.integrate import quad
from scipy.interpolate import RectBivariateSpline
from abc import abstractmethod


class IGuessMethod:
    @abstractmethod
    def __init__(self, hits: hits.Hits) -> None:
        self.hits_data = hits.data
        pass

    @abstractmethod
    def guess(self) -> track.Track:
        pass


class LineFit(IGuessMethod):
    def __init__(self, hits: hits.Hits) -> None:
        super().__init__(hits)
        self.name = "Line Fit"
        self.opt_method = None

    def guess(self) -> track.Track:
        # select data
        df = self.hits_data[["x0", "y0", "z0", "t0"]]
        result = linregress(df["t0"], df["x0"])
        [x0, vx] = [result.intercept, result.slope]
        result = linregress(df["t0"], df["y0"])
        [y0, vy] = [result.intercept, result.slope]
        result = linregress(df["t0"], df["z0"])
        [z0, vz] = [result.intercept, result.slope]
        pos = np.array([x0, y0, z0])
        dir = np.array([vx, vy, vz])
        dir /= np.linalg.norm(dir)
        track_guess = track.Track(np.append(pos, dir))
        return track_guess


class IRecMethod:
    @abstractmethod
    def __init__(self, hits: hits.Hits) -> None:
        self.hits_data = hits.get_numpy_data()
        if "n" in hits.data.keys():
            self.hits_num = hits.data["n"].to_numpy()
        else:
            self.hits_num = None

    @abstractmethod
    def pdf(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        """
        Get the log(PDF) of the function
        """
        pass

    def likelihood(self, paras_pol: np.ndarray) -> float:
        """
        Get the minus of the sum of log(likelihood)
        """
        paras_rect = np.append(paras_pol[:3], VecOperation.pol2rect(*paras_pol[-2:]))
        [distance, _, time_res] = hits.Hits.get_residual(self.hits_data, paras_rect)
        likeli = self.pdf(time_res, distance, self.hits_num)
        return -likeli.sum()

    def reconstruct(self, track_input: track.Track) -> track.Track:
        # prepare the first guess parameters
        paras_init = track_input.to_pol()
        self.result = minimize(fun=self.likelihood, x0=paras_init, method="Powell")
        dir = VecOperation.pol2rect(*self.result["x"][3:5])
        pos = self.result["x"][0:3]
        track_rec = track.Track(np.append(pos, dir))
        return track_rec


class Estimator(IRecMethod):
    def __init__(self, hits: hits.Hits) -> None:
        super().__init__(hits)
        self.name = "M-Estimator"

    def pdf(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        return 2 - 2*np.sqrt(time*time/2 + 1)


class ResidualTimeSPE(IRecMethod):
    def __init__(self, hits: hits.Hits, medium: str) -> None:
        """
        Reconstruct the photon hits by first photon.

        Parameters:
        -----------
        medium:
            Select a specific PDF by the medium property.
        """
        super().__init__(hits)
        self.name = "Residual Time SPE"
        self.medium = medium

    def pdf(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        return getattr(self, "pdf_"+self.medium)(time, distance, num)

    def pdf_sea_water_new(self, time: np.ndarray, distance: np.ndarray,
                              num: np.ndarray) -> np.ndarray:
        def pdf(time, distance):
            mean = 0.0199 * distance - 0.114
            tau = 0.0669 * distance + 0.328
            alpha = 0.0127 * distance + 0.538
            time_ = (time - mean) / tau
            t_sk = alpha * time_
            return 2 / (exp(-t_sk) + 1) / (pi*tau*(time_*time_+1))
        log_pdf = log(pdf(time, distance))
        likeli = log_pdf
        return likeli

class ResidualTimeMPE(IRecMethod):
    def __init__(self, hits: hits.Hits, medium: str) -> None:
        """
        Reconstruct the photon hits by first photon.

        Parameters:
        -----------
        medium:
            Select a specific PDF by the medium property.
        """
        super().__init__(hits)
        self.name = "Residual Time MPE"
        self.medium = medium

    def pdf(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        return getattr(self, "pdf_"+self.medium)(time, distance, num)

    def pdf_old(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        mean = -0.021 * distance - 0.1
        tau = 0.067 * distance + 0.1
        alpha = 0.063 * distance + 0.3
        time_ = (time - mean) / tau
        t_sk = alpha * time_
        log_pdf = log((t_sk/sqrt(t_sk*t_sk+1)+1) / (pi*tau*(time_*time_+1)))
        log_cdf = np.log(1/2 + 1/pi*(1/sqrt(t_sk*t_sk+1) + np.arctan(t_sk)))
        likeli = log(num) + log_pdf + (num-1)*log_cdf
        return likeli

    def pdf_sea_water(self, time: np.ndarray, distance: np.ndarray,
                      num: np.ndarray) -> np.ndarray:
        mean = 0.0175 * distance - 0.144
        tau = 0.0706 * distance + 0.307
        alpha = 0.0288 * distance + 1.166
        time_ = (time - mean) / tau
        t_sk = alpha * time_
        log_pdf = log(2 / (exp(-t_sk) + 1) / (pi*tau*(time_*time_+1)))
        log_cdf = np.log(1/2 + 1/pi*(1/sqrt(t_sk*t_sk+1) + np.arctan(t_sk)))
        likeli = log(num) + log_pdf + (num-1)*log_cdf
        return likeli

    def pdf_sea_water_new_cdf(self, time: np.ndarray, distance: np.ndarray,
                              num: np.ndarray) -> np.ndarray:
        def pdf(time, distance):
            mean = 0.0199 * distance - 0.114
            tau = 0.0669 * distance + 0.328
            alpha = 0.0127 * distance + 0.538
            time_ = (time - mean) / tau
            t_sk = alpha * time_
            return 2 / (exp(-t_sk) + 1) / (pi*tau*(time_*time_+1))
        log_pdf = log(pdf(time, distance))
        cdf = np.array([quad(pdf, -5, time, args=(distance))[0]
                        for [time, distance] in zip(time, distance)])
        likeli = log(num) + log_pdf + (num-1)*np.log(1-cdf)
        return likeli


class ResidualTimeSpline(IRecMethod):
    def __init__(self, hits: hits.Hits, interplate: RectBivariateSpline) -> None:
        """
        Reconstruct the photon hits by first photon.

        Parameters:
        -----------
        interplate:
            Select a interplate object.
        """
        super().__init__(hits)
        self.name = "Residual Time Spline"
        self.itp = interplate

    def pdf(self, time: np.ndarray, distance: np.ndarray, num: np.ndarray) -> np.ndarray:
        log_pdf = self.itp(distance, time, grid=False)
        return log_pdf
