## 目的

处理经过trigger筛选后得到的，与Muon相关联的hit，最终重建出muon的信息。

## 流程

0. 读取数据
1. 读取true muon信息
2. Line Fit
3. M-estimator
4. hit clean
5. likelihood

## Hits

hits首先会被读取进来，然后可能会加上TTS扰动，接下来可能要做groupby操作，操作完还可能要经过几轮clean，中间还可能会append上其他的hits来做merge。因此hit是经常变化的一个量，可以把hits包装成一个类。

Hits下面应该要有：

私有变量：
- data: 一个pd.DataFrame
- key: PMT，SiPM或者hDOM

方法：
- init(data, key)
- static read(path_data: str, key: str): 读取hits的信息
- spread(sig_time: float): 加上时间扰动
- append(Hits): 与另外一个Hits合并
- merge(method: str): 筛选出first hit的时间，或者average hit的实践
- purge(Selector): 筛选出一些hit

### IHitSelector

私有变量：
- name

方法：
- select: 主要API

#### SelectByResidualTime

- time_threshold

## Track

私有变量：
- parameters: 一个np.ndarray，包含(x0, y0, z0, nx, ny, nz)，要保证归一化
- parameters_pol: 包含(x0, y0, z0, theta, phi)

方法：
- init(parameters)
- static first_guess(GuessMethod)
- static read(path_paras)
- static from_pol(parameters_pol)
- to_pol()
- angle_error(track): 计算和另外一个track的角度差
- reconstruct(Hits, RecMethod) -> Track: 根据Hits重建出一个新的Track。

### IGuessMethod

私有变量：
- name
- result

方法：
- init(): 初始化重建过程中所需的参数
- guess(): 主要API

#### LineFit

### IRecMethod

私有变量：
- name
- result
- opt_method: str 对应scipy.optimize里头的方法

方法：
- init(): 初始化重建过程中所需的参数
- reconstruct(): 主要API

#### LineFit(RecMethod)

#### M-estimator(RecMethod)

#### ResidualTimeAvg(RecMethod)

#### ResidualTimeMPE(RecMethod)

