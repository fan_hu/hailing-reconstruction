import RecClass as rec
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
likeliModel = "avgNew"
angleErrList = []

for id in range(100):
    print("Doing Particle {:d}".format(id))
    doms = rec.read_dom(directory, id, trigger=True)
    paraIni = rec.read_para(directory, id, "TF")
    paraSim = rec.read_para(directory, id, "sim")
    trackIni = rec.Track(*paraIni)
    timeShift = trackIni.time_shift(doms)
    optimize = rec.minimize(likeliModel, doms, paraIni, timeShift, "Nelder-Mead", {'maxiter': 3000, 'disp': False})
    paraOpt = optimize.x
    rec.write_opt_rst(paraOpt, directory, id, likeliModel)
    angleErr = rec.angle_err(paraOpt[-2:], paraSim[-2:])
    angleErrList.append(angleErr)

angleErrList = np.sort(np.array(angleErrList))
angleErrListSel = angleErrList[: -int(0.05*len(angleErrList))]

plt.hist(angleErrListSel, bins=20, density=True)
plt.xlabel('Angle Error [minits]')
plt.ylabel('Probability')
plt.savefig('StaLikeliPmts.svg')



