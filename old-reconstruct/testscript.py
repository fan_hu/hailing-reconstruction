import RecClass as rec
from pathlib import Path
import numpy as np
import pandas as pd
directory = rec.directory
idx = 1
doms = rec.read_dom(directory, idx)
print(len(doms))
domsCas = rec.hit_clean_causality(doms)
print(len(domsCas))
# domsLv = rec.hit_clean_level(doms, 1)
# print(len(domsLv))

rec.plot_dom(doms)
rec.plot_dom(domsCas)
# rec.plot_dom(domsLv)

paraIni = rec.read_para(directory, idx, "TF")
trackIni = rec.Track(*paraIni)
print(trackIni.length())
print(rec.hit_received(doms))

likeliModel = 'M_estimator'
paraIni = rec.read_para(directory, idx, "TF")
dirIni = rec.pol2rect(*paraIni[-2:])
print(dirIni)
paraSim = rec.read_para(directory, idx, "sim")
trackIni = rec.Track(*paraIni)
timeShift = trackIni.time_shift(domsCas)
optimize = rec.minimize(likeliModel, domsCas, paraIni, timeShift, "Powell", {'maxiter': 3000, 'disp': True})
paraOpt = optimize.x
rec.angle_err(paraOpt[-2:], paraSim[-2:])