import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import splev, splrep

dirSel = "/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeSelect/"
dirPlt = '/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timePlot/'
dirRst = '/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeRst/'


d_min = [0.0, ]
for i in range(40):
    d_add = max(1.0, 0.2*np.exp(i/10))
    d_min.append(d_min[-1] + d_add)

quantiles = np.linspace(0, 1, 51)

for i in range(len(d_min)):
    # read data
    time_array = pd.read_csv(dirSel+'I'+str(i)+'.csv', sep=",", skiprows=0, names=['time'])['time']
    time_array = time_array.to_numpy()

    # data purification
    time_array.sort()
    raw_length = len(time_array)
    idx_trunc = int(raw_length*0.05)
    time_arr_select = time_array[idx_trunc : -idx_trunc]

    # jetter smoothing
    data_length = len(time_arr_select)
    time_scale = time_arr_select[-1]-time_arr_select[0]
    jet_sig = np.sqrt(0.05**2 + (time_scale/80)**2)
    if (data_length > 1000):
        jetter = np.random.normal(0, jet_sig, 10*data_length)
        time_arr_sel_jet = np.tile(time_arr_select, 10) + jetter
    else:
        jetter = np.random.normal(0, jet_sig, 50*data_length)
        time_arr_sel_jet = np.tile(time_arr_select, 50) + jetter

    # quantile ploting
    time_quantile = np.quantile(time_arr_sel_jet, quantiles)
    time_center = (time_quantile[:-1] + time_quantile[1:])/2
    time_duration = time_quantile[-1] - time_quantile[1]
    prob_center = 0.02 / (time_quantile[1:] - time_quantile[:-1])
    plt.plot(time_center, prob_center)
    plt.xlabel('time (ns)')
    plt.ylabel('distribution')
    if (i != len(d_min)-1):
        plt.title(r'd$\in$[{:.2f}, {:.2f}]'.format(d_min[i], d_min[i+1]))
    else:
        plt.title(r'd$\in$[{:.2f}, $\inf$]'.format(d_min[i]))
    plt.xlim(time_quantile[0], time_quantile[-1])
    plt.ylim(0, max(prob_center)*1.2)
    plt.savefig(dirPlt+'I'+str(i)+'.svg'.format(d_min[i]))
    plt.close()
    np.savetxt(dirRst+'I'+str(i)+'quantile.csv', time_quantile, fmt='%.4f', delimiter=', ')

    # interplot
    time_tot = np.arange(time_center[0], time_center[-1]+time_duration/2, 0.1)
    time_spl = time_tot[(time_tot >= time_center[0]) & (time_tot < time_center[-1])]
    time_later = time_tot[time_tot >= time_center[-1]]
    spl = splrep(time_center, prob_center)
    prob_spl = splev(time_spl, spl)
    prob_later = np.ones(len(time_later)) * prob_spl[-1]
    prob_tot = np.append(prob_spl, prob_later)
    np.savetxt(dirRst+'I'+str(i)+'time.csv', time_tot, fmt='%.1f', delimiter=', ')
    np.savetxt(dirRst+'I'+str(i)+'prob.csv', prob_tot, fmt='%.4f', delimiter=', ')
