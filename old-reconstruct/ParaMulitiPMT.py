import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

def get_res_avg(track, doms, nlim):
    t = []
    d = []
    n = []
    for dom in doms:
        if dom.ni < nlim:
            continue
        [d_dom, t_geo_dom] = track.geometry(dom)
        vec_si = track.direction_vec(dom)
        t_temp = 0
        n_temp = 0
        for i in range(len(rec.DIRECTION_LIST)):
            if (dom.n_pmts[i] == 0):
                continue
            costh_temp = -vec_si.dot(rec.DIRECTION_LIST[i])
            if (costh_temp > 0):
                t_temp = sum(30*np.log(np.array(dom.t_pmts[i]-t_geo_dom)/30 + 1))
                n_temp += dom.n_pmts[i]
        if (n_temp == 0):
            continue
        d.append(d_dom)
        t.append(t_temp / n_temp)
        n.append(n_temp)
    rst = np.array([t, d, n])
    return rst

def get_res(track, doms):
    t = []
    d = []
    for dom in doms:
        [d_dom, t_geo_dom] = track.geometry(dom)
        vec_si = track.direction_vec(dom)
        for i in range(len(rec.DIRECTION_LIST)):
            if (dom.n_pmts[i] == 0):
                continue
            costh_temp = -vec_si.dot(rec.DIRECTION_LIST[i])
            if (costh_temp < 0):
                for time in dom.t_pmts[i]:
                    time_norm = 30*np.log(np.array(time-t_geo_dom)/30 + 1)
                    t.append(time_norm)
                    d.append(d_dom)
    rst = np.array([t, d])
    return rst

def Pandel_likeli(para):
    rst = 0
    [beta, lam] = para
    for i in range(len(t)):
        t_exp = beta * d[i] / lam
        t_var = beta**2 * d[i] / lam / n[i]
        rst += np.log(t_var)/2 + (t[i] - t_exp)**2 / (2* t_var)
    return rst


def Ops_likeli(para):
    rst = 0
    [beta, lam] = para
    for i in range(len(t_ops)):
        t_norm = t_ops[i] / beta
        alpha = d_ops[i] / lam
        rst -= np.log(rec.spl_gamma(t_norm, alpha, alpha) / beta)
    return rst




directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
[t, d, n] = [np.array([])] * 3
[t_ops, d_ops] = [np.array([])] * 2
for id in range(30):
    doms = rec.read_dom(directory, id, True)
    paraSim = rec.read_para(directory, id, 'sim')
    trackSim = rec.Track(*paraSim)
    [t_temp, d_temp, n_temp] = get_res_avg(trackSim, doms, 1)
    [t_ops_temp, d_ops_temp] = get_res(trackSim, doms)
    t = np.append(t, t_temp)
    d = np.append(d, d_temp)
    n = np.append(n, n_temp)
    t_ops = np.append(t_ops, t_ops_temp)
    d_ops = np.append(d_ops, d_ops_temp)
dmax_pos = d_ops.argmax()
t_ops = np.delete(t_ops, dmax_pos)
d_ops = np.delete(d_ops, dmax_pos)

# show scattering of manipulated average residual time distribution
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d_ops, t_ops, s=3, alpha=.5)
plt.xlim(0, 200)
plt.ylim(-5, 200)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")

#show statistic property of t_res as a function of distance
data_ops = np.array([t_ops, d_ops]).T
binSize = 7; disPerBin = 10
distance = np.array(range(binSize)) * disPerBin + disPerBin/2
for i in range(binSize):
    for j in range(len(data_ops)):
        if (data_ops[j][1] >= disPerBin*i and data_ops[j][1] < disPerBin*(i+1)):
            data_ops[j][1] = i
[timeAvg, timeSig, num] = np.zeros((3,binSize))
for j in range(binSize):
    time_costhBin_select = data_ops[data_ops[:,1] == j][:,0]
    timeAvg[j] = time_costhBin_select.mean()
    timeSig[j] = time_costhBin_select.std()
    num[j] = len(time_costhBin_select)
# plt.errorbar(distance, timeAvg, timeSig, c='royalblue')
# plt.plot(distance, 4*np.sqrt(distance))
plt.plot(distance, timeSig)
plt.plot(distance, 10+3*np.sqrt(distance))

# do fitting
para_ini = [30, 180]
fit_rst = so.minimize(Pandel_likeli, para_ini, method='Nelder-Mead')
[beta, lam] = fit_rst["x"]
fit_ops_rst = so.minimize(Ops_likeli, para_ini, method='Nelder-Mead')
[beta_ops, lam_ops] = fit_ops_rst["x"]
rec.write_pmts_para(beta, lam, beta_ops, lam_ops)