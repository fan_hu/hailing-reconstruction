import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec


directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
for muonID in range(1000):
    if (muonID % 10 == 0):
        print(muonID)
    data_hit = pd.read_csv(directory+"hits_"+str(muonID), sep=",", skiprows=1, names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "t_res", "wl"])
    data_hit = data_hit.sort_values(by="t")
    timebins = 30
    weigh_pos = np.zeros((timebins,5))  # (t, x, y, z, n)
    deleterows = []
    for timebin in range(timebins):
        mask_t = data_hit["t"].between(200.0 * timebin, 200.0 * (timebin+1))
        data_t = data_hit[mask_t]
        datalen = data_t.shape[0]
        if (datalen < 3):  # ignore the time bins that has little hit
            deleterows.append(timebin)
            continue
        for i in range(datalen):
            hit = np.array([data_t["t"].iloc[i], data_t["x"].iloc[i], data_t["y"].iloc[i], data_t["z"].iloc[i]])
            weigh_pos[timebin][:4] += hit
        weigh_pos[timebin] /= datalen
        weigh_pos[timebin][4] = datalen
    weigh_pos = np.delete(weigh_pos, obj=deleterows, axis=0)
    # gravity center
    grav_center = np.zeros(3)
    for i in range(len(weigh_pos)):
        grav_center += weigh_pos[i][1:4] * weigh_pos[i][4]
    grav_center /= np.sum(weigh_pos[:,4])
    # scipy minimize
    def func_min(paras):
        [x0, y0, z0, vx, vy, vz] = paras
        divation = []
        for i in range(len(weigh_pos)):
            ti = weigh_pos[i][0]
            xi = weigh_pos[i][1]
            yi = weigh_pos[i][2]
            zi = weigh_pos[i][3]
            ni = weigh_pos[i][4]
            divation.append((x0 + vx * ti - xi)**2 * ni)
            divation.append((y0 + vy * ti - yi)**2 * ni)
            divation.append((z0 + vz * ti - zi)**2 * ni)
        return np.sqrt(sum(divation))
    def func_fit(ti, x0, y0, z0, vx, vy, vz):
        xi = x0 + ti * vx
        yi = y0 + ti * vy
        zi = z0 + ti * vz
        return np.array([xi, yi, zi])
    x0 = weigh_pos[0][1:4]
    v0 = (weigh_pos[-1][1:4] - weigh_pos[0][1:4]) / (weigh_pos[-1][0] - weigh_pos[0][0])
    para_ini = np.append(x0, v0)
    min_rst = so.minimize(fun = func_min, x0=para_ini)
    para_opt = min_rst["x"]
    time_fit = np.linspace(0, 200*timebins, timebins+1)
    pos_fit = func_fit(time_fit, *para_opt).transpose()
    # show result
    angle_fit = rec.rect2pol(para_opt[-3:])
    rst_fit = np.append(grav_center, angle_fit)
    # print("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(*rst_fit))
    with open(directory+'rec/'+'particle_'+str(muonID)+"_TF", 'w') as fl:
        fl.write("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(*rst_fit))
    plt.figure(figsize=(6,5), dpi=300)
    dotSize = 20*np.sqrt(weigh_pos[:,4]/max(weigh_pos[:,4]))
    plt.scatter(weigh_pos[:,1], weigh_pos[:,2], s=dotSize)
    plt.plot(pos_fit[:,0], pos_fit[:,1], c='darkred', alpha=0.5)
    plt.xlim(-500, 500)
    plt.ylim(-500, 500)
    plt.xlabel('X coordinates (m)')
    plt.ylabel('Y coordinates (m)')
    plt.savefig(directory+'rec/'+'particle_'+str(muonID)+"_TF.png")
    plt.close()