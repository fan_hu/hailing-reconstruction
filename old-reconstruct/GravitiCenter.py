import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
from pathlib import Path

# read data
dirctory = "../data/03161827/"
Path(dirctory+"rec").mkdir(parents=True, exist_ok=True)

for muon_number in range(10):
    # read data
    data_hit = pd.read_csv(dirctory+"Muon_"+str(muon_number), sep=",", skiprows=5, names=["id", "x", "y", "z", "t", "wl"])
    data_hit = data_hit.sort_values(by="t")

    # get center of gravity
    length = data_hit.shape[0]
    r0 = np.zeros(3)
    for i in range(length):
        r0[0] += data_hit["x"].iloc[i]
        r0[1] += data_hit["y"].iloc[i]
        r0[2] += data_hit["z"].iloc[i]
    r0 /= length

    print("x, y, z \n {:.1f}, {:.1f}, {:.1f}" .format(r0[0], r0[1], r0[2]))
