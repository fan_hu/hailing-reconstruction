import numpy as np
from numpy import pi, power, log, log10, sqrt, exp, sin, cos
from random import random
from copy import copy
C_LT = 0.2998
REF_IDX = 1.3425
LAM_S = 50
LAM_A = 35
DOM_R = 0.5
sca_g = 0.9
sca_b = 0.0526316


class Source:
    def __init__(self, x0, y0, z0, t0):
        self.r0 = np.array([x0,y0,z0])
        self.t0 = t0

    def gen_photons(self):
        theta = random() * pi
        phi = random() * 2 * pi - pi
        return Photon(*self.r0, self.t0, theta, phi)


class Photon:
    def __init__(self, xi, yi, zi, ti, theta, phi):
        self.ri = np.array([xi,yi,zi])
        self.ti = ti
        self.pi = np.array([cos(phi)*sin(theta), sin(phi)*sin(theta), cos(theta)])
        self.ni = 0
        self.len = 0
        self.lenTot = - LAM_A * log(1-random())
        self.flagHit = False

    def para(self):
        return(np.append(self.ri, self.ti))

    def scatter(self):
        rand1 = random()
        rand2 = random()
        if (rand1 < 0.55):
            xi = 2 * rand2 - 1
            cos_theta = (1 + sca_g*sca_g - pow((1 - sca_g*sca_g) / (1 + sca_g*xi), 2)) / (2 * sca_g)
        else:
            cos_theta = 2 * pow(rand2, sca_b) - 1
        sin_theta = sqrt(1 - cos_theta**2)
        phi = random() * 2 * pi - pi
        p0_i = np.array([cos(phi)*sin_theta, sin(phi)*sin_theta, cos_theta])
        cos_phi = self.pi[0] / sqrt(1 - self.pi[2]**2)
        sin_phi = self.pi[1] / sqrt(1 - self.pi[2]**2)
        cos_the = self.pi[2]
        sin_the = sqrt(1 - self.pi[2]**2)
        p0_f = np.zeros(3)
        p0_f[0] = p0_i[0] * cos_the * cos_phi - p0_i[1] * sin_phi + p0_i[2] * sin_the * cos_phi
        p0_f[1] = p0_i[0] * cos_the * sin_phi - p0_i[1] * cos_phi + p0_i[2] * sin_the * sin_phi
        p0_f[2] = -p0_i[0] * sin_the + p0_i[2] * cos_the
        self.pi = p0_f

def prop(photon):
    photon.ni += 1
    length = - LAM_S * log(1-random())
    if (length > (photon.lenTot - photon.len)):
        length = photon.lenTot - photon.len
    photon.len += length
    photon.ri += photon.pi * length
    photon.ti += length / C_LT * REF_IDX
    return photon

class Dom:
    def __init__(self, xi, yi, zi):
        self.ri = np.array([xi,yi,zi])
        self.ni = 0
        self.ti = np.array([])

    def recieve(self, photoni, photonf):
        flag_hit = False
        vec_OA = photoni.ri - self.ri
        len_AC = -(vec_OA.dot(photoni.pi))
        len_OA = sqrt(vec_OA.dot(vec_OA))
        len_OC = sqrt(len_OA * len_OA - len_AC * len_AC)
        if (len_OC < DOM_R and len_AC > 0):
            vec_OB = photonf.ri - self.ri
            len_OB = sqrt(vec_OB.dot(vec_OB))
            vec_AB = photonf.ri - photoni.ri
            len_AB = sqrt(vec_AB.dot(vec_AB))
            if (len_AC < len_AB or len_OB < DOM_R):
                len_CR = sqrt(DOM_R * DOM_R - len_OC * len_OC)
                len_AR = len_AC - len_CR
                flag_hit = True
                self.ni += 1
                self.ti = np.append(self.ti, photoni.ti + len_AR / C_LT * REF_IDX)
        return flag_hit

source = Source(0.0, 0.0, 0.0, 0.0)
dom = Dom(0.0, 0.0, 20.0)
for i in range(50000000):
    photon_i = source.gen_photons()
    while (photon_i.len < photon_i.lenTot):
        photon_f = prop(photon_i)
        if (dom.recieve(photon_i, photon_f)):
            break
        photon_i = photon_f
        if (photon_i.len < photon_i.lenTot):
            photon_i.scatter()
with open('PointSource', 'w') as fl:
    for i in range(dom.ni):
        fl.write('{:.3f}\n'.format(dom.ti[i]))