import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

def get_residual(track, doms):
    t = []
    d = []
    for dom in doms:
        [d_dom, t_geo_dom] = track.geometry(dom)
        for ti in dom.ti:
            t.append(ti - t_geo_dom)
            d.append(d_dom)
    rst = np.array([t, d])
    return rst

def time_func(t, tau0=30):
    if t > 0:
        return tau0 * np.log(t / tau0 + 1)
    else:
        return t

def get_res_avg(track, doms, nlim, tau0=10000):
    t = []
    d = []
    n = []
    for dom in doms:
        if dom.ni < nlim:
            continue
        t_temp = np.array([])
        [d_dom, t_geo_dom] = track.geometry(dom)
        for ti in dom.ti:
            t_temp = np.append(t_temp, time_func(ti-t_geo_dom, tau0))
        t_avg = np.average(t_temp)
        d.append(d_dom)
        t.append(t_avg)
        n.append(dom.ni)
    rst = np.array([t, d, n])
    return rst

# fitting function
def alpha_func(d, lam):
    return d/lam

def Pandel_likeli(para):
    # para = [beta, lam]
    rst = 0
    for i in range(len(t_avg)):
        alpha = alpha_func(d_avg[i], para[1])
        rst += np.log(para[0]) + np.log(alpha) / 2
        rst += (t_avg[i] - para[0] * alpha)**2 / (2 * para[0]**2 * alpha / n_avg[i])
    return rst


directory = "../data/03260856/"
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
doms = rec.read_dom(directory, 0, trigger=False)
for muonID in range(0, 1):
    doms_temp = rec.read_dom(directory, muonID, trigger=False)
    for i in range(len(doms)):
        doms[i].ni += doms_temp[i].ni
        doms[i].ti = np.append(doms[i].ti, doms_temp[i].ti)
doms_trg = []
for dom in doms:
    if (dom.ni != 0):
        doms_trg.append(dom)
paraSim = rec.read_para(directory, 0, "sim")
paraSim = paraSim[:3]
pointSim = rec.Point(*paraSim)
domSel = doms[4210]
t = domSel.ti
d = np.array([25*np.sqrt(3)] * len(t))
t = t - d / rec.C_MED
[t_avg_raw, d_avg, n_avg] = get_res_avg(pointSim, doms_trg, 3)
[t_avg, d_avg, n_avg] = get_res_avg(pointSim, doms_trg, 3, 30)

# show distribution of residual time at distance 34.64
plt.figure(figsize=(6,5), dpi=300)
plt.hist(t, bins=40, range=(-10,150))
plt.xlim(-10, 150)
plt.yscale('log')
plt.xlabel("residual time (ns)")
plt.ylabel("distribution")
plt.savefig(directory + 'rec/' + 'DisResTimeRaw.png')







# show scattering of raw average residual time distribution
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d_avg, t_avg_raw, s=3, alpha=.5)
plt.xlim(0, 300)
plt.ylim(0, 200)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'AvgResTimeRaw.png')
# show scattering of manipulated average residual time distribution
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d_avg, t_avg, s=3, alpha=.5)
plt.xlim(0, 300)
plt.ylim(0, 100)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'AvgResTime.png')
# show scattering of hit number
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d_avg, n_avg, s=3, alpha=.5)
d_fit = np.arange(1, 200, 0.1)
n_fit = 3000000 * rec.DOM_R**2 / 2 / d_fit * np.exp(-d_fit/rec.SIN_CK/40)
plt.plot(d_fit, n_fit, c='darkred')
plt.yscale('log')
plt.xlim(0, 200)
plt.ylim(10, 10000)
plt.xlabel("distance (m)")
plt.ylabel("number of hits")
plt.savefig(directory + 'rec/' + 'NumHitFit.png')




# do fitting
para_ini = [200, 500]
fit_rst = so.minimize(Pandel_likeli, para_ini, method='Nelder-Mead')
[beta, lam] = fit_rst["x"]
# calculate fiting result
d_fit = np.arange(1, 200, 0.1)
t_avg_fit = beta * alpha_func(d_fit, lam)
n_fit = 3000000 * rec.DOM_R**2 / 2 / d_fit * np.exp(-d_fit/rec.SIN_CK/40)
t_avg_err_fit = beta * np.sqrt(alpha_func(d_fit, lam) / n_fit)

# show fit sigma
plt.figure(figsize=(6,5), dpi=300)
plt.plot(d_fit, t_avg_fit, color="darkred")
plt.fill_between(d_fit, t_avg_fit-t_avg_err_fit, t_avg_fit+t_avg_err_fit, color="darkred", alpha=0.3)
plt.scatter(d_avg, t_avg, s=3, alpha=.5)
plt.xlim(0, 200)
plt.ylim(0, 150)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'AvgResTimeFit.png')
plt.close()
