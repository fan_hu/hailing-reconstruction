import RecClass as rec
from pathlib import Path
import numpy as np

directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
model1 = 'M_estimator'
model2 = 'avg'
for idx in range(100):
    print("Doing Particle {:d}".format(idx))
    doms = rec.read_dom(directory, idx)
    doms = rec.hit_clean_causality(doms)
    paraIni = rec.read_para(directory, idx, "TF")
    paraSim = rec.read_para(directory, idx, "sim")
    trackIni = rec.Track(*paraIni)
    timeShift = trackIni.time_shift(doms)

    # optimize using M_estimator
    optimize = rec.minimize(model1, doms, paraIni, timeShift, "Powell", {'maxiter': 3000, 'disp': True})
    paraOpt1 = optimize.x
    rec.write_opt_rst(paraOpt1, directory, idx, model1)
    gridLikeli = rec.grid(paraOpt1[-2:], 0.01, 30)
    likeli = rec.angle_likeli(gridLikeli, model1, doms, paraOpt1, timeShift)
    rec.figure_likeli(gridLikeli, likeli, paraOpt1, paraSim, directory, idx, model1)

    # optimize using avg
    rec.read_pandel_para()
    optimize = rec.minimize(model2, doms, paraOpt1, timeShift, "Nelder-Mead", {'maxiter': 3000, 'disp': True})
    paraOpt2 = optimize.x
    rec.write_opt_rst(paraOpt2, directory, idx, model2)
    gridLikeli = rec.grid(paraOpt2[-2:], 0.01, 30)
    likeli = rec.angle_likeli(gridLikeli, model2, doms, paraOpt2, timeShift)
    rec.figure_likeli(gridLikeli, likeli, paraOpt2, paraSim, directory, idx, model2)