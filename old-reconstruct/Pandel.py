import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

def time_func(t, tau0):
    if t > 0:
        return tau0 * np.log(t / tau0 + 1)
    else:
        return t

def get_res_avg(track, doms, nlim, tau0=10000):
    t = []
    d = []
    n = []
    for dom in doms:
        if dom.ni < nlim:
            continue
        t_temp = np.array([])
        [d_dom, t_geo_dom] = track.geometry(dom)
        for ti in dom.ti:
            t_temp = np.append(t_temp, time_func(ti-t_geo_dom, tau0))
        t_avg = np.average(t_temp)
        d.append(d_dom)
        t.append(t_avg)
        n.append(dom.ni)
    rst = np.array([t, d, n])
    return rst

# fitting function
def alpha_func(d, lam):
    return d/lam

def Pandel_likeli(para):
    # para = [beta, lam]
    rst = 0
    for i in range(len(t)):
        alpha = alpha_func(d[i], para[1])
        rst += np.log(para[0]) + np.log(alpha) / 2
        rst += (t[i] - para[0] * alpha)**2 / (2 * para[0]**2 * alpha / n[i])
    return rst


directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
[t, d, n] = [np.array([])] * 3
for id in range(10):
    doms = rec.read_dom(directory, id, True)
    paraSim = rec.read_para(directory, id, 'sim')
    trackSim = rec.Track(*paraSim)
    [t_temp, d_temp, n_temp] = get_res_avg(trackSim, doms, 3, 30)
    t = np.append(t, t_temp)
    d = np.append(d, d_temp)
    n = np.append(n, n_temp)

# show scattering of manipulated average residual time distribution
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d, t, s=3, alpha=.5)
plt.xlim(0, 200)
plt.ylim(0, 80)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'AvgResTime.png')

# do fitting
para_ini = [200, 500]
fit_rst = so.minimize(Pandel_likeli, para_ini, method='Nelder-Mead')
[beta, lam] = fit_rst["x"]
# write fitting result
rec.write_pandel_para(beta, lam)

# calculate fiting result
d_fit = np.arange(1, 200, 0.1)
t_fit = beta * alpha_func(d_fit, lam)

# show fit
plt.figure(figsize=(6,5), dpi=300)
plt.plot(d_fit, t_fit, color="darkred")
plt.scatter(d, t, s=3, alpha=.5)
plt.xlim(0, 200)
plt.ylim(0, 150)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'AvgResTimeFit.png')
plt.close()
