import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import least_squares

angleErr = np.genfromtxt('angleErrListNp.csv')

# smoothing data
data_len = len(angleErr)
jetter = np.random.normal(0, 0.2, 50*data_len)
angleErr_sm = np.tile(angleErr, 50) + jetter

# generate histogram data
n_hist, bins_hist = np.histogram(angleErr_sm, range = (0, 15), bins=20, density=True)
sig_hist = (bins_hist[1:] + bins_hist[:-1]) / 2

# find the best fit value of disp
def func_fit(disp):
    n_hist_fit = sig_hist/disp**2 * np.exp(-np.power(sig_hist,2)/2/disp**2)
    squre_sum = np.sum(np.power(n_hist_fit-n_hist,2))
    return squre_sum
disp_list = np.linspace(3.5, 6, 100)
for disp in disp_list:
    print("disp: {:.2f}, squre_sum: {:.4f}".format(disp, func_fit(disp)))


# calculate the fit data
disp = 4.1
sig_fit = np.linspace(0, 15, 100)
n_fit = sig_fit/disp**2 * np.exp(-np.power(sig_fit,2)/2/disp**2)

plt.scatter(sig_hist, n_hist, s=10.0)
plt.plot(sig_fit, n_fit, color='tomato')
font2 = {'family': 'serif', 'weight': 'normal', 'size': 12}

plt.xlim(0, 15)
plt.ylim(0,)
plt.xlabel('Angle Error [minits]', fontdict=font2)
plt.ylabel('Probability', fontdict=font2)


