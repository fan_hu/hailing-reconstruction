import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

# read data
dirctory = "../data/03131407/"
Path(dirctory+"rec").mkdir(parents=True, exist_ok=True)
data_hit = pd.read_csv(dirctory+"Muon_0", sep=",", skiprows=5, names=["id", "x", "y", "z", "t", "wl"])
data_hit = data_hit.sort_values(by="t")

# initialize dom data
doms = []
for i in range(20):
    for j in range(20):
        for k in range(20):
            doms.append(rec.Dom(-475+50*i, -475+50*j, -475+50*k))

# read hit into dom data
num_hit = data_hit.shape[0]
for i in range(num_hit):
    dom_id = data_hit["id"].iloc[i]
    hit_time = data_hit["t"].iloc[i]
    doms[dom_id].get_hit(hit_time)

# true track element
track_para = np.array([-619, -616, -600, 0.865, 0.710])
track = rec.Track(*track_para)

# read track data for ploting
distances = []
numbers = []
for dom in doms:
    distances.append(track.geometry(dom)[0])
    numbers.append(dom.ni)
numbers = np.array(numbers)
distances = np.array(distances)

flux_dom = 25000
lamb_a_num = 60
distances_fit = np.arange(1, 500, 1)
numbers_fit = flux_dom * rec.DOM_R**2 / 2 / distances_fit * np.exp(-distances_fit / 0.666 / lamb_a_num)

# plot
[fig, ax1] = plt.subplots(1, 1, figsize=(5,5))
ax1.scatter(distances, numbers, s=3, alpha=.5)
ax1.plot(distances_fit, numbers_fit, color="darkred")
ax1.set_yscale("log")
ax1.set_ylim(1, 2000)
ax1.set_xlim(0, 150)
ax1.set_ylabel("Number of hits")
ax1.set_xlabel("Distance to track (m)")
plt.show()