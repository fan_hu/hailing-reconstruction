import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

# ini track
para = np.array([-619, -616, -600, 0.865, 0.710])
track = rec.Track(*para)

def read_data(files_train, files_test, files_valid):
    doms_tot = []
    for files in [files_train, files_test, files_valid]:
        data_temp = pd.DataFrame()
        for file in files:
            data_read = pd.read_csv(dirctory + file, sep=",", skiprows=5, names=["id", "x", "y", "z", "t", "wl"])
            data_temp = data_temp.append(data_read, ignore_index=True)
        # initialize dom data
        doms = []
        for i in range(20):
            for j in range(20):
                for k in range(20):
                    doms.append(rec.Dom(-475+50*i, -475+50*j, -475+50*k))
        # read hit into dom data
        num_hit = data_temp.shape[0]
        for i in range(num_hit):
            dom_id = data_temp["id"].iloc[i]
            hit_time = data_temp["t"].iloc[i]
            doms[dom_id].get_hit(hit_time)
        # select doms that get trigered
        doms_trg = []
        for dom in doms:
            if (dom.ni != 0):
                doms_trg.append(dom)
        doms_tot.append(doms_trg)
    return doms_tot
        

def select_d(d, t_res, d_min, d_max):
    t_res_rst = []
    for i in range(len(d)):
        if d_min < d[i] and d[i] < d_max:
            t_res_rst.append(t_res[i])
    return (np.array(t_res_rst))

def get_residual(track, doms):
    t = []
    d = []
    for dom in doms:
        [d_dom, t_geo_dom] = track.geometry(dom)
        for ti in dom.ti:
            t.append(ti - t_geo_dom)
            d.append(d_dom)
    rst = np.array([t, d])
    return rst

def get_res_avg(track, doms):
    t = []
    d = []
    for dom in doms:
        t_temp = np.array([])
        [d_dom, t_geo_dom] = track.geometry(dom)
        for ti in dom.ti:
            t_temp = np.append(t_temp, ti-t_geo_dom)
        t_avg = np.average(t_temp)
        d.append(d_dom)
        t.append(t_avg)
    rst = np.array([t, d])
    return rst
    




# read data
dirctory = "../data/03132030/"
Path(dirctory+"rec").mkdir(parents=True, exist_ok=True)
train = ["Muon_" + str(i) for i in range(0,6)]
test = ["Muon_" + str(i) for i in range(6,8)]
valid = ["Muon_" + str(i) for i in range(8,10)]
[doms_train, doms_test, doms_valid] = read_data(train, test, valid)
[t_train, d_train] = get_residual(track, doms_train)
[t_train_avg, d_train_avg] = get_res_avg(track, doms_train)
[t_test, d_test] = get_residual(track, doms_test)

# select by distance
t5 = select_d(d_train, t_train, 4, 6)
t10 = select_d(d_train, t_train, 8.5, 9.5)
t20 = select_d(d_train, t_train, 18.5, 21.5)
t40 = select_d(d_train, t_train, 38, 42)
t60 = select_d(d_train, t_train, 57.5, 62.5)
t80 = select_d(d_train, t_train, 77, 83)
t120 = select_d(d_train, t_train, 115, 125)
t200 = select_d(d_train, t_train, 190, 210)

[fig, [ax0, ax1, ax2, ax3, ax4]] = plt.subplots(1, 5, figsize=(20,4))
[n10, bins10, patches10] = ax0.hist(t10, bins=40, range=(-5,20))
ax0.set_xlim(-5, 20)
ax0.set_yscale('log')
ax0.set_xlabel('Residual time (ns)')
# ax0.set_title('d=10')
ax1.hist(t20, bins=40, range=(-5,40))
ax1.set_xlim(-5, 40)
ax1.set_yscale('log')
ax1.set_xlabel('Residual time (ns)')
# ax0.set_title('d=20')
ax2.hist(t40, bins=40, range=(-5,60))
ax2.set_xlim(-5, 60)
ax2.set_yscale('log')
ax2.set_xlabel('Residual time (ns)')
# ax0.set_title('d=40')
ax3.hist(t60, bins=40, range=(-10,90))
ax3.set_xlim(-10, 90)
ax3.set_yscale('log')
ax3.set_xlabel('Residual time (ns)')
# ax0.set_title('d=60')
ax4.hist(t80, bins=30, range=(-10,120))
ax4.set_xlim(-10, 120)
ax4.set_yscale('log')
ax4.set_xlabel('Residual time (ns)')
# ax0.set_title('d=80')
plt.show()

# Scattering plot
plt.scatter(d_train, t_train, s=3, alpha=.3)
plt.scatter(d_train_avg, t_train_avg, s=3, alpha=.7)
plt.xlim(0,150)
plt.ylim(-5, 60)
plt.xlabel("distance (m)")
plt.ylabel("residual time (ns)")
plt.savefig("res.png")
plt.show()
