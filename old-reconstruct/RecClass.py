import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from numpy import pi, power, log, log10, sqrt, exp
import json
import copy
import itertools

# define constants
REF_IDX = 1.343
THETA_CK = np.arccos(1/REF_IDX)
TAN_CK = np.tan(THETA_CK)
SIN_CK = np.sin(THETA_CK)
C_LT = 0.2998
C_MED = C_LT / REF_IDX
DOM_R = 0.25
TIME_COR = DOM_R / C_MED
SCALE = np.array([1000, 1000, 1000, pi, 2*pi])
ABS_NUM = 60
FLUX_NUM = 83000
TRUNC_FACTOR = 1000
TAU0 = 30
DIRECTION_LIST = np.array(
    [[1, 0, 0], [-1, 0, 0], [0, 1, 0], [0, -1, 0], [0, 0, 1], [0, 0, -1]])
directory = "/Users/apple/data/06051800/"
directoryTime = "/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeRst/"


def gaussian(x, delta, sigma):
    if (x > delta-3*sigma or x < delta+3*sigma):
        norml = 1 / sqrt(2*pi) / sigma
        return norml * exp(-power(x-delta, 2) / sigma**2 / 2)
    else:
        return 0


def gamma(x, a):
    if (x > 0):
        return power(x, a-1) * exp(-x) / sc.gamma(a)
    else:
        return 0


def spl_gamma(x, alpha, sig):
    if (x > 1.5*sig):
        return gamma(x, alpha)
    y = 1.5 * sig
    a1 = [y**3, y**2, 1/sqrt(2*pi)/sig]
    a2 = [3*y**2, 2*y, 0]
    a3 = [y**4/4, y**3/3, 1.5/sqrt(2*pi)+0.5]
    a = np.array([a1, a2, a3])
    b1 = power(y, alpha-1) * exp(-y) / sc.gamma(alpha)
    b2 = ((alpha-1)/y - 1) * b1
    b3 = 1 - sc.gammaincc(alpha, y)
    b = np.array([b1, b2, b3])
    [f1, f2, fac] = np.linalg.solve(a, b)
    if (x > 0):
        return f1*x**3 + f2*x**2 + fac/sqrt(2*pi)/sig
    else:
        return fac/sqrt(2*pi)/sig * exp(-x**2/2/sig**2)


def normalize(x, d, beta):
    if (x > 0):
        return d * log(x / d + 1) / beta
    else:
        return x / beta


class Dom:
    '''
    This class defines DOM by its position.
    '''

    def __init__(self, xi: float = 0, yi: float = 0, zi: float = 0):
        self.id = np.array([xi, yi, zi])
        self.ri = np.array([-475+50*xi, -475+50*yi, -475+50*zi])
        self.ni = 0
        self.hits = []

    def get_hit(self, ti: float, direc, pi: np.ndarray, pmts: bool = True):
        '''
        Receive hit coresponding to the dom by its hit time

        Parameters
        ----------
        `ti : float`
          Time when the photon arrive the DOM surface

        `direc : array_like`
          3D vector from DOM center to hit position

        `pi : array_like`
          3D vector represents the momentum direction of photon

        `pmts : bool`
          True to set up multi PMT configure.
          The direction of PMT is indicated by DIRECTION_LIST

        Return
        ------
        `self : Dom`

        '''
        if (pmts):
            for i in range(len(DIRECTION_LIST)):
                if ((direc.dot(DIRECTION_LIST[i]) > 0.86) and (direc.dot(pi) < -0.3)):
                    self.hits.append([ti, i, 0b0])
                    self.ni += 1
        else:
            if (direc.dot(pi) < -0.3):
                self.hits.append([ti, -1, 0b0])
                self.ni += 1
        return self

    def sort(self):
        if len(self.hits) > 1:
            self.hits.sort_values(by='t', inplace=True)
        self.hits.reset_index(drop=True, inplace=True)
        return self

    def to_pd(self):
        self.hits = pd.DataFrame(data=self.hits, columns=['t', 'id', 'lv'])
        self.hits = self.hits.astype(
            dtype={'t': 'float', 'id': 'int8', 'lv': 'uint8'})
        return self

    def re_ini(self):
        self.ni = len(self.hits)
        return self


def read_dom(directory: str, muonID: int, trigger=True, pmts=True):
    # read data from file
    data_hit = pd.read_csv(directory+"hits_"+str(muonID), sep=",", skiprows=1, names=[
                           "id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "dist", "costh", "t", "t_res", "wl"])
    data_hit = data_hit.sort_values(by="t")
    # initialize dom class
    doms = [Dom(i, j, k) for i in range(20)
            for j in range(20) for k in range(20)]
    # read hit data into dom s
    num_hit = data_hit.shape[0]
    for i in range(num_hit):
        dom_id = data_hit["id"].iloc[i]
        hit_time = data_hit["t"].iloc[i]
        nx = data_hit["nx"].iloc[i]
        ny = data_hit["ny"].iloc[i]
        nz = data_hit["nz"].iloc[i]
        ni = np.array([nx, ny, nz])
        px = data_hit["px"].iloc[i]
        py = data_hit["py"].iloc[i]
        pz = data_hit["pz"].iloc[i]
        pi = np.array([px, py, pz])
        doms[dom_id].get_hit(hit_time, ni, pi, pmts)

    # remove dom with 0 hit
    doms = [dom for dom in doms if dom.ni != 0]
    # sort and trigger
    for dom in doms:
        dom.to_pd()
        dom.re_ini()
        dom.sort()
    doms = hit_trigger(doms)
    return doms


def plot_dom(doms: list, directory: str, muonID: int):
    # calculate hit number
    domsXY = [[sum([dom.ni for dom in doms if dom.id[0] == i and dom.id[1] == j])
               for j in range(20)] for i in range(20)]
    domsYZ = [[sum([dom.ni for dom in doms if dom.id[1] == j and dom.id[2] == k])
               for k in range(20)] for j in range(20)]
    domsXZ = [[sum([dom.ni for dom in doms if dom.id[0] == i and dom.id[2] == k])
               for k in range(20)] for i in range(20)]

    # plot circle
    fig, axs = plt.subplots(2, 2, figsize=(8, 8), dpi=150)
    axs[1][1].remove()
    plot_circ(axs[0][0], domsXZ, '', 'Z')
    plot_circ(axs[0][1], domsYZ, 'Y', '', invertX=True)
    plot_circ(axs[1][0], domsXY, 'X', 'Y')
    fig.savefig(directory + 'figs/' + 'track_' + str(muonID) + '.png')


def plot_circ(ax, data, xlabel, ylabel, invertX=False):
    for i, j in itertools.product(range(20), range(20)):
        if (data[i][j] > 0):
            ax.add_patch(plt.Circle(
                (-475+50*i, -475+50*j), data[i][j]**0.3 * 10.0))
            if invertX:
                ax.set_xlim(500, -500)
            else:
                ax.set_xlim(-500, 500)
            ax.set_ylim(-500, 500)
            ax.set_xlabel(xlabel)
            ax.set_ylabel(ylabel)


def hit_trigger(doms):
    # hits clustering in 10 ns
    for dom in doms:
        if len(dom.hits) == 0:
            continue
        try:
            for i in range(len(dom.hits)-1):
                if dom.hits.loc[i+1, 't'] - dom.hits.loc[i, 't'] < 10:
                    dom.hits.loc[i:i+2, 'lv'] |= 0b01
            for i in range(len(dom.hits)-2):
                if dom.hits.loc[i+2, 't'] - dom.hits.loc[i, 't'] < 10:
                    dom.hits.loc[i:i+3, 'lv'] |= 0b010
            for i in range(len(dom.hits)-3):
                if dom.hits.loc[i+3, 't'] - dom.hits.loc[i, 't'] < 10:
                    dom.hits.loc[i:i+4, 'lv'] |= 0b0100
        except IndexError:
            continue
    return doms


def hit_clean_causality(domsInput):
    doms = copy.deepcopy(domsInput)
    nMax = 0
    iMax = 0
    for i in range(len(doms)):
        if doms[i].ni > nMax:
            nMax = doms[i].ni
            iMax = i
    tRef = min(doms[iMax].hits['t'])
    riRef = doms[iMax].ri
    for i in range(len(doms)):
        popList = []
        riDel = doms[i].ri - riRef
        riDel = sqrt(riDel.dot(riDel))
        for index, hit in doms[i].hits.iterrows():
            tiDel = abs(hit['t'] - tRef)
            if tiDel-riDel/C_MED > 20 or abs(tiDel-riDel/C_LT) > 400:
                popList.append(index)
        doms[i].hits.drop(index=popList, inplace=True)
        doms[i].re_ini()
    doms = [dom for dom in doms if dom.ni != 0]
    return doms

# not useful


def hit_clean_level(domsInput, lvCut):
    doms = copy.deepcopy(domsInput)
    for i in range(len(doms)):
        popList = []
        for index, hit in doms[i].hits.iterrows():
            if hit['lv'] < lvCut:
                popList.append(index)
        doms[i].hits.drop(index=popList, inplace=True)
        doms[i].re_ini()
    doms = [dom for dom in doms if dom.ni != 0]
    return doms


def hit_received(doms):
    num = 0
    for dom in doms:
        num += dom.ni
    return num


class Point:
    def __init__(self, x0=0, y0=0, z0=0, t0=0):
        self.r0 = np.array([x0, y0, z0])
        self.t0 = t0

    def para(self):
        return np.append(self.r0, self.t0)

    def geometry(self, dom):
        r0i = dom.ri - self.r0
        d = sqrt(r0i.dot(r0i))
        t_geo = self.t0 + d / C_MED
        return [d, t_geo]

    def direction_vec(self, dom):
        r0i = dom.ri - self.r0
        return r0i / sqrt(r0i.dot(r0i))


class Track:
    '''
    This class defines a track by 6 parameters
    '''

    def __init__(self, x0=0, y0=0, z0=0, theta=0, phi=0, t0=0):
        self.r0 = np.array([x0, y0, z0])
        self.t0 = t0
        self.theta = theta
        self.phi = phi
        self.p0 = np.array(
            [np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])

    def para(self):
        '''
        Show trakc paramters
        '''
        return np.append(self.r0, np.array([self.theta, self.phi, self.t0]))

    def length(self):
        la = 0
        while la < 2000:
            la += 1
            pos = self.r0 + la * self.p0
            if sum(np.append(pos > 500, pos < -500)):
                break
        lb = 0
        while lb < 2000:
            lb += 1
            pos = self.r0 - lb * self.p0
            if sum(np.append(pos > 500, pos < -500)):
                break
        return la+lb

    def geometry(self, dom):
        '''
        Parameter: DOM object. 
        Return: [d, t_geo]
        '''
        r0i = dom.ri - self.r0
        l = self.p0.dot(r0i)
        d = sqrt(r0i.dot(r0i) - l**2)
        if (d < DOM_R):
            d = DOM_R * exp(d / DOM_R - 1)
        t_geo = self.t0 + (l + d * TAN_CK) / C_LT
        return [d, t_geo]

    def direction_vec(self, dom):
        r0i = dom.ri - self.r0
        l = self.p0.dot(r0i)
        d = sqrt(r0i.dot(r0i) - l**2)
        rs = self.r0 + self.p0 * (l - d / TAN_CK)
        rsi = dom.ri - rs
        return rsi / sqrt(rsi.dot(rsi))

    def time_shift(self, doms):
        '''
        Shift t0 of the track to make sure all residual time is larger than zero
        Return: time shifted
        '''
        t_geo = self.geometry(doms[0])[1]
        tmin = doms[0].hits['t'].iloc[0] - t_geo
        for dom in doms:
            t_geo = self.geometry(dom)[1]
            tmin = min(dom.hits['t'].iloc[0] - t_geo, tmin)
        self.t0 += tmin
        return tmin

    '''
    likeli is defined as log(likelihood)
    '''

    def likeli_time(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            alpha = d / paraAvg["lam"]
            for ti in dom.ti:
                t_res = ti - t_geo
                t = TAU0 * log(t_res / TAU0 + 1) / paraAvg["beta"]
                prob = gamma(t, alpha)
                likeli += log10(prob)
        return likeli

    def likeli_num(self, doms):
        likeli = 0
        for dom in doms:
            d = self.geometry(dom)[0]
            p_hit1 = exp(-d/ABS_NUM)
            n = FLUX_NUM * DOM_R**2 / 2 / d * exp(-d/SIN_CK/ABS_NUM) / p_hit1
            if (dom.ni == 0):
                likeli += n * log10(1 - p_hit1)
            else:
                likeli += log10(1 - power(1 - p_hit1, n))
        return likeli

    def likeli_M_estimator(self, doms):
        likeli = 0
        for dom in doms:
            t_geo = self.geometry(dom)[1]
            t_res = dom.hits['t'].to_numpy() - t_geo
            likeli += np.sum(-2*sqrt(1+pow(t_res, 2)/2)+2)
        return likeli

    def likeli_avg(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            t_res = dom.hits['t'].to_numpy() - t_geo
            t = np.array([normalize(t_temp, 30, paraAvg["beta"])
                          for t_temp in t_res])
            t_avg = np.average(t)
            n = dom.ni
            likeli -= log(d)/2 + (t_avg - d /
                                  paraAvg["lam"])**2 / (2 * d / n / paraAvg["lam"])
            if np.isnan(likeli):
                print("Error")
                print("dom.ri: ", dom.ri)
                print("t_avg: ", t_avg)
                print("d: ", d)
                print("n: ", n)
                print("track_para: ", self.para())
                break
        return likeli

    def likeli_avgNew(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            t_res = dom.ti - t_geo
            t = np.array([])
            for t_temp in t_res:
                t = np.append(t, t_temp)
            t_avg = np.average(t)
            n = dom.ni
            average = 11 * (np.exp(d / 11 * 0.22) - 1) - TIME_COR
            sigma = 0.95 * d + 5
            likeli -= log(2*pi)/2 + log(sigma/sqrt(n)) + \
                (t_avg-average)**2 / (2*sigma**2/n)
            if np.isnan(likeli):
                print("Error")
                print("dom.ri: ", dom.ri)
                print("t_avg: ", t_avg)
                print("d: ", d)
                print("n: ", n)
                print("track_para: ", self.para())
                break
        return likeli

    def likeli_avgPmts(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            rsi = self.direction_vec(dom)
            t_temp = []
            n_temp = 0
            for i in range(len(DIRECTION_LIST)):
                if (dom.n_pmts[i] == 0):
                    continue
                costh = -rsi.dot(DIRECTION_LIST[i])
                if (costh > 0):
                    t_temp += dom.t_pmts[i]
                    n_temp += dom.n_pmts[i]
                else:
                    for time in dom.t_pmts[i]:
                        t_res = time - t_geo
                        tau = (paraPmts["sigC"] + paraPmts["facB"]
                               * sqrt(d))**2 / (paraPmts["facA"]*sqrt(d))
                        alpha = (paraPmts["facA"]*sqrt(d))**2 / \
                            (paraPmts["sigC"] + paraPmts["facB"]*sqrt(d))**2
                        t_norm = normalize(t_res, 30, tau)
                        likeli += log(spl_gamma(t_norm, alpha, alpha) / tau)
            if (n_temp == 0):
                continue
            t_res = t_temp - t_geo
            t = np.array([])
            for t_res_temp in t_res:
                t = np.append(t, normalize(t_res_temp, 30, paraPmts["beta"]))
            t_avg = np.average(t)
            n = n_temp
            alpha = d / paraPmts["lam"]
            likeli -= log(2*pi)/2 + log(alpha/n)/2 + \
                (t_avg-alpha)**2 / (2*alpha/n) + log(paraPmts["beta"])
            if np.isnan(likeli):
                print("Error")
                print("dom.ri: ", dom.ri)
                print("t_avg: ", t_avg)
                print("d: ", d)
                print("n: ", n)
                print("track_para: ", self.para())
                break
        return likeli

    def likeli_tot(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            iIdx = np.searchsorted(d_min, d) - 1
            timeDataSel = timeData[iIdx]
            probDataSel = probData[iIdx]
            for time in (dom.ti-t_geo):
                tIdx = np.searchsorted(timeDataSel, time)
                if (tIdx == 0 or tIdx == len(timeDataSel)):
                    prob = 0.0001
                else:
                    prob = probDataSel[tIdx]
                likeli += np.log(prob)
            if np.isnan(likeli):
                print("Error")
                print("dom.ri: ", dom.ri)
                print("d: ", d)
                print("track_para: ", self.para())
                break
        return likeli

    def likeli_avgDrop(self, doms):
        likeli = 0
        for dom in doms:
            [d, t_geo] = self.geometry(dom)
            rsi = self.direction_vec(dom)
            t_temp = []
            n_temp = 0
            for i in range(len(DIRECTION_LIST)):
                if (dom.n_pmts[i] == 0):
                    continue
                costh = -rsi.dot(DIRECTION_LIST[i])
                if (costh > 0):
                    t_temp += dom.t_pmts[i]
                    n_temp += dom.n_pmts[i]
            if (n_temp == 0):
                continue
            t_res = t_temp - t_geo
            t = np.array([])
            for t_temp in t_res:
                t = np.append(t, normalize(t_temp, 30, paraDrop["beta"]))
            t_avg = np.average(t)
            n = n_temp
            likeli -= log(d)/2 + (t_avg - d /
                                  paraDrop["lam"])**2 / (2 * d / n / paraDrop["lam"])
            if np.isnan(likeli):
                print("Error")
                print("dom.ri: ", dom.ri)
                print("t_avg: ", t_avg)
                print("d: ", d)
                print("n: ", n)
                print("track_para: ", self.para())
                break
        return likeli


def pol2rect(theta, phi):
    return np.array([np.cos(phi)*np.sin(theta), np.sin(phi)*np.sin(theta), np.cos(theta)])


def rect2pol(p0):
    p0 /= sqrt(p0.dot(p0))
    theta = np.arccos(p0[2])
    phi = np.arctan2(p0[1], p0[0])
    return np.array([theta, phi])


def angle_err(angle1, angle2):
    p1 = pol2rect(*angle1)
    p2 = pol2rect(*angle2)
    delta = np.arccos(p1.dot(p2))
    return delta * 180 / pi * 180


def read_para(directory, muonID, model):
    if (model == "sim"):
        data_para = pd.read_csv(directory+"particle_"+str(muonID), sep=",", skiprows=1,
                                nrows=1, names=["x0", "y0", "z0", "t0", "theta", "phi", "E0", "type"])
        para = data_para.drop(columns=['t0', 'E0', "type"]).to_numpy()[0]
        return para
    else:
        para = pd.read_csv(directory+"rec/particle_"+str(muonID)+"_"+model, sep=",",
                           skiprows=1, names=["x0", "y0", "z0", "theta", "phi"]).to_numpy()[0]
        return para


def write_opt_rst(paraOpt, directory, muonID, model):
    with open(directory+'rec/'+'particle_'+str(muonID)+'_'+model, 'w') as fl:
        fl.write(
            "x, y, z, theta, phi \n {:.3f}, {:.3f}, {:.3f}, {:.7f}, {:.7f}" .format(*paraOpt))


def write_pandel_para(beta, lam):
    pandelPara = {"timeScale": beta, "distanceScale": lam}
    with open("ParaPandel.json", 'w') as f:
        json.dump(pandelPara, f, indent=4)


def read_pandel_para():
    with open("ParaPandel.json", 'r') as f:
        pandelPara = json.load(f)
    global paraAvg
    paraAvg = {"beta": pandelPara["timeScale"],
               "lam": pandelPara["distanceScale"]}
    print("Using following parameters: ")
    print("Time Sclae: {:.3f} , Distance Scale: {:.3f}".format(
        *paraAvg.values()))


def write_pmts_para(beta, lam, beta_ops, lam_ops):
    pmtsPara = {"timeScale": beta, "distanceScale": lam,
                "timeScaleOpposite": beta_ops, "distanceScaleOpposite": lam_ops}
    with open("ParaPmts.json", 'w') as f:
        json.dump(pmtsPara, f, indent=4)


def read_pmts_para():
    with open("ParaPmts.json", 'r') as f:
        pmtsPara = json.load(f)
    global paraPmts
    paraPmts = {"beta": pmtsPara["timeScale"], "lam": pmtsPara["distanceScale"],
                "facA": pmtsPara["factorA"], "facB": pmtsPara["factorB"], "sigC": pmtsPara["sigmaC"]}
    print("Using following parameters: ")
    print(paraPmts)


def write_pmtsDrop_para(beta, lam):
    paraDrop = {"timeScale": beta, "distanceScale": lam}
    with open("paraDrop.json", 'w') as f:
        json.dump(paraDrop, f, indent=4)


def read_pmtsDrop_para():
    with open("paraDrop.json", 'r') as f:
        pmtsDropPara = json.load(f)
    global paraDrop
    paraDrop = {"beta": pmtsDropPara["timeScale"],
                "lam": pmtsDropPara["distanceScale"]}
    print("Using following parameters: ")
    print("Time Sclae: {:.3f} , Distance Scale: {:.3f}".format(
        *paraDrop.values()))


def read_timeResidual():
    global d_min
    d_min = [0.0, ]
    for i in range(40):
        d_add = max(1.0, 0.2*np.exp(i/10))
        d_min.append(d_min[-1] + d_add)
    d_min = np.array(d_min)

    global timeData, probData
    timeData = []
    probData = []
    for i in range(len(d_min)):
        timeDataTemp = pd.read_csv(directoryTime+'I'+str(i)+'time.csv',
                                   sep=",", skiprows=0, names=['time'])['time'].to_numpy()
        probDataTemp = pd.read_csv(directoryTime+'I'+str(i)+'prob.csv',
                                   sep=",", skiprows=0, names=['time'])['time'].to_numpy()
        timeData.append(timeDataTemp)
        probData.append(probDataTemp)
    print("Read time data succeed!")


def minimize(likeliMethod, doms, paraIni, timeShift, minimizeMethod, minimizeOptitions):
    def func_minimize(paras):
        track = Track(*paras, timeShift)
        method = getattr(track, "likeli_"+likeliMethod)
        return -method(doms)
    optimize = so.minimize(func_minimize, paraIni,
                           method=minimizeMethod, options=minimizeOptitions)
    return optimize


def grid(angles, gridLim, gridSize):
    thetaList = np.linspace(angles[0]-gridLim, angles[0]+gridLim, gridSize)
    phiList = np.linspace(angles[1]-gridLim, angles[1]+gridLim, gridSize)
    angleGrid = np.zeros((gridSize, gridSize, 2))
    for i in range(gridSize):
        for j in range(gridSize):
            angleGrid[i][j] = np.array([thetaList[i], phiList[j]])
    return angleGrid


def angle_likeli(grid, likeliMethod, doms, paraOpt, timeShift):
    def func_likeli(angles):
        [x, y, z] = paraOpt[:3]
        track = Track(x0=x, y0=y, z0=z,
                      theta=angles[0], phi=angles[1], t0=timeShift)
        method = getattr(track, "likeli_"+likeliMethod)
        return -method(doms)
    angleLikeli = np.zeros(grid.shape[:2])
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            angleLikeli[i][j] = func_likeli(grid[i][j])
    return angleLikeli


def sigma_matrix(likeliMethod, doms, paraOpt, timeShift, delta=0.0005, minimizeMethod="Nelder-Mead", minimizeOptitions={'maxiter': 100, 'disp': True}):
    # # doing a specific minimize at each point
    # def func_likeli(angles):
    #     def func_minimize(paras):
    #         [x0, y0, z0] = paras
    #         track = Track(x0=x0, y0=y0, z0=z0, theta=angles[0], phi=angles[1], t0=timeShift)
    #         method = getattr(track, "likeli_"+likeliMethod)
    #         return -method(doms)
    #     optimize = so.minimize(func_minimize, paraOpt[:3], method=minimizeMethod, options=minimizeOptitions)
    #     return optimize.fun

    # using the smae x y z at each point
    def func_likeli(angles):
        [x0, y0, z0] = paraOpt[:3]
        track = Track(x0, y0, z0, angles[0], angles[1])
        method = getattr(track, "likeli_"+likeliMethod)
        return -method(doms)

    anglesOpt = paraOpt[-2:]
    likeli = np.zeros((3, 3))
    anglesGrid = np.zeros((3, 3, 2))
    for i in range(3):
        for j in range(3):
            anglesGrid[i][j] = anglesOpt + [delta * (i-1), delta * (j-1)]
            likeli[i][j] = func_likeli(anglesGrid[i][j])
    d2Ldx2 = (likeli[2][1] + likeli[0][1] - 2*likeli[1][1]) / delta**2
    d2Ldy2 = (likeli[1][2] + likeli[1][0] - 2*likeli[0][1]) / delta**2
    d2Ldxdy = (likeli[2][2] + likeli[0][0] - likeli[2]
               [0] - likeli[0][2]) / delta**2 / 4
    sigmaMatrix = [[d2Ldx2, d2Ldxdy], [d2Ldxdy, d2Ldy2]]
    pcc = sigmaMatrix[1][0] / sqrt(sigmaMatrix[0][0] * sigmaMatrix[1][1])
    return [sigmaMatrix, pcc]


def angle_sigma(grid, sigmaMatrix):
    def func_sigma(angles):
        return exp(angles.dot(sigmaMatrix).dot(angles.T)/2)
    angleSigma = np.zeros(grid.shape[:2])
    for i in range(grid.shape[0]):
        for j in range(grid.shape[1]):
            angleSigma[i][j] = func_sigma(grid[i][j])
    return angleSigma


def figure_likeli(grid, angleLikeli, paraOpt, paraSim, directory, muonID, likeliModel):
    from matplotlib import ticker, cm
    listTheta = grid[:, 0, 0] * 180 / pi
    listPhi = grid[0, :, 1] * 180 / pi
    angleLikeli_v = angleLikeli - angleLikeli.min()
    likeli_range = power(angleLikeli.max() - angleLikeli.min(), 0.5)
    levels = np.linspace(0, likeli_range, 7)
    levels = power(levels, 2)
    angleErr = angle_err(paraOpt[-2:], paraSim[-2:])
    # visualize the Likelihood
    plt.figure(figsize=(5.5, 4), dpi=300)
    cs = plt.contourf(listTheta-paraOpt[-2]*180/pi, listPhi-paraOpt[-1]*180/pi, angleLikeli_v, levels, cmap=cm.PuBu_r)
    cbar = plt.colorbar(cs)
    plt.plot(paraSim[-2]*180/pi-paraOpt[-2]*180/pi, paraSim[-1]
             * 180/pi-paraOpt[-1]*180/pi, 'x', c="lightgreen")  # simulation point
    plt.plot(0, 0, '.', c="tomato")  # optimize point
    font = {'family': 'serif', 'color':  'darkred',
            'weight': 'normal', 'size': 12}
    plt.text(-0.4, 0.4, "angle error = {:.1f} minute".format(angleErr),
             fontdict=font, bbox=dict(facecolor='lightblue', alpha=0.5))
    plt.title(r"Countour of $\ln \mathcal{L}(\theta,\phi) - \ln \mathcal{L}_\mathrm{min}$")
    plt.xlabel(r"$\Delta \theta$" + r"$~\mathrm{[degree]}$")
    plt.ylabel(r"$\Delta \phi$" + r"$~\mathrm{[degree]}$")
    plt.xlim(-0.5, 0.5)
    plt.ylim(-0.5, 0.5)
    plt.savefig(directory + 'rec/' + 'particle_' +
                str(muonID) + '_' + likeliModel + '.png')
    plt.close()


def figure_sigma(grid, angleSigma, pcc, paraOpt, paraSim, directory, muonID):
    listTheta = grid[:, 0, 0] * 180 * 180 / pi
    listPhi = grid[0, :, 1] * 180 * 180 / pi
    angleErr = angle_err(paraOpt[-2:], paraSim[-2:])
    # visualize the Likelihood
    plt.figure(figsize=(5.5, 4), dpi=300)
    levels = [exp(-i**2/9/sqrt(1-pcc**2)) for i in range(-3, 0)]
    plt.contour(listTheta, listPhi, angleSigma, levels=levels)

    plt.plot(0, 0, '.')
    plt.plot((paraSim[-2]-paraOpt[-2])*180*180/pi,
             (paraSim[-1]-paraOpt[-1])*180*180/pi, 'x')
    font = {'family': 'serif', 'color':  'darkred',
            'weight': 'normal', 'size': 12}
    plt.text(listTheta[1], listPhi[-5], "angle error = {:.1f} minute".format(
        angleErr), fontdict=font, bbox=dict(facecolor='lightblue', alpha=0.5))
    plt.xlabel(r"$\Delta\theta ~\mathrm{[minute]}$")
    plt.ylabel(r"$\Delta\phi ~\mathrm{[minute]}$")
    plt.xlim(listTheta[0], listTheta[-1])
    plt.ylim(listPhi[0], listPhi[-1])
    plt.savefig(directory + 'rec/' + 'particle_' + str(muonID) + '_sigma.png')
    plt.close()




# test script
if __name__ == "__main__":
    likeliModel = 'avg'
    read_pandel_para()
    doms = read_dom(directory, 0, trigger=True)
    paraIni = read_para(directory, 0, "TF")
    paraSim = read_para(directory, 0, "sim")
    trackIni = Track(*paraIni)
    timeShift = trackIni.time_shift(doms)
    optimize = minimize(likeliModel, doms, paraIni, timeShift, "Nelder-Mead", {'maxiter': 3000, 'disp': True})
    paraOpt = optimize.x
    gridLikeli = grid(paraOpt[-2:], 0.01, 30)
    likeli = angle_likeli(gridLikeli, likeliModel, doms, paraOpt, timeShift)
    figure_likeli(gridLikeli, likeli, paraOpt, paraSim, directory, 0, likeliModel)