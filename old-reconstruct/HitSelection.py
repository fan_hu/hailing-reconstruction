import pandas as pd
import numpy as np

dirData = "/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeData/"
dirSel = "/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeSelect/"

d_min = [0.0, ]
for i in range(40):
    d_add = max(1.0, 0.2*np.exp(i/10))
    d_min.append(d_min[-1] + d_add)



for fileID in range(100):
    data = pd.read_csv(dirData+'data'+str(fileID)+'.csv', sep=',', skiprows=0, names=['dist', 'costh', 'tRes'])
    for i in range(len(d_min)):
        # data selecting
        if (i != len(d_min)-1):
            mask = (data['dist'] >= d_min[i]) & (data['dist'] < d_min[i+1])
        else:
            mask = data['dist'] >= d_min[i]
        data_select = data[mask]
        time_array = data_select['tRes']
        time_array.to_csv(dirSel+'I'+str(i)+'.csv', mode='a', header=False, index=False)
