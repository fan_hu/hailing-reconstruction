import RecClass as rec
from pathlib import Path
import numpy as np

directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
likeliModel = 'avg'
rec.read_pandel_para()
for id in range(100):
    print("Doing Particle {:d}".format(id))
    doms = rec.read_dom(directory, id, trigger=True)
    paraIni = rec.read_para(directory, id, "TF")
    paraSim = rec.read_para(directory, id, "sim")
    trackIni = rec.Track(*paraIni)
    timeShift = trackIni.time_shift(doms)
    optimize = rec.minimize(likeliModel, doms, paraIni, timeShift, "Nelder-Mead", {'maxiter': 3000, 'disp': True})
    paraOpt = optimize.x
    rec.write_opt_rst(paraOpt, directory, id, likeliModel)
    # paraOpt = rec.read_para(directory, id, "avg")
    gridLikeli = rec.grid(paraOpt[-2:], 0.05, 30)
    likeli = rec.angle_likeli(gridLikeli, likeliModel, doms, paraOpt, timeShift)
    rec.figure_likeli(gridLikeli, likeli, paraOpt, paraSim, directory, id, likeliModel)
    # [hess, pcc] = rec.sigma_matrix(likeliModel, doms, paraOpt, timeShift)
    # # hess = -np.linalg.inv(optimize.hess_inv)[-2:,-2:]
    # pcc = hess[1][0] / np.sqrt(hess[0][0] * hess[1][1])
    # print("Hess: ")
    # print(hess)
    # print("pcc: ", pcc)
    # gridSigma = rec.grid(np.array([0,0]), 3/np.power(np.linalg.det(hess),0.25), 30)
    # sigma = rec.angle_sigma(gridSigma, hess)
    # rec.figure_sigma(gridSigma, sigma, pcc, paraOpt, paraSim, directory, id)
