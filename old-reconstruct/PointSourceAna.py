import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
import RecClass as rec
import pandas as pd

directory = "../data/04011058d100/"
data_hit = pd.read_csv(directory+"hits_"+str(0), sep=",", skiprows=1, names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "t", "wl"])
for i in range(1, 10):
    data_temp = pd.read_csv(directory+"hits_"+str(i), sep=",", skiprows=1, names=["id", "x", "y", "z", "nx", "ny", "nz", "px", "py", "pz", "t", "wl"])
    data_hit = data_hit.append(data_temp, ignore_index=True)

dropList = []
for i in range(len(data_hit)):
    nx = data_hit["nx"].iloc[i]
    ny = data_hit["ny"].iloc[i]
    nz = data_hit["nz"].iloc[i]
    ni = np.array([nx, ny, nz])
    px = data_hit["px"].iloc[i]
    py = data_hit["py"].iloc[i]
    pz = data_hit["pz"].iloc[i]
    pi = np.array([px, py, pz])
    if (ni.dot(pi) > -0.3):
        dropList.append(i)
data_hit = data_hit.drop(dropList)

data_array = data_hit.drop(columns=['id', 'x', 'y', 'z', 'nx', 'ny', 'px', 'py', 'pz', 'wl']).to_numpy()
time = data_array[:, 1] - 100 / rec.C_MED
costh = -data_array[:, 0]
costhBin = np.zeros(costh.shape)
binSize = 20
cosmin = np.zeros(binSize)
cosmax = np.zeros(binSize)
for j in range(binSize):
    cosmin[j] = np.cos(np.pi*(j+1)/binSize)
    cosmax[j] = np.cos(np.pi*j/binSize)

for i in range(len(costh)):
    for j in range(binSize):
        if (costh[i] >= cosmin[j] and costh[i] < cosmax[j]):
            costhBin[i] = j
            continue
time_costhBin = np.array([time, costhBin]).T
timeListAntiSide = time_costhBin[time_costhBin[:,1] > 9][:,0]

[timeAvg, timeSig, num] = np.zeros((3,binSize))
for j in range(binSize):
    time_costhBin_select = time_costhBin[time_costhBin[:,1] == j][:,0]
    timeAvg[j] = time_costhBin_select.mean()
    timeSig[j] = time_costhBin_select.std()
    num[j] = len(time_costhBin_select)
costh = np.cos([np.pi*(i+0.5)/binSize for i in range(binSize)])
numCmu = np.zeros(binSize)
numCmu[-1] = num[-1]
for i in range(1, binSize):
    numCmu[-(i+1)] = numCmu[-i] + num[-(i+1)]

[fig, [ax0, ax1]] = plt.subplots(1, 2, figsize=(14,5), dpi=300)
fig.subplots_adjust(wspace=.3)
ax0.plot(costh, numCmu, c='darkred')
font0 = {'family': 'serif', 'color':  'darkred', 'weight': 'normal', 'size': 12}
fontx = {'family': 'serif', 'weight': 'normal', 'size': 12}
ax0.set_xlim(1, -1)
ax0.set_xlabel(r"$\cos\theta$", fontdict=fontx)
ax0.set_ylim(0.5, max(numCmu)*2)
ax0.set_yscale("log")
ax0.set_ylabel("number of hits", fontdict=font0)
ax0.tick_params(axis='y', labelcolor='darkred')
ax0.set_title(r"property of $t_{\mathrm{res}}$ as a function of $\cos\theta$", fontdict=fontx)

ax2 = ax0.twinx()
ax2.errorbar(costh, timeAvg, timeSig, c='royalblue')
font2 = {'family': 'serif', 'color':  'royalblue', 'weight': 'normal', 'size': 12}
ax2.set_ylim(-50, 280)
ax2.set_ylabel("average residual time", fontdict=font2)
ax2.tick_params(axis='y', labelcolor='royalblue')

ax1.hist(timeListAntiSide, bins=20, range=(0,150))
ax1.set_yscale('log')
ax1.set_ylim(0.5, )
ax1.set_xlabel("residual time", fontdict=fontx)
ax1.set_ylabel("distribution", fontdict=fontx)
ax1.set_title(r"distribution of $t_{\mathrm{res}}$ with $\cos\theta < 0$", fontdict=fontx)






# directory = rec.directory
# doms = rec.read_single_dom(directory, 0)
# dom = doms[0]
# point = rec.Point(0, 0, 0, 0)
# [d, t_geo] = point.geometry(dom)
# t_res = dom.ti - t_geo

# t_ford = np.array(dom.t_pmts[-1] - t_geo)
# t_back = np.array(dom.t_pmts[-2] - t_geo)
# t_side = np.array((dom.t_pmts[0] + dom.t_pmts[1] + dom.t_pmts[2] + dom.t_pmts[3]) - t_geo)
# [t_side.mean(), t_side.std(), len(t_side)]
# [t_ford.mean() + rec.TIME_COR, t_ford.std(), len(t_ford)]
# [t_back.mean() - rec.TIME_COR, t_back.std(), len(t_back)]

# # show scattering of raw average residual time distribution
# plt.figure(figsize=(6,5), dpi=300)
# plt.hist(t_res, bins=20, range=[0,20])
# plt.yscale('log')
# plt.xlabel("Residual time")
# plt.ylabel("Distribution")
# plt.savefig('PointSource.png')
