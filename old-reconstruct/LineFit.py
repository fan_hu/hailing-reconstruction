import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path

# read data
dirctory = "../data/03161827/"
Path(dirctory+"rec").mkdir(parents=True, exist_ok=True)
for muon_number in range(10):
    # read data
    data_hit = pd.read_csv(dirctory+"Muon_"+str(muon_number), sep=",", skiprows=5, names=["id", "x", "y", "z", "t", "wl"])
    data_hit = data_hit.sort_values(by="t")

    r_mean = np.array([data_hit["x"].mean(), data_hit["x"].mean(), data_hit["x"].mean()])
    t_mean = data_hit["t"].mean()
    rt_mean = np.array([
        (data_hit["x"] * data_hit["t"]).mean(), 
        (data_hit["y"] * data_hit["t"]).mean(), 
        (data_hit["z"] * data_hit["t"]).mean()
    ])
    tt_mean = (data_hit["t"] * data_hit["t"]).mean()

    v_rec = (rt_mean - r_mean * t_mean) / (tt_mean - t_mean**2)
    phi = np.arctan(v_rec[1] / v_rec[0])
    theta = np.arccos(v_rec[2] / np.sqrt(v_rec.dot(v_rec)))
    r0 = r_mean - t_mean * v_rec

    # print result
    print("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(r0[0], r0[1], r0[2], theta, phi))
    with open(dirctory+'rec/'+'Muon_'+str(muon_number)+"_LF", 'w') as fl:
        fl.write("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(r0[0], r0[1], r0[2], theta, phi))
