import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
import scipy.optimize as so
from pathlib import Path
from numpy import pi
import RecClass as rec

def get_res_avg(track, doms, nlim):
    t = []
    d = []
    n = []
    for dom in doms:
        if dom.ni < nlim:
            continue
        [d_dom, t_geo_dom] = track.geometry(dom)
        vec_si = track.direction_vec(dom)
        t_temp = 0
        n_temp = 0
        for i in range(len(rec.DIRECTION_LIST)):
            if (dom.n_pmts[i] == 0):
                continue
            costh_temp = -vec_si.dot(rec.DIRECTION_LIST[i])
            if (costh_temp > 0):
                t_temp = sum(30*np.log(np.array(dom.t_pmts[i]-t_geo_dom)/30 + 1))
                n_temp += dom.n_pmts[i]
        if (n_temp == 0):
            continue
        d.append(d_dom)
        t.append(t_temp / n_temp)
        n.append(n_temp)
    rst = np.array([t, d, n])
    return rst

def Pandel_likeli(para):
    rst = 0
    [beta, lam] = para
    for i in range(len(t)):
        t_exp = beta * d[i] / lam
        t_var = beta**2 * d[i] / lam / n[i]
        rst += np.log(t_var)/2 + (t[i] - t_exp)**2 / (2* t_var)
    return rst


directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
[t, d, n] = [np.array([])] * 3
for id in range(10):
    doms = rec.read_dom(directory, id, True)
    paraSim = rec.read_para(directory, id, 'sim')
    trackSim = rec.Track(*paraSim)
    [t_temp, d_temp, n_temp] = get_res_avg(trackSim, doms, 1)
    t = np.append(t, t_temp)
    d = np.append(d, d_temp)
    n = np.append(n, n_temp)
data = np.array([t, d, n]).T

# show scattering of manipulated average residual time distribution
plt.figure(figsize=(6,5), dpi=300)
plt.scatter(d, t, s=3, alpha=.5)
plt.xlim(0, 200)
plt.ylim(-5, 200)
plt.xlabel("distance (m)")
plt.ylabel("average residual time (ns)")
plt.savefig(directory + 'rec/' + 'MPMTAvgResTime.png')


# do fitting
para_ini = [30, 180]
fit_rst = so.minimize(Pandel_likeli, para_ini, method='Nelder-Mead')
[beta, lam] = fit_rst["x"]
rec.write_pmtsDrop_para(beta, lam)