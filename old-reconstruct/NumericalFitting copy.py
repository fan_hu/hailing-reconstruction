import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from scipy.interpolate import splev, splrep
import RecClass as rec

dirSel = "/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeSelect/"
dirPlt = '/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timePlot/'
dirRst = '/Users/apple/OneDrive - pku.edu.cn/prog/C/PPX/reconstruct/timeRst/'


dataAvg = []
dataSig = []

d_min = [0.0, ]
for i in range(40):
    d_add = max(1.0, 0.2*np.exp(i/10))
    d_min.append(d_min[-1] + d_add)

for i in range(len(d_min)):
    # read data
    time_array = pd.read_csv(dirSel+'I'+str(i)+'.csv', sep=",", skiprows=0, names=['time'])['time']
    time_array = time_array.to_numpy()

    # calculate the average and sigma of the data
    dataAvg.append(time_array.mean() + rec.TIME_COR)
    dataSig.append(time_array.std())


d = {
    'dist': np.array(d_min),
    'avg': np.array(dataAvg),
    'sig': np.array(dataSig)}
dataSta = pd.DataFrame(d)

plt.plot(dataSta["dist"], dataSta["avg"])
paraA = 11; paraB = 0.22
plt.plot(dataSta["dist"], paraA*(np.exp(dataSta["dist"]/paraA*paraB)-1))
plt.xlim(0, 100)
plt.ylim(0, 60)

plt.plot(dataSta["dist"], dataSta["sig"])
paraC = 0.95; paraD = 5
plt.plot(dataSta["dist"], paraC*dataSta["dist"]+paraD)
plt.xlim(0, 100)
plt.ylim(0, 100)
