import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import scipy.special as sc
from pathlib import Path

# read data
dirctory = "../data/03161827/"
Path(dirctory+"rec").mkdir(parents=True, exist_ok=True)
for muon_number in range(10):
    # read data
    data_hit = pd.read_csv(dirctory+"Muon_"+str(muon_number), sep=",", skiprows=5, names=["id", "x", "y", "z", "t", "wl"])
    data_hit = data_hit.sort_values(by="t")

    # get center of gravity
    length = data_hit.shape[0]
    r0 = np.zeros(3)
    for i in range(length):
        r0[0] += data_hit["x"].iloc[i]
        r0[1] += data_hit["y"].iloc[i]
        r0[2] += data_hit["z"].iloc[i]
    r0 /= length

    # get inertial tensor
    inertialTensor = np.zeros((3,3))
    for i in range(length):
        ri = np.array([data_hit["x"].iloc[i], data_hit["y"].iloc[i], data_hit["z"].iloc[i]])
        for j in range(3):
            for k in range(3):
                if (j == k):
                    inertialTensor[j][k] += ri.dot(ri)
                inertialTensor[j][k] -= ri[j] * ri[k]

    # get the minmumal axis of the inertial tensor
    [eigenValues, eigenVectors] = np.linalg.eig(inertialTensor)
    minIndex = np.argmin(eigenValues)
    minVector = eigenVectors[:, minIndex]

    # change to theta phi reprensentation
    theta = np.arccos(minVector[2])
    phi = np.arctan2(minVector[1], minVector[0])

    # print and save result
    print("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(r0[0], r0[1], r0[2], theta, phi))
    with open(dirctory+'rec/'+'Muon_'+str(muon_number)+"_IT", 'w') as fl:
        fl.write("x, y, z, theta, phi \n {:.1f}, {:.1f}, {:.1f}, {:.3f}, {:.3f}" .format(r0[0], r0[1], r0[2], theta, phi))
