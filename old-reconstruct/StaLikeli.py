import RecClass as rec
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt

directory = rec.directory
Path(directory+"rec").mkdir(parents=True, exist_ok=True)
likeliModel = 'avg'
rec.read_pandel_para()
angleErrList = []

for id in range(1000):
    print("Doing Particle {:d}".format(id))
    doms = rec.read_dom(directory, id, trigger=True)
    paraIni = rec.read_para(directory, id, "TF")
    paraSim = rec.read_para(directory, id, "sim")
    trackIni = rec.Track(*paraIni)
    timeShift = trackIni.time_shift(doms)
    optimize = rec.minimize(likeliModel, doms, paraIni, timeShift, "Nelder-Mead", {'maxiter': 3000, 'disp': False})
    paraOpt = optimize.x
    rec.write_opt_rst(paraOpt, directory, id, likeliModel)
    angleErr = rec.angle_err(paraOpt[-2:], paraSim[-2:])
    angleErrList.append(angleErr)

angleErrListNp = np.array(angleErrList)
angleErrListNp.sort()

np.savetxt('angleErrListNp.csv', angleErrListNp)

# smoothing data
data_len = len(angleErrListNp)
jetter = np.random.normal(0, 0.2, 50*data_len)
angleErrListNp_sm = np.tile(angleErrListNp, 50) + jetter

n_hist, bins_hist, patches = plt.hist(angleErrListNp_sm, range = (0, 15), bins=20, density=True)

sig_hist = (bins_hist[1:] + bins_hist[:-1]) / 2
sig_fit = np.linspace(0, 15, 100)
disp = 4.0
n_fit = sig_fit/disp**2 * np.exp(-np.power(sig_fit,2)/2/disp**2)

plt.scatter(sig_hist, n_hist)
plt.plot(sig_fit, n_fit)
plt.xlim(0, 15)


angleErrListSel = angleErrList[: -int(0.05*len(angleErrList))]

plt.hist(angleErrListSel, bins=20, density=True)
plt.xlabel('Angle Error [minits]')
plt.ylabel('Probability')


